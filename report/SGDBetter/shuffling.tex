


This part is inspired from~\cite{MASSART2018334}. The authors have a problem where the objective function is a large sum, as~\eqref{eq:generalObjectiveSum}, and they show that with a particular shuffling, the convergence can be sped up.
In particular, the goal is to efficiently average a set of $N$ symmetric positive definite matrices or SPD matrices.
\nomenclature{SPD}{Symmetric Positive Definite}
The least squares mean of the $N$-tuple $\matr{A} = \parent{\matr{A}_{1}, \dots, \matr{A}_{N}} \in \M^{N}$, with corresponding positive weights $\omega = \parent{\omega_{1}, \dots, \omega_{N}}$, is defined as
\begin{equation*}
  \Lambda \parent{\omega, \matr{A}} = \argmini{\matr{X} \in \M} \sum_{n = 1}^{N} \omega_{n} d^{2} \parent{\matr{X}, \matr{A}_{n}},
\end{equation*}
where $d \parent{\matr{G}, \matr{H}}$ is the affine-invariant distance defined as
\begin{equation*}
  d \parent{\matr{G}, \matr{H}} = \norm{\log \parent{\matr{G}^{-\frac{1}{2}} \matr{H} \matr{G}^{-\frac{1}{2}}}}_{F},
\end{equation*}
for $\matr{G}, \matr{H} \in \mathbb{P}_{n}$, the cone of $n \times n$ SPD matrices.
The matrix exponential and the logarithm of a matrix are defined in Appendix~\ref{appSec:expLog}.


%\todo[inline]{Are the definitions of log and power -1/2 of a matrix correct? Answer: Define log as we define square root: i.e. function applied to diagonalization in natural way}

%\begin{comment}
Inductive sequences are used to estimate the least squares mean of a set of data points belonging to an arbitrary NPC space $\parent{\M, d}$.
As a reminder, a NPC space is a complete metric space with non-positive curvature.
\nomenclature{NPC}{Non-Positive Curvature}
These sequences are built by starting from one data point and making successive steps towards other data points.
A formal definition for the case of the least squares mean can be found at~\cite[Definition~2.1]{MASSART2018334}.


\begin{comment}
\begin{definition}[Inductive sequences]
  Let $\matr{A} = \parent{\matr{A}_{1}, \dots, \matr{A}_{N}} \in \M^{N}$ be a $N$-tuple of data points belonging to a NPC space $\parent{\M, d}$.
  Given a sequence of indexes $p = \parent{p_{1}, p_{2}, \dots}$ and a sequence of step-sizes $\alpha = \parent{\alpha_{1}, \alpha_{2}, \dots}$,
  we define an \emph{inductive sequence} $\parent{\matr{X}_{k}}_{k \in \N_{0}}$ (with $\N_{0}$ the set of strictly positive naturals) as follows:
  \begin{align*}
    \matr{X}_{1} &= \matr{A}_{p_{1}}, \\
    \matr{X}_{k} &= \matr{X}_{k-1} \#_{\alpha_{k}} \matr{A}_{p_{k}}, \quad k \geq 2.
  \end{align*}

  The notation $\matr{A} \#_{\alpha_{k}} \matr{B}$, for $\alpha_{k} \in \squared{0,1}$, stands for the unique point located on the minimizing geodesic between $\matr{A}$ and $\matr{B}$ and satisfying
  $d \parent{\matr{A}, \matr{A} \#_{\alpha_{k}} \matr{B}} = \alpha_{k} d \parent{\matr{A}, \matr{B}}$.
  If $\M = \mathbb{P}_{n}$, this point is the weighted two-variable geometric mean, defined as
  \begin{equation*}
    \matr{A} \#_{\alpha} \matr{B} = \matr{A}^{\frac{1}{2}} \parent{\matr{A}^{-\frac{1}{2}} \matr{B} \matr{A}^{-\frac{1}{2}}}^{\alpha} \matr{A}^{\frac{1}{2}}.
  \end{equation*}
\end{definition}
\end{comment}

%\todo[inline]{Why is ``inductive'' capitalized in~\cite{MASSART2018334}?}


%\todo[inline]{In order to be coherent with Chapter~\ref{ch:riem}, we should use $d$ instead of $\delta$, right ?}
%\end{comment}

The authors define a sequence $p = \parent{p_{1}, \dots, p_{N}}$, and two operations that act on that sequence: the \emph{reverse} operation and the \emph{Faro shuffle}.
It is easier to think about these two definitions as operations on a deck of cards.
\begin{definition}[Reverse] \label{def:reverse}
  Given a sequence $p = \parent{p_{1}, \dots, p_{N}}$, the \emph{reverse} operation is defined as
  \begin{equation}
  \label{eq:reverseShuffle}
    \emph{\text{\textbf{reverse}}} \parent{p} = \parent{p_{N}, \dots, p_{1}}.
  \end{equation}
\end{definition}

Intuitively, we simply swap the order of the deck: we put it upside-down.
The last card becomes the first one, the first card becomes the last one, and so on.
This operation does not influence the order of one card with respect to its neighbors, which is why the second definition is introduced.


\begin{definition}[Faro shuffle] \label{def:faro}
  Given a sequence $p = \parent{p_{1}, \dots, p_{N}}$ with $N = 2M$, it can be rewritten as $p = \parent{p_{1}, \dots, p_{M}, p_{M+1}, \dots, p_{2M}}$.
  Then, the \emph{Faro shuffle}, which is also called the \emph{riffle shuffle}, has two variants: the \emph{in-shuffle} and the \emph{out-shuffle}, defined as
  \begin{align}
    \emph{\text{\textbf{in-shuffle}}} \parent{p}  &= \parent{p_{M+1}, p_{1}, p_{M+2}, p_{2}, \dots, p_{2M}, p_{M}}, \label{eq:inShuffleFaro} \\
    \emph{\text{\textbf{out-shuffle}}} \parent{p} &= \parent{p_{1}, p_{M+1}, p_{2}, p_{M+2}, \dots, p_{M}, p_{2M}}. \label{eq:outShuffleFaro}
  \end{align}
\end{definition}

Let us give a way to interpret this operation.
The original sequence can be thought of as a deck of $N = 2M$ cards, which is divided into two halves.
The top half of the deck is placed in the left hand, while the bottom half of the deck is placed in the right hand.
Then, there are two ways to interleave the cards:
for an \emph{in-shuffle}, cards are alternatively interleaved from the left and right hands; for an \emph{out-shuffle}, cards are alternatively interleaved from the right and left hands.
Thus, a deck originally arranged as $\parent{1 \ 2 \ 3 \ 4 \ 5 \ 6 \ 7 \ 8}$ would become $\parent{5 \ 1 \ 6 \ 2 \ 7 \ 3 \ 8 \ 4}$ using an in-shuffle, and it would become $\parent{1 \ 5 \ 2 \ 6 \ 3 \ 7 \ 4 \ 8}$ using an out-shuffle.
It is easy to understand riffle shuffles with their applications in card tricks but it turns out that they are also used in the theory of parallel processing, as it is explained in~\cite{1671798}.
%American magicians call this the \emph{Faro shuffle}, and to their English colleagues it is known as \emph{weave shuffle}.

At this point, the careful reader will have noticed that we only consider the case when $N$ is even.
When $N$ is odd, one could alternate between an in-shuffle and an out-shuffle, but in practice this will not be necessary.
Indeed, for us, the number of data points $N$ corresponds to $N = \abs{\Omega}$, which is usually a really large number.
Hence, we have $N \approx N - 1$, and one data point can simply be ignored.
Therefore, we are back to the case when $N$ is even.


\subsection{Different ways to browse the data points} \label{subsec:shufflingMethods}

How can we apply this ``shuffling'' idea to our matrix completion algorithm?
At each iteration, we want to go through all the data points.
Hence, such an iteration is called an \emph{epoch}.
The order in which the data points are scanned through might seem unimportant, but the idea presented in~\cite[Algorithm~1]{MASSART2018334} gives us a good guess about which order the method should follow.
For the reader's convenience, this algorithm is repeated in the following lines, which is called Algorithm~\ref{algo:shuffle} in the present document. 


\begin{algorithm}
\begin{algorithmic}[1]
%\Procedure{Shuffling algorithm}{$\alpha_{0} > 0, \tau \in \parent{0,1}$}
   \State Data: $N$, the number of data points.
   \State $p^{\parent{1}} = \parent{p_{1}, \dots, p_{N}}$
   \For{$k = 1, 2, \dots$}
      \State $p^{\parent{2k}} = \text{\textbf{reverse}} \parent{p^{\parent{2k-1}}}$
      \State $p^{\parent{2k+1}} = \text{\textbf{in-shuffle}} \parent{p^{\parent{2k-1}}}$
   \EndFor  
%\EndProcedure
\end{algorithmic}
\caption{Smart shuffling algorithm}
\label{algo:shuffle}
\end{algorithm}

All the methods that are described in this section work in the same way, except for \emph{one} detail.
For every method, epoch $k$ starts by defining a sequence $p^{\parent{k}}$.
Then, the data points are browsed in the order specified by $p^{\parent{k}}$.
By browsing, we mean that we perform a scaled SGD step with a batch containing one element: $p_{1}^{\parent{k}}$, then $p_{2}^{\parent{k}}$, and so on, until $p_{N}^{\parent{k}}$.
We always have $\abs{p^{\parent{k}}} = N$. For example, if $N = 3$, we can have $p^{\parent{k}} = \parent{3, 1, 2}$.
In that case, a scaled SGD step is performed with sample $n = 3$, then with sample $n = 1$ and finally with sample $n = 2$.

Of course, the small detail that differentiates the four methods described next is in the way the sequence $p^{\parent{k}}$ is defined.
\begin{enumerate}
  \item \label{item:noShuffle} ``No shuffling''.
        The sequence $p^{\parent{k}}$ is defined as $p^{\parent{k}} = \parent{1, \dots, N}$ for every $k$.
        This is also called the ``Cyclic order'' or the ``Circular order''.
        This method is deterministic. 
  \item \label{item:randomWithout} ``Random shuffling without replacement''.
        The sequence $p^{\parent{k}}$ is defined as a random permutation without repetition of the $N$ data points.
        All the data points are visited.
        We can use ``repl.'' to shorten ``replacement''.
        We also assume that ``Random shuffling'' denotes a shuffling without replacement in the sequel. 
        This method is stochastic.
  \item \label{item:randomWith} ``Random shuffling with replacement''.
        The sequence $p^{\parent{k}}$ is defined as a random permutation with repetition of the $N$ data points.
        This means that at epoch $k$, some data points are visited more than once, while others might not be visited.
        This method is stochastic.
  \item \label{item:smartShuffle} ``Smart shuffling''.
        The sequence $p^{\parent{k}}$ is defined by Algorithm~\ref{algo:shuffle}.
        This method is deterministic.
\end{enumerate}



\subsection{Numerical experiments} \label{subsec:numExp}

In the upcoming experiments, synthetic data are created in the following way:
two full-rank matrices $\matr{A} \in \R^{d_{1} \times r}_{*}$ and $\matr{B} \in \R^{d_{2} \times r}_{*}$ are generated according to a standard Gaussian distribution.
Of course, all the elements of these two matrices are independently and identically distributed. 
The product $\matr{M} = \matr{A} \matr{B}^{\top}$ is a matrix of size $d_{1} \times d_{2}$ and of rank $r$.
Indeed, we have $r \ll \min \bracket{d_{1}, d_{2}}$, and we use Proposition~\ref{prop:rankApp} stated in Appendix~\ref{appSec:rank}.
Then, a fraction of the entries of $\matr{M}$ are randomly removed with uniform probability.
The number of known entries is determined by the OS ratio, since we have $\abs{\Omega} = N = \text{OS} \times r \times \parent{d_{1} + d_{2} - r}$.

%\todo[inline]{Say that r << d1, d2 + explain why rank(M) = r}



Since the experiments are random, representative simulations were implemented.
The same experiment is run five times, and then the results are averaged.
The evolution of the train RMSE or test RMSE is analyzed as a function of the number of epochs.
As a reminder, the two most famous metrics used to measure accuracy are the Mean Absolute Error (MAE) and the Root Mean Squared Error (RMSE).
We only give the definitions for the training set, since it suffices to replace ``$\text{Train}$'' by ``$\text{Test}$'' in the following definitions in order to have them for the testing set.
\begin{enumerate}
  \item The MAE is defined as 
        \begin{equation}
        \label{eq:MAE}
            \text{MAE}_{\text{Train}} = \dfrac{\sum_{\parent{i, j} \in \text{TrainingSet}} \abs{M_{ij} - X_{ij}}}{\abs{\text{TrainingSet}}},
        \end{equation}
        and the normalized version of the MAE, called the Normalized Mean Absolute Error or NMAE is often considered as well.
        It is defined as 
        \begin{equation}
        \label{eq:NMAE}
            \text{NMAE}_{\text{Train}} = \dfrac{\text{MAE}_{\text{Train}}}{\text{range}},
        \end{equation}
        where the range is the distance between the largest and the smallest value in the data set.
        
  \item For the RMSE, we start by defining the Mean Squared Error or MSE as
        \begin{equation}
        \label{eq:MSE}
            \text{MSE}_{\text{Train}} = \dfrac{\sum_{\parent{i, j} \in \text{TrainingSet}} \parent{M_{ij} - X_{ij}}^{2}}{\abs{\text{TrainingSet}}}.
        \end{equation}
        Then, the RMSE is simply defined as 
        \begin{equation}
        \label{eq:RMSE}
          \text{RMSE}_{\text{Train}} = \sqrt{\text{MSE}_{\text{Train}}}.
        \end{equation}
\end{enumerate}

\nomenclature{MAE}{Mean Absolute Error}
\nomenclature{NMAE}{Normalized Mean Absolute Error}
\nomenclature{RMSE}{Root Mean Squared Error}
\nomenclature{MSE}{Mean Squared Error}

Here, since the computation times for the different methods are similar, the number of iterations, or epochs, can be used to truthfully represent the complexity of the algorithm. 
In Figure~\ref{fig:four}, we see that the four methods behave similarly, ``Smart shuffling'' behaving slightly worse, for a particular set of parameters.
Varying the seed in MATLAB does not change the results, i.e., the experiment is rather stable.
Specifically, a small square matrix of size $1000 \times 1000$ and of rank $10$ is chosen.
The OS ratio is set to $4$.
The MSE tolerance is set to $10^{-8}$, and we are considering a maximum of $100$ epochs.
In practice, the algorithm always converges in less than $100$ epochs.
Some noise is introduced according to a parameter $\texttt{noiseFac} = 10^{-6}$ to a well-conditioned matrix. 
Starting the algorithm with randomly initialized matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ does not yield good results.
Hence, the initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are defined by computing the $r$ dominant SVD, that is, the $r$ largest singular values of the sparse matrix containing the known entries.
%We also compute the corresponding $r$ eigenvectors.
\nomenclature{SVD}{Singular Value Decomposition}
In other words, we first compute the matrices $\matr{U}$, $\matr{\Sigma}$ and $\matr{V}$ from the sparse matrix containing the known entries.
Then the initial matrices $\matr{L}_{0} = \matr{U} \matr{\Sigma}^{\frac{1}{2}}$ and $\matr{R}_{0} = \matr{V} \matr{\Sigma}^{\frac{1}{2}}$ are defined.
Finally, regarding the step-size, the bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} was chosen, with an initial value of $0.05$.
Since the test RMSE is very similar to the train RMSE, we only display the latter.



\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{images/syntheticFourTrain}
    \caption{Evolution of the train RMSE for scaled SGD, using the four different shuffling methods described in Subsection~\ref{subsec:shufflingMethods}.
              A synthetic matrix of size $1000 \times 1000$ is constructed, for which the rank is known to be $r = 10$.
              The OS ratio is set to $4$.
              Initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are computed using $r$ dominant SVD.
              The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.05$.
              The algorithm is stopped when either the MSE (defined in~\eqref{eq:MSE}) is less than $10^{-8}$ or the number of epochs exceeds $100$.
              The same experiment is run five times, and the results are averaged.
            }
    \label{fig:four}
\end{figure}


At this point, one should note that even if the ``No shuffling'' method described above (item~\ref{item:noShuffle} on page~\pageref{item:noShuffle}) is deterministic, it will not produce the same results for two identical experiments. 
Indeed, the true data matrix $\matr{M}$ is stochastic, hence the initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are stochastic as well; therefore the sequences of step-sizes defined by the bold driver protocol are different, and so on.
Of course, the same can be said about the ``Smart shuffling'' method described above (item~\ref{item:smartShuffle} on page~\pageref{item:smartShuffle}), since it also deterministic.
This being said, the careful reader will have noticed that for a given experiment, after the first epoch, these two methods move in the same direction and at the same speed, hence they increase or decrease the cost \emph{in the same way}.
This stems from the fact that the sequence $p^{\parent{1}}$ is the same for the two methods at the first epoch.
Since the initial step-size is also the same for the two methods at the first epoch, the observation makes sense.




All simulations are performed on version R2015b of \textsc{MATLAB}, on a 2.0 GHz Intel Core i7 machine with 8 GB of RAM.
For a remark on the implementation, we refer the reader to Appendix~\ref{app:C++}.


Surprisingly, in Figure~\ref{fig:four}, we observe that the ``No shuffling'' method performs slightly better than the ``Random shuffling without replacement''.
Indeed, it is shown in~\cite{MASSART2018334} that visiting the data points in a cyclic order usually results in a slow convergence.
It can be that we need larger tests to observe this phenomenon.
Equivalently, the fact that synthetic data is used can also be a problem. 
We observe that ``Smart shuffling'' is the worst of the four methods. 

%Let us give some intuition for that: there are $N!$ different ways to define the sequence $p^{k}$.
%Furthermore, it is clear that Algorithm~\ref{algo:shuffle} will sequentially take one of the $N!$ possible orders.
%However, the SGD method often stops way before all of these sequences have been used.
%Hence, even if Algorithm~\ref{algo:shuffle} proved useful in averaging a set of $N$ SPD matrices, it is not useful in the case of the synthetic data set for our LRMC problem, because convergence is already attained after a few epochs.

%\todo[inline]{Is this a correct intuition???}

\begin{comment}
However, it is interesting to look at the first iteration in Figure~\ref{fig:four}:
we see that for ``Random shuffling'' with and without replacement, the first epoch is really effective, in the sense that it causes a strong decrease in the cost.
The contrast with ``Smart shuffling'' and ``No shuffling'' is strong: for these two methods, the first epoch is actually really bad, since the cost function increases.
Therefore, we have tried to decrease the initial step-size.
This gave encouraging results: the four curves overlap, which indicates that they all perform equally well.
This overlap makes the plot less interesting, which is why we do not display it here.
\end{comment}

%\todo[inline]{Is there another way to display it?}

Hence, it seems that looking at the data points in a ``smart'' way does not really influence the speed of convergence compared to a cyclic order, nor a random order.
Let us vary some parameters to see if this is always the case.

\paragraph{Step-size}
All the step-size methods described in Section~\ref{sec:SGD:step} have been tested.
We have tried the constant step-size $\alpha_{k} = \alpha$ for different values of $\alpha$, it did not yield good results.
The same can be said about the geometric step-size, and the exponential decay protocol.
The counter method gave similar results than the ones obtained with the bold driver.
%\todo[inline]{Understand peak at 21 iterations.}



\paragraph{Over-sampling ratio}
In Figure~\ref{fig:OS}, page~\pageref{fig:OS}, we can see the difference between a small and a large OS ratio on the convergence.
Specifically, Figure~\ref{fig:fourOSSmall} corresponds to $\text{OS} = 2$ and Figure~\ref{fig:fourOSLarge} corresponds to $\text{OS} = 8$.
All the other parameters are unchanged, \emph{ceteris paribus}, which means that we are still working with a small matrix of size $1000 \times 1000$, of rank $10$, and so on.
The only exception to this is that the initial step-size was set to $0.01$, otherwise the results were too noisy.
As we had predicted, we observe that a large OS ratio enables a faster convergence, i.e., fewer epochs, since we have more data points.
In fact, taking a small value for $\text{OS}$ can even cause the algorithm to reach the limit of $100$ epochs, before having attained convergence.
However, the faster convergence guaranteed by a larger OS ratio comes at a cost: every epoch is more expensive when the OS ratio is larger, since more data points have to be browsed. 
Again, since the train RMSE behaves similarly to the test RMSE, we only display the former. 
On the one hand, when only a few entries are sampled, the ``Smart shuffling'' method performs worse than ``Random shuffling without repl.'' or ``No shuffling''.
Surprisingly, again, the cyclic order performs the best.
Changing the seed in order to control the random number generation did not give very different results. 
On the other hand, when many entries are sampled, the ``Random shuffling with replacement'' method is the worst, as we had expected, and the three others behave similarly.

%It even goes up in the beginning, which shows that we should pick a smaller initial step-size.

%\todo[inline]{Make other tests: HELLO}

\begin{figure}[h!]
\centering
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{fourOSSmall}
  \caption{$\text{OS} = 2$}
  \label{fig:fourOSSmall}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{fourOSLarge}
  \caption{$\text{OS} = 8$}
  \label{fig:fourOSLarge}
\end{subfigure}
\caption{Influence of the OS ratio on the evolution of the train RMSE for scaled SGD, using the four shuffling methods described in Subsection~\ref{subsec:shufflingMethods}.
          A synthetic matrix of size $1000 \times 1000$ is used, for which the rank is known to be $r = 10$.
          Initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are computed using $r$ dominant SVD.
          The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.01$.
          The algorithm is stopped when either the MSE is less than $10^{-8}$ or the number of epochs exceeds $100$.
          The same experiment is run five times, and the results are averaged.}
\label{fig:OS}
\end{figure}

\paragraph{In or out?}
One might wonder if it matters to consider an \emph{in-shuffle} or an \emph{out-shuffle}.
If $N$ is large, then simulations show that there is no difference.
However, if $N$ is small, we can see that the in-shuffle performs better:
this is due to the fact that with an out-shuffle, the extreme elements do not change.
Indeed, the first element of the sequence $p^{\parent{1}}$ either stays the first element of $p^{\parent{k}}$ or becomes the last element of $p^{\parent{k}}$, for all following values of $k$.
Similarly, for the last element of $p^{\parent{1}}$.
This is easy to see by looking at Definitions~\ref{def:reverse} and~\ref{def:faro}.
Therefore, unless stated otherwise, we always consider an in-shuffle in the sequel.


%\todo[inline]{How to show that two methods are similar?}

\begin{comment}
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{images/fourInOur}
    \caption{Evolution of the train RMSE for the in-shuffle or out-shuffle dilemma.}
    \label{fig:fourInOur}
\end{figure}
\end{comment}

\paragraph{Rectangular matrices}
We consider rectangular matrices of size $1000 \times 100$, and rank known to be $10$.
The OS ratio is set to $4$, \emph{ceteris paribus}.
The results depicted in Figure~\ref{fig:fourRect}, page~\pageref{fig:fourRect}, show that ``Smart shuffling'' performs worse than ``Random shuffling'' with or without replacement.
However, the results are very noisy, i.e., changing the seed in MATLAB can yield very different curves for the train RMSE.
Therefore, no conclusion can be taken, since more tests need to be done.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.7\textwidth]{images/fourRect}
    \caption{Evolution of the train RMSE for scaled SGD, using the four different shuffling methods described in Subsection~\ref{subsec:shufflingMethods}.
              A synthetic rectangular matrix of size $1000 \times 100$ is used, for which the rank is known to be $r = 10$.
              The OS ratio is set to $5$.
              Initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are randomly generated according to a standard Gaussian distribution.
              The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.1$.
              The algorithm is stopped when either the MSE is less than $10^{-8}$ or the number of epochs exceeds $100$.
              The same experiment is run five times, and the results are averaged.}
    \label{fig:fourRect}
\end{figure}


\paragraph{Other parameters}
Changing other parameters like the noise ratio, or considering ill-conditioned matrices did not yield interesting results: the four curves overlap most of the time.
In short, it seems that the ``Smart shuffling'' does not give better results, neither for the training error nor for the testing error. 

\paragraph{Real data set}
Here, we are again working with the first part of the Jester data set, taken from~\cite{Goldberg:2001:ECT:593963.594023}, as in Section~\ref{sec:SGDB:rank}.
The four methods are tested for $2000$ users.
The results are depicted in Figure~\ref{fig:jokesFour}, page~\pageref{fig:jokesFour}.
As usual, the training error (\ref{fig:jokesFourTrain}) is lower than the testing error (\ref{fig:jokesFourTest}), because the training error is overly optimistic.
%We see that the four methods behave similarly.
%The results on this real-world data set are rather positive.
It is observed that ``No shuffling'' performs worse than the ``Random shuffling''.
Furthermore, ``Random shuffling'' performs better without replacement than with replacement.
Finally, ``Smart shuffling'' gives the lowest test RMSE.
%As it was predicted, when the data points are browsed in a cyclic order (``No shuffling''), the performance is worse than when they are browsed in a random order (``Random shuffling'').
%Here, the ``Smart shuffling'' method performs slightly better than the three others. 
We can see that the initial step-size is obviously too large in these tests, since the cost starts to increase for most of the shuffling methods.


\begin{figure}[!htb]
\centering
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{jokesFourTrain}
  \caption{Train RMSE}
  \label{fig:jokesFourTrain}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{jokesFourTest}
  \caption{Test RMSE}
  \label{fig:jokesFourTest}
\end{subfigure}
\caption{Evolution of the train and test RMSE for scaled SGD, using the four different shuffling methods described in Subsection~\ref{subsec:shufflingMethods}.
          The real-world Jester data set is used from~\cite{Goldberg:2001:ECT:593963.594023}, i.e., a matrix of size $2000 \times 100$.
          The rank is fixed to $r = 5$ according to Subsection~\ref{subsec:rank:cv}.
          Initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are randomly generated according to a standard Gaussian distribution.
          The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.1$.
          The algorithm is stopped when either the MSE (defined in~\eqref{eq:MSE}) is less than $10^{-8}$ or the number of epochs exceeds $40$.
          Note that the first five epochs are not shown, otherwise the graph would have been less convenient to scale for the vertical axis.
          }
\label{fig:jokesFour}
\end{figure}


%\todo[inline]{Note that we start at $5$ for graphical reasons. Otherwise the plot was not nice.}

\vspace{5mm}
Up to now, we have always considered that all the data points had to be visited at every epoch.
This does not have to be the case.
Therefore, another shuffling idea, called \emph{importance sampling}, is described in the next section.


