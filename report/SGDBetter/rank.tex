At this point, it is clear that the rank $r$ is a \emph{hyperparameter}, i.e., a parameter that the user is able to choose.
Hence, the question arises: \emph{how do we choose this hyperparameter?} 
Often, a value is chosen by convention, like $r = 5$ for small problems, and $r = 10$ for large problems. 
In this section, we look for a more custom way of finding the best value of $r$.
This is called the \emph{model selection problem}.
In particular, two methods are discussed.
The first one is called the \emph{elbow method} and is often used to determine the number of clusters in a data set, i.e., the problem of $K$-means clustering.
This is a similar problem to identifying the number of latent features in a data set.
The second one is \emph{cross-validation}, which is very famous in the machine learning community.





\subsection{The elbow method} \label{subsec:rank:elbow}

Here is a summary inspired from~\cite{MLEPFL}: the data is split into one training set $S_{\text{training}}$ and one testing set $S_{\text{testing}}$.
Let us assume that the sizes are equal.
In that case, it means that the first half of the observed values of $\matr{M}$ is taken to train the algorithm, and the other half to test it. 
These two sets can be thought of as having been generated independently and sampled according to an underlying but unknown distribution.
On the one hand, the training set is used to \emph{construct a prediction function}; that is, the learning algorithm runs with the training set.
On the other hand, the testing set is used to \emph{compute the validation error}, also called the \emph{testing error}, that is, the error that a particular prediction function makes on unseen data.

We have in addition a set of values for the parameter of the model, i.e., the rank $r$, that the user can choose.
Let us denote these values by $r_{d}$, for $d = 1, \dots, D$.
To keep things simple we assume that $D$ is some finite value.


%The basic idea of this bound can be carried over to the case where we have infinitely many models.
%In this case, a more sophisticated concept called the Vapnik–Chervonenkis dimension, or VC-dimension, is used: as long as we have models with a finite VC-dimension, then the bound has the same form, with $D$ replaced by the VC-dimension.

%\todo[inline]{Noise or interesting fact?}

The learning algorithm is run $D$ times on the same training set $S_{\text{training}}$ to compute the $D$ prediction functions $f_{r_{d}}$ for $d = 1, \dots, D$.
For each such prediction function, the validation error is computed with the testing set.
We then choose that value of the parameter $r$ which gives the smallest validation error.



Models can be too limited, or they can be too rich.
In the first case, we cannot find a function that is a good fit for the data in our model.
We \emph{underfit}.
In the second case, we have such a rich model family that the prediction function fits not only the underlying function but also the noise in the data.
We then talk about an \emph{overfit}.
Both of these phenomena are undesirable. 




The elbow method works as follows.
The training error always decreases as the rank $r$ increases.
Hence, we have to find a way to determine the ``optimal rank'', in a way that the model is rich enough to represent the underlying interactions, but not too rich in order to avoid fitting the noise.
This being said, one should know that \emph{there is no perfect rank}, because the true data matrix $\matr{M}$ is almost never sampled with a specific rank in mind.
However, to fix the ideas, let us use $r^{\star}$ to denote the optimal rank.
We are trying to find the perfect value for $r$ to avoid both underfitting and overfitting.
Therefore, we try to identify a \emph{kink} in the curve representing the \emph{training} error as a function of the rank.
A kink can be understood as a break, or a fracture in the curve.
To fix the ideas, we would identify a line with negative slope $-a$ for $r \in \squared{0, r^{\star}}$, and a line with negative slope $-b$ for $r \in \left[r^{\star}, \infty\right[$, for $a, b > 0$.
Ideally, the first line is very steep, and the second one is flatter, i.e., $a > b$, such that $r^{\star}$ can easily be identified.


%\todo[inline]{What is a kink? Is it clear what a kink is?}


In other words, let us look at the curve representing the training error as a function of the rank: if this line looks like an arm, then the ``elbow'' on the arm is the best value of $r$.
The elbow method states that the kink happens at the rank $r^{\star}$.




On the one hand, for all the values of $r < r^{\star}$, the training error decreases rapidly, since increasing the rank makes the model richer and more able to represent the underlying interactions.
In other words, underfitting happens as long as $r < r^{\star}$, since the model is not complex enough to represent all the interactions in the true matrix, hence both the training error and the testing error are high.

On the other hand, for all the values of $r > r^{\star}$, the training error decreases more slowly, since increasing the rank only allows the model to fit the underlying noise, which does not decrease the error drastically.
In other words, overfitting happens when $r > r^{\star}$, because the model is too complex, hence it can represent both the interactions in the true matrix, but also the underlying noise. This means that the training error continues to decrease, while the testing error increases, since the model moves too far from the mean of the true data, and it will fail to predict well on unseen data. 

Putting everything together, the elbow method is a graphical way of determining the value of $r^{\star}$.
Let us look at Figure~\ref{fig:rankJokes}.
We are working with the first part of the Jester data set, available at~\cite{Goldberg:2001:ECT:593963.594023}.
It consists of ratings, ranging between $-10$ and $10$, of $100$ jokes by $24 983$ users.
Hence, the matrix $\matr{M}$ is very thin, with $d_{1} = 24983$ and $d_{2} = 100$.
In order to reduce the size of the problem, $2000$ users are randomly selected, and all the other users are neglected. 
Two ratings per user are randomly extracted as test data. 
Using the notations defined above, we have $D = 10$ values for the rank, from $1$ to $10$. 
For each rank, the train and the test error are computed using the scaled SGD method described in~\eqref{eq:sgdWithScalingGeneral}, with ``Random shuffling'', a method described in the upcoming Subsection~\ref{subsec:shufflingMethods}.
The train and test error, or mean squared error (MSE), are computed as the sum of the squared differences between the true values and the predicted values, over the total number of values.
The MSE is formally defined in the upcoming equation~\eqref{eq:MSE}.
For the initialization of the algorithm, full rank matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are randomly generated.
Regarding the step-size, the bold driver protocol described in Section~\ref{sec:SGD:step} is chosen, with an initial value of $0.01$.
The number of iterations was limited to $100$, and the gradient tolerance for the MSE was set to $10^{-8}$.
In practice, most of the runs did not reach the gradient tolerance level, because they were interrupted after $100$ iterations.  
This entire process is repeated five times, and we average the results. 
According to the elbow method, the optimal rank is $r^{\star} = 2$.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.7\textwidth]{images/rankJokes}
    \caption{Average train and test MSE for different values of the rank $r$, which is fixed \emph{a priori}, for the Jester data set described in~\cite{Goldberg:2001:ECT:593963.594023}.
              In particular, the sampled matrix is rectangular of size $2000 \times 100$, and two ratings per user are randomly extracted as test data.
              The MSE is defined as $\norm{\P \parent{\matr{M}} - \P \parent{\matr{X}}}^{2}_{F} / \abs{\Omega}$.
              The scaled SGD method described by the equations~\eqref{eq:sgdWithScalingGeneral} is used.
              We see that the elbow method is not always easy to apply, because the notion of ``true rank'' is not always applicable.}
    \label{fig:rankJokes}
\end{figure}


%\todo[inline]{Define MSE in this case!}

%\todo[inline]{Explain that the graph comes from our own codes.}

The training error is under the testing error, which makes a lot of sense since the training error is always too optimistic. 
The interpretation is that the training error is not a good representation of the accuracy of the algorithm, since it uses the same data to construct the prediction function and to evaluate the accuracy.

In other applications, we might have a case where the ``best'' rank is $r = 1$.
In such cases, this graphical method does not work well.
Indeed, we cannot notice a kink in that case.
Therefore, another method, described in~\cite{doi:10.1111/1467-9868.00293} is used.
However, since it is unlikely that the best rank is $r = 1$ in our situations, it will not be used.





\subsection{Cross-validation} \label{subsec:rank:cv}
Splitting the data once into two parts (one for training and one for testing) is not the most efficient way to use the available information.
To solve this problem, cross-validation, often abbreviated by CV, is a popular way of making better use of the data.
\nomenclature{CV}{Cross-Validation}
Since there are many versions to date, let us just mention one.
For a full review of cross-validation, we refer the reader to~\cite[Section~1.3]{MLBishop}.
A popular scheme is called $K$-fold CV.
The data is randomly partitioned into $K$ groups, then we train $K$ times.
Each time, exactly one of the $K$ groups is left out for testing and the remaining $K - 1$ groups are used for training.
Then, the $K$ results are averaged.
Note that in this way all the data has been utilized for training and for testing, and each data point has been used the same number of times.
One major drawback of $K$-fold CV is that the number of training runs that must be performed is increased by a factor $K$, and this can prove problematic for models in which the training is itself computationally expensive.

By trying $K$-fold CV with $K=5$ in order to determine the best rank for the same data set, i.e., the Jester data set, we find $r^{\star} = 5$.
This corresponds to the value suggested in~\cite{5394534}.
The value for $r^{\star}$ was set to $2$ with the elbow method described above.
Hence, we see that finding the best rank can be difficult since such a concept is tedious to define. 
For a remark on the implementation, we refer the reader to Appendix~\ref{app:Python}.


