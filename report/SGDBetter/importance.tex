This section is inspired from~\cite[Section~3]{2013arXiv1310.5715N}.
The idea is the following: up to now, we were looking at $N$ data points at every epoch.
This will not be changed.
Instead, we can ask: do we have to browse \emph{all} the data points?
In the previous section, three of the four shuffling methods are considering all the data points at every epoch.
The only analysis is about the order: is there a specific order to browse all the data points such that the convergence is better?
However, ``Random shuffling with replacement'' does not specifically take all the data points into account: one data point can be taken more than once, while another might not be taken at all.
The methods that are discussed in this section have a similar behavior, hence they can be called ``Weighted shuffling'' methods.


We want to show that some data points are more important than others, and that these data points should be visited more often.
Hence, $N$ data points are still considered at every epoch, but they are not necessarily $N$ \emph{different} data points. 
This can be seen as a shuffling where some data points are left behind, while others are duplicated to make sure that $N$ data points are visited in total at every epoch.
E.g., if $N = 3$, we could define the sequence $p^{\parent{k}}$ as $\parent{2,1,2}$.

We start by redefining the objective function of the original LRMC formulation as a least squares function.
For the reader's convenience, the objective function~\eqref{eq:OmegaObjective} of page~\pageref{eq:OmegaObjective} is repeated here
\begin{equation*}
  \literalref{eq:OmegaObjective}.
\end{equation*}
Therefore, we have the following proposition.
\begin{proposition}
\label{prop:ls}
  The objective function~\eqref{eq:OmegaObjective} of page~\pageref{eq:OmegaObjective} can be written as
  \begin{equation}
  \label{eq:Axb}
    f \parent{\matr{x}} = \frac{1}{2} \norm{\matr{A} \matr{x} - \matr{b}}_{2}^{2},
  \end{equation}
  for some vectors $\matr{x}, \matr{b}$ and a matrix $\matr{A}$.
\end{proposition}

\begin{proof}
  See Appendix~\ref{app:proofEquivalentObjective}.
\end{proof}






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
At this point, if the objective function is of the form~\eqref{eq:Axb}, it is very tempting to add a regularization term in order to avoid overfitting.
We would thus have
\begin{equation}
  f \parent{\matr{x}} = \frac{1}{2} \norm{\matr{A} \matr{x} - \matr{b}}_{2}^{2} + \lambda \norm{\matr{x}}^{2}_{2}.
\end{equation}
This would give a problem formulation of the form
\begin{equation*}
%\label{LRMC:prob}
  \begin{aligned}
    & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
    & & \dfrac{1}{2} \norm{\P \parent{\matr{X} - \matr{M}}}_{F}^{2} + \lambda \norm{\matr{X}}^{2}_{F} \\
    & \text{subject to}
    & & \text{rank} \parent{\matr{X}} = r.
  \end{aligned}
\end{equation*}
However, this would not be necessary. 
Indeed, the algorithm already avoids overfitting by imposing the low-rank constraints: $\matr{X} \in \M_{r}$. 
Hence, as it is stated in Section~\ref{sec:work}, we decided to put $\lambda = 0$.
\end{comment}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


The authors of~\cite{2013arXiv1310.5715N} consider SGD methods, where gradient estimates are sampled from a weighted distribution.
As previously, the random variable $\mathcal{N}$ is one of the $N$ training examples, i.e., $\mathcal{N} \in \squared{N}$.
We use $n$ to denote a realization of $\mathcal{N}$.
For a weight function $w$ which assigns a non-negative weight $w \parent{n} \geq 0$ to each index $n$, the weighted distribution $\mathcal{D}^{\parent{w}}$ is defined as the distribution such that
\begin{equation*}
  \mathbb{P}_{\mathcal{D}^{\parent{w}}} \squared{I} \propto \E_{\mathcal{N} \thicksim \mathcal{D}} \squared{\mathbf{1}_{I} \parent{\mathcal{N}} w \parent{\mathcal{N}}},
\end{equation*}
where $I$ is an event (subset of indices) and $\mathbf{1}_{I} \parent{\cdot}$ its indicator function.
We have a discrete distribution $\mathcal{D}$ since the sample $\mathcal{N}$ is chosen from $\squared{N}$.
Hence, the probabilities of the original probability mass function $p \parent{n}$ are weighted in order to obtain a new probability mass function, written as
\begin{equation*}
  p^{\parent{w}} \parent{n} \propto w \parent{n} p \parent{n}.
\end{equation*}
At this point it may be useful to mention that $w$ can be seen as a weight function, but also as a weight vector of size $N \times 1$.
Indeed, entry $w_{n} = w \parent{n}$ simply denotes the weight assigned to data point $n$.
Therefore, we use $\matr{w}$ to denote this vector, and we interchangeably write $w \parent{n}$ and $w_{n}$ to denote the weight of data point $n$.



The authors use rejection sampling to construct $\mathcal{D}^{\parent{w}}$. 
The procedure is as follows: first, define a constant $W \geq \sup_{n} w \parent{n}$.
We choose $W = 1$.
Then, sample $\mathcal{N} \thicksim \mathcal{D}$, say we select data point $n$, and accept that sample with probability $w \parent{n} / W$.
Otherwise, reject and continue to resample until a suggestion $n$ is accepted.
The accepted samples are then distributed according to $\mathcal{D}^{\parent{w}}$.

How can we define the weight function $w$ ? 
We propose two heuristics.

\subsection{A really expensive method} \label{subsec:importanceExpensive}
This method could work with small data sets.
We start by computing the cost $f_{0}$ corresponding to the initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$.
If on the one hand, after the first epoch it seems that sample $n$ does not decrease the cost function, then this sample is given a low weight $w \parent{n}$.
If on the other hand, it seems that sample $n$ greatly decreases the cost function, then this sample is given a high weight $w \parent{n}$.
Concretely, for each sample $n \in \squared{N}$, corresponding to item $\parent{i, j} \in \Omega$, the rows $\matr{L}_{i:}$ and $\matr{R}_{j:}$ are updated, and the new cost $f_{n}$ is computed.
We use $\delta_{n} = f_{n} - f_{0}$ to denote the difference, and we hope that $\delta_{n} < 0$.
If indeed $\delta_{n} < 0$, then it means that data point $n$ allowed us to decrease the cost, and this data point is given a high weight, proportional to $\abs{\delta_{n}}$.
If however $\delta_{n} > 0$, then it means that data point $n$ did not allow us to decrease the cost, and this data point is given a zero weight. 
Hence, a weight vector $\matr{w}$ is obtained.
Of course, when the cost corresponding to the next sample $n + 1$ is computed, one should first undo the updates of the rows $\matr{L}_{i:}$ and $\matr{R}_{j:}$, otherwise the update of sample $n + 1$ will benefit from the updates of all the preceding samples.
We can already mention that this algorithm is not feasible in practice, since evaluating the cost function for every data point is impossible in real-life applications.
Furthermore, we would have to compute $\delta_{n}$ for every $n \in \squared{N}$, which can be very expensive.


One could think that this should be done only at the first iteration, and then keep the weights as they are for all the following epochs.
However, if the weights are optimal for the first epoch, it does not necessarily mean that they are optimal for the next ones.
Furthermore, it is even hard to argue that the weights are optimal \emph{for the first epoch at all}. 
Indeed, it may be that data point $n$ is ``good'' when we start with the initial random matrices $\matr{L}_{0}$ and $\matr{R}_{0}$.
However, we first visit some other data points before looking at $n$, and it might be the case that $n$ is not good anymore at that time.

Since we suggest recomputing the weights after every iteration, it is clear that this method cannot work for the Netflix problem, for instance.
For this reason, this expensive method is tried on a small synthetic data set: a small matrix of size $200 \times 200$ is randomly generated, with a known rank $r = 4$.
Then, some entries are removed with uniform probability.
As usual, the total number of known entries is specified by the OS ratio, set to $4$.
In this experiment, the weights are updated \emph{after every epoch}: this means that convergence is really slow, as it can be seen in Figure~\ref{fig:syntheticExpensiveWeights}.
The algorithm is initialized with matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ that are populated with random entries drawn from the standard Gaussian distribution.
The initial step-size is set to $0.01$ and the bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used.
As always, the regularization and scaling parameters are set to $\lambda = 0$ and $\mu = 0.5$ respectively.
One experiment corresponds to the number of epochs needed to reach the tolerance level for the train MSE, namely $10^{-8}$.
Five such experiments are run, which gives five different outputs, and the results are averaged. 
Indeed, the given outputs are different because each experiment depends on the random initializations, the random permutations, hence the sequences of step-sizes are not the same, and so on.
For comparison, we take the ``Random shuffling'' method described in item~\ref{item:randomWithout} on page~\pageref{item:randomWithout} above where all the data points are browsed in a random order.
%As a reminder, our method ``Expensive weighted shuffling'' also browses the data points in a random order, but some are never browsed, while others are browsed more than once.
%The two methods are respectively abbreviated by ``Random shuffling'' and ``Expensive weights'' in Figure~\ref{fig:syntheticExpensiveWeights}.
As expected, the weighted shuffling version is a lot slower, because each epoch starts by computing the weights, which is a very costly process.
However, we can observe that fewer epochs are needed, which reinforces our intuition.
Indeed, ``Random shuffling'' needs $17$ epochs on average to converge, whereas ``Weighted shuffling'' only needs $14$ epochs on average.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{syntheticExpensiveWeights}
    \caption{Evolution of the train and test RMSE for the ``Random shuffling'' and ``Weighted shuffling'' methods, described in Subsections~\ref{subsec:shufflingMethods} and~\ref{subsec:importanceExpensive} respectively.
              The synthetic data consists of a small matrix of size $200 \times 200$, which is known to be of rank $r = 4$.
              The OS ratio is set to $4$.
              The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.01$.
              The weights are updated after every epoch.
              The algorithm is stopped when either the MSE is less than $10^{-8}$ or the number of epochs exceeds $100$.
              The experiment is repeated five times, and the results are averaged.
              The weighted method is a lot slower than the random method, but it needs fewer epochs: the epochs are more expensive but also more accurate.}
    \label{fig:syntheticExpensiveWeights}
\end{figure}

%\todo[inline]{check graph to put x instead of o}

Since this method is very expensive, we will not consider it, and we focus on the cheaper alternative described next.

\subsection{A cheaper method} \label{subsec:importanceCheap}
This method can work for larger data sets.
As always, let us consider one data point, i.e., one pair $\parent{i, j} \in \Omega$, and let us use $n \in \squared{N}$ to denote this data point.
At epoch $k$, we use $\matr{X}_{k} = \matr{L}_{k} \matr{R}_{k}^{\top}$ to denote the current iterate, and $X_{ij}^{k}$ to denote the prediction for entry $\parent{i, j}$.
We define a new variable $\delta^{k}_{ij} = \abs{M_{ij} - X^{k}_{ij}}$ to measure the distance between the true value and the predicted value of data point $n$ after epoch $k$.
If $\delta^{k}_{ij}$ is small, then we guess that data point $n$ has ``done its job'', and it will not enable us to decrease the cost function anymore.
If however $\delta^{k}_{ij}$ is large, then the heuristic tells us that data point $n$ ``still has some job to do'', and it will surely enable us to decrease the cost function.
Hence, the $N$-dimensional vector $\matr{\delta}^{k}$ is computed to see which data points are most likely to decrease the cost function.

%\todo[inline]{Confusing to use $\delta$ twice ?}

Let us fix one iteration $k$.
If one was trying to define the optimal weights, the following problem would need to be solved
\begin{equation*}
%\label{LRMC:prob}
  \begin{aligned}
    \matr{w}^{\star} \parent{\matr{X}}
    = \  & \underset{\matr{w} \in \R^{N}} \argmax
         &                                         & \dfrac{1}{2} \sum_{\parent{i, j} \in \Omega} w_{ij} \parent{\delta_{ij}^{k}}^{2} \\
         & \text{subject to}
         &                                         & \sum_{\parent{i, j} \in \Omega} w_{ij} = 1, \\
         & \ 
         &                                         & w_{ij} \geq 0 \ \forall \parent{i, j} \in \Omega.
  \end{aligned}
\end{equation*}

First, notice that the search space $\R^{N}$ can be simplified to $\squared{0, 1}^{N}$.
It is well-known that the solution to this problem is very sparse.
Indeed, let us define the pair $\parent{i', j'} \in \Omega$ such that $\delta^{k}_{i'j'} \geq \delta^{k}_{ij} \ \forall \parent{i, j} \in \Omega$.
%Equivalently, we can define the pair $\parent{i', j'} \in \Omega$ such that $\delta^{2}_{i'j'} \geq \delta^{2}_{ij} \ \forall \parent{i, j} \in \Omega$, since $\delta_{ij} \geq 0 \ \forall \parent{i, j} \in \Omega$.
In other words, the pair $\parent{i', j'}$ denotes the data point such that the predicted value $X_{i'j'}$ and the true value $M_{i'j'}$ are the furthest apart.
Then, the solution has the form
\begin{equation*}
  w_{ij}^{\star} \parent{\matr{X}}
  =
  \begin{cases}
    1,   & \text{if } \parent{i, j} = \parent{i', j'}, \\
    0 ,  & \text{otherwise}.
  \end{cases}
\end{equation*}

If we effectively use this weight vector, we end up choosing only one data point, which is rather extreme.
Hence, the vector $\matr{\delta}$ is sorted in \emph{decreasing} order, and the $\beta$ first data points are selected in this list.
These data points correspond to the ones that have the largest distance between the predicted value and the true value. 
Therefore, they correspond to the data points that ``still have some job to do'' according to the heuristic that we propose. 
A non-zero weight $w_{ij}$ is given to these points.
This weight can be attributed in several ways, let us simply choose two: either we set $w_{ij} = 1$, or we give a weight proportional to $\delta_{ij}$, i.e., $w_{ij} \sim \delta_{ij}$.
In the sequel, unless stated otherwise, we consider $w_{ij} = \parent{\delta_{ij}}^{1/2}$.
Of course, after the computation of the weights, vector $\matr{w}$ is rescaled such that $\sum_{n = 1}^{N} w_{n} = 1$.
With this vector, epoch $k$ starts by constructing a vector of size $N$ with random data points, but knowing that data point $n$ has a probability $w_{n}$ of being chosen.
Of course, the weights need not be updated after every epoch.
%We see that if $\beta$ is small, then the weight vector $\matr{w}$ is sparse. 

In Figure~\ref{fig:jokesWeights}, the Jester data set is considered.
We decide to update the weights after five iterations, and we use $\beta = \frac{3N}{4}$. 
This means that only $75 \%$ of the vector $\matr{w}$ has a non-zero value. 
As stated above, we suggest two ways of defining the weights:
Figures~\ref{fig:jokesWeightsOne} and~\ref{fig:jokesWeightsDelta} show the results when $w_{ij} = 1$ and $w_{ij} = \parent{\delta_{ij}}^{1/2}$, respectively.
The results are similar:
for the two ways of defining $w_{ij}$, the weighted shuffling method and the random shuffling method perform equally well on the test set and on the train set.
If we take $200$ users instead of $2000$, then it appears that the weighted shuffling performs better than the random shuffling for the test set, and the inverse is true for the train set.
However, we can say that with the Jester data set, the cheap weighted method does not improve the convergence rate:
the curve of the weighted shuffling, i.e., the red curve, decreases more slowly at the beginning, but joins the curve of the random shuffling, i.e., the blue curve, after some epochs.
%, the random shuffling is slightly better.
%The testing error had the same behavior as the training error, which is why we have omitted it. 


\begin{figure}[h!]
\centering
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{jokesWeightsOne}
  \caption{$w_{ij} = 1$}
  \label{fig:jokesWeightsOne}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{jokesWeightsDelta}
  \caption{$w_{ij} = \parent{\delta_{ij}}^{1/2}$}
  \label{fig:jokesWeightsDelta}
\end{subfigure}
\caption{Evolution of the train and test RMSE for the ``Random shuffling'' and ``Weighted shuffling'' methods, described in Subsections~\ref{subsec:shufflingMethods} and~\ref{subsec:importanceCheap} respectively.
          The Jester data set comes from~\cite{Goldberg:2001:ECT:593963.594023}, and $2000$ users are randomly selected. 
          The rank is fixed to $r = 5$ according to Subsection~\ref{subsec:rank:cv}.
          The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.01$.
          The weights are updated after every five epochs, with $\beta = \frac{3N}{4}$.
          The algorithm is stopped when either the MSE is less than $10^{-8}$ or the number of epochs exceeds $50$.
          The experiment is repeated five times, and the results are averaged.
          Marginally, the weighted shuffling attains a lower test RMSE than the random shuffling, but the inverse is true for the train RMSE.
          Therefore, it is fair to say that with larger matrices, the two curves coincide, both for $w_{ij} = 1$ and for $w_{ij} = \parent{\delta_{ij}}^{1/2}$.
          }
\label{fig:jokesWeights}
\end{figure}




The next thing that we want to analyze is the size of $\beta$.
In theory, the optimal choice would be $\beta = 1$, i.e., all the weights are equal to zero, except $w_{i'j'}$, where the pair $\parent{i', j'}$ has been defined above.
However, simulations have shown that this does not work in practice.
Hence, we want to know the best value for $\beta$, i.e., the best number of data points that have to be given a non-zero weight.
A large value of $\beta$ means that the selection is rather lax, i.e., a lot of data points are assigned a non-zero weight, whereas a small value of $\beta$ indicates a more selective process.


We know from Figure~\ref{fig:jokesWeights} that it is hard to achieve a RMSE lower than $3.5$ for the training error on the Jester data set.
Therefore, the tolerance for the MSE is set to $15 > 12.25 = 3.5^{2}$.
The time needed for several methods to attain this tolerance level is recorded.
The procedure is repeated five times, the results are averaged and depicted in Table~\ref{tab:timingWeights}.
Of course, in order to compare with previously defined methods, the timing is also given for the ``Random shuffling'' method: 94.7267 seconds, with the specifications given in the caption of Table~\ref{tab:timingWeights}. 
These results are not surprising: indeed, we have seen in Figure~\ref{fig:jokesWeights} that ``Random shuffling'' is usually better than ``Weighted shuffling'' on the training set, but the latter is better on the testing set.

\begin{table}[h!]
\centering 
\begin{tabular}{l|c|c} 
\rowcolor{lightgray}  $\beta$   & $w_{ij} = 1$        & $w_{ij} \sim \delta_{ij}$     \\ \hline 
                      $N/4$     & $\infty$            & $\infty$                      \\
\rowcolor{lightgray}  $N/2$     & 183.2642            & 352.8762                      \\ 
                      $3N/4$    & $\matr{97.7030}$    & 123.1840                      \\
\rowcolor{lightgray}  $N$       & 102.1952            & 104.3330                      \\ 
\end{tabular} 
\caption{Timing in seconds to reach $15$ for the train MSE, defined in~\eqref{eq:MSE}, for scaled SGD with ``Weighted shuffling'' method described in Subsection~\ref{subsec:importanceExpensive}.
          The Jester data set comes from~\cite{Goldberg:2001:ECT:593963.594023}, and $2000$ users are randomly selected. 
          The rank is fixed to $r = 5$ according to Subsection~\ref{subsec:rank:cv}.
          The initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are populated with random entries drawn from the standard Gaussian distribution.
          The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.01$.
          The weights are updated after every five epochs.
          We define $w_{ij} \sim \delta_{ij}$ as $w_{ij} = \parent{\delta_{ij}}^{1/2}$.
          The algorithm is stopped when either the MSE is less than $15$ or the number of epochs exceeds $5000$.
          The experiment is repeated five times, and the results are averaged.
          It appears that the fastest method corresponds to $w_{ij} = 1$ with $\beta = \frac{3N}{4}$.
          The ``Random shuffling'' described in Subsection~\ref{subsec:shufflingMethods} converges in 94.7267 seconds, which is slightly better than ``Weighted shuffling''.
          We see that when $\beta = N/4$, the algorithm never converges, i.e., the selection is too selective, hence we do not consider enough data points.
          } 
\label{tab:timingWeights} 
\end{table} 






%%%%%%%%%%%%%%%

\begin{comment}
\begin{table}
    \centering
    \begin{tabular}{|c|c||c|c|}
        \hline
        \multicolumn{2}{|c}{Weights $w = 1$} & \multicolumn{2}{c|}{Weights $w = \delta$} \\
        \hline \hline
        \rowcolor{lightgray} $\beta$     & Timing (s)  & $\beta$     & Timing (s)\\ \hline 
        $N/4$  & 16.2435 & $N/4$  & 16.2435    \\ 
        \rowcolor{lightgray} $2N/4$    & 10.0076 & $N/4$  & 16.2435    \\ 
        $3N/4$  & 6.3329  & $N/4$  & 16.2435  \\ \rowcolor{lightgray} 
        $N$    & 5.0362   & $N/4$  & 16.2435 \\ 
        \hline
    \end{tabular}
    \caption{Multiprogram sets}
    \label{multiprogram}
\end{table}
\end{comment}

%%%%%%%%%%%%%%%

As a side note, the curious reader might see the equivalence of this weighted method with the Iteratively Reweighted Least Squares (IRLS) method.
\nomenclature{IRLS}{Iteratively Reweighted Least Squares}
Indeed, a weight has been assigned to each observation $\parent{i, j} \in \Omega$.
This weight depends on the matrix $\matr{X}$.
We use $w_{ij} \parent{\matr{X}}$ to denote the weight of data point $\parent{i, j}$.
Thus, the problem becomes
\begin{equation*}
  \min_{\matr{X} \in \M_{r}} \frac{1}{2} \sum_{\parent{i, j} \in \Omega} w_{ij} \parent{\matr{X}} \parent{M_{ij} - X_{ij}}^{2},
\end{equation*}
and the algorithm creates a sequence of iterates $\bracket{\matr{X}^{\parent{k}}}_{k=1}^K$ by solving
\begin{equation*}
  \matr{X}^{\parent{k+1}} = \argmini{\matr{X} \in \M_{r}} \frac{1}{2} \sum_{\parent{i, j} \in \Omega} w_{ij} \parent{\matr{X}^{\parent{k}}} \parent{M_{ij} - X_{ij}}^{2}.
\end{equation*}



%%%%%%%%%%%%%%%

\begin{comment}
\paragraph{Iteratively Reweighted Least Squares (IRLS)}
\todo[inline]{Too many we's.}
As seen in Proposition~\ref{prop:ls}, the LRMC problem can be written as a least squares problem:
\begin{equation*}
  \min_{\matr{x} \in \R^{d_{1} d_{2}}} \norm{\matr{A} \matr{x} - \matr{b}}_{2}^{2} 
\end{equation*}
with $\matr{A}$ is of size $\abs{\Omega} \times d_{1} d_{2}$.
Since $\abs{\Omega} \ll d_{1} d_{2}$, the system is said to be \emph{under-specified}.
If $\matr{A}$ was full-column rank, then the solution would be
\begin{equation*}
  \matr{x} = \matr{A}^{\top} \squared{\matr{A} \matr{A}^{\top}}^{-1} \matr{Ab}
\end{equation*}
since the matrix $\matr{A}$ is not full-column rank, we could find generalized solutions exist using the Moore-Penrose pseudo-inverse, as it is explained in~\cite{moorepenrose72}.
We are often tempted to solve a weighted version instead.
As it has been shown in Proposition~\ref{prop:ls}, the  problem can be written as a least squares problem:
\begin{equation*}
  \min_{\matr{X} \in \M_{r}} \sum_{\parent{i, j} \in \Omega} \parent{M_{ij} - X_{ij}}^{2}.
\end{equation*}
We are tempted to define a weight for each observation $\parent{i, j} \in \Omega$.
This weight depends on the matrix $\matr{X}$.
We will use $w_{ij} \parent{\matr{X}}$ to denote the weight of data point $\parent{i, j}$.
Hence the problem becomes
\begin{equation*}
  \min_{\matr{X} \in \M_{r}} \sum_{\parent{i, j} \in \Omega} w_{ij} \parent{\matr{X}} \parent{M_{ij} - X_{ij}}^{2}.
\end{equation*}
The algorithm will create a sequence of iterates  $\bracket{\matr{X}^{\parent{k}}}_{k=1}^K$ by solving
\begin{equation*}
  \matr{X}^{\parent{k+1}} = \argmini{\matr{X} \in \M_{r}} \sum_{\parent{i, j} \in \Omega} w_{ij} \parent{\matr{X}^{\parent{k}}} \parent{M_{ij} - X_{ij}}^{2}
\end{equation*}
\end{comment}

%%%%%%%%%%%%%%%


Up to now, both in Section~\ref{sec:SGDB:shuffling} and in this section, we have always considered iterations of $N$ data points. 
This does not have to be the case.
Therefore, a last shuffling idea, called \emph{batch SGD}, is described in the next section.




