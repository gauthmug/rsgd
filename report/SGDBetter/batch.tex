%\todo[inline]{Too many we's}



Up to now, the batch $\B$ contained a single data point.
In Section~\ref{sec:SGDB:shuffling}, four sequences of $N$ data points are discussed, but only epochs were considered, i.e., passes through all the data points.
At every epoch, one data point is visited at a time, in some specific order.
Section~\ref{sec:SGDB:importance} introduces two new methods where $N$ data points are chosen with repetition among $N$ data points, hence some data points are chosen more than once, while others are not chosen at all.
We cannot really talk about an epoch in that case since \emph{all} the data points are not visited, but the algorithm still browses $N$ data points.





In this section, we do not consider epochs anymore.
Instead, we consider iterations such that at iteration $k$, $b_{k}$ data points are visited, with $b_{k} = \abs{\B_{k}} \leq N$.
It has been shown previously that, as the batch-size increases, the error in the sampled gradient decreases, and so the batch-size can be used to implicitly control the error in the gradient.
%Some people prefer to talk about the sample instead of the batch-size, but these two names are equivalent.
Formally, using the notation of the introduction of Chapter~\ref{ch:SGD}, the direction $g_{k}$ is defined as
\begin{equation*}
  g_{k} = \nabla f \parent{x_{k}} + e_{k},
\end{equation*}
where $e_{k}$ is the error, and the search direction is defined as $p_{k} = - g_{k}$.
It is clear that if $\B_{k} = \squared{N}$, then $e_{k} = 0$.
At this point, it is important to mention that the number of iterations cannot be used unequivocally anymore to show the evolution of the error, because one iteration for method A can be very different from one iteration for method B.
Indeed, if method A takes $b_{k} = N$ and method B takes $b_{k} = 1$, then iteration $k$ is cheaper for method B, but it is less precise.


If only $b_{k}$ data points are considered at every iteration, the easiest idea is to fix the batch-size $b_{k} = b$ for every iteration $k$.
Then, one might ask how to determine the best value for $b$.
In order to determine this, a small experiment is run, from which the results are displayed on Figure~\ref{fig:constant}.
We work with a square matrix of size $1000 \times 1000$, and rank known to be $r = 10$.
The OS ratio is set to $6$.
The ``Constant batch-size'' method is compared with ``Random shuffling'' described on item~\ref{item:randomWithout} in Section~\ref{sec:SGDB:shuffling}.
In subfigure~\ref{fig:smallConstant} it can be seen that when the value of $b$ is increased, convergence is achieved faster. 
Hence, it would make sense to take the largest possible value for $b$, i.e., $b = N$, where $N$ is the number of data points.
However, subfigure~\ref{fig:largeConstant} shows that when $b$ increases too much, the convergence is slower.
Hence, taking $b = \frac{N}{10}$ seems to be a sweet spot.

\begin{figure}
\centering
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{smallConstant}
  \caption{Small values of $b$.}
  \label{fig:smallConstant}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
  \centering
  \includegraphics[width=0.99\linewidth]{largeConstant}
  \caption{Large values of $b$.}
  \label{fig:largeConstant}
\end{subfigure}
\caption{Evolution of the training error as a function of computation time.
         The ``Constant batch-size'' defined in Section~\ref{sec:SGDB:batch} is compared to ``Random shuffling'' described on item~\ref{item:randomWithout} in Section~\ref{sec:SGDB:shuffling}, for the scaled SGD method.
         We consider a matrix of size $1000 \times 1000$, rank known to be $10$ and OS ratio set to $6$.
         Initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are randomly generated according to a standard Gaussian distribution.
         The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.1$.       
         It appears that a trade-off in the size of the batch has to be found.
         }
\label{fig:constant}
\end{figure}

Of course, other methods can also be thought of, since $b_{k} = b$ for every $k$ is not a very rich model.
Another choice is to let $b_{k}$ take a random integer value between $1$ and $N$ at every iteration.
This gives rise to a method called ``Random batch-size''. 

Finally, a choice inspired from~\cite{FriedlanderSchmidt2012} is based on an increasing batch-size strategy.
The name ``Increasing batch-size'' is used to denote this method.
The idea is to bridge the gap between two ends of a spectrum, where $b_{k} = 1$ is at the end of ``cheap iterations with slow convergence'', and $b_{k} = N$ is ``expensive iterations with fast convergence''.



Let us summarize: we have defined three new methods that do not consider epochs anymore.
At iteration $k$, an integer $b_{k}$ between $1$ and $N$ is defined.
Then, $b_{k}$ data points are chosen, and only the rows of $\matr{L}$ and $\matr{R}$ corresponding to these data points are updated.
In Figure~\ref{fig:hybrid}, page~\pageref{fig:hybrid}, these three methods are compared to ``Random shuffling'' described on item~\ref{item:randomWithout} in Section~\ref{sec:SGDB:shuffling}.
We see that the hybrid batch methods are faster, and the ``Increasing batch-size'' has the fastest convergence. 

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{hybrid}
    \caption{Evolution of the training error as a function of computation time.
              The scaled SGD method is applied with four different methods for the batch-size.
              The ``Random shuffling'' is described on item~\ref{item:randomWithout} in Section~\ref{sec:SGDB:shuffling}.
              The ``Constant batch-size'' takes $b_{k} = b = \frac{N}{10}$ for every $k$.
              The ``Random batch-size'' takes $b_{k}$ at random between $1$ and $N$ for every $k$.
              The ``Increasing batch-size'' starts with $b_{1} = 1$, then linearly adds some factor $\Delta$, which defines an arithmetic sequence: $b_{k} = b_{k-1} + \Delta$.
              The factor $\Delta$ is called the common difference, and is set to $\Delta = \frac{M}{N}$ where $M$ is the maximal number of iterations, set to $100$.
              Initial matrices $\matr{L}_{0}$ and $\matr{R}_{0}$ are randomly generated according to a standard Gaussian distribution.
              We consider a matrix of size $1000 \times 1000$, rank known to be $10$ and OS ratio set to $6$.
              The bold driver protocol described at item~\ref{item:bold} on page~\pageref{item:bold} is used, with an initial value of $0.1$.       
              The algorithm is stopped when either the MSE is less than $10^{-8}$ or the number of epochs exceeds $100$.
}
    \label{fig:hybrid}
\end{figure}


\begin{comment}
In Figure~\ref{fig:hybrid}, page \pageref{fig:hybrid}, we see that the standard method converges faster than the hybrid method.
As a reminder, the standard method goes through all the data points at every iteration, while the hybrid method only goes through $b_{k}$ data points it iteration $k$, with $b_{k}$ going from $1$ to $N$ linearly.
It seems that the standard method is better.
However, let us look at the computation time, in Table~\ref{tab:timing}.
It is clear that the hybrid method is way faster. 
We remind that all the Figures and all the Tables of this document can be recreated with MATLAB 2015b, and the code from \url{https://bitbucket.org/gauthmug/rsgd/src/master/}. 
\end{comment}


\begin{comment}
\begin{table} 
\centering 
\begin{tabular}{l|l} 
\rowcolor{lightgray} 
Method  & Timing (s)  \\ \hline 
Standard & 129.4801    \\ \rowcolor{lightgray} 
Hybrid & 5.0162    \\ 
\end{tabular} 
\caption{Timing} 
\label{tab:timing} 
\end{table} 
\end{comment}


