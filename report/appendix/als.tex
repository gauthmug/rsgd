
For simplicity, let us first assume that all the entries of $\matr{M}$ are sampled, i.e., $\Omega = \squared{d_{1}} \times \squared{d_{2}}$.
Then the objective function~\eqref{eq:ObjectiveLR} becomes
\begin{equation*}
  f \parent{\matr{L}, \matr{R}} = \dfrac{1}{2} \sum_{i = 1}^{d_{1}} \sum_{j = 1}^{d_{2}} \parent{M_{ij} - \matr{L}_{i:} \matr{R}_{j:}}^{2}
                                = \dfrac{1}{2} \norm{\matr{M} - \matr{L} \matr{R}^{\top}}_{F}^{2}.
\end{equation*}

The idea of the alternating least squares (ALS) method is to use coordinate descent to minimize the cost.
The approach is thus to minimize with respect to $\matr{L}$, while $\matr{R}$ is fixed;
then to minimize with respect to $\matr{R}$, while $\matr{L}$ is fixed: this is the \emph{alternating} part.
This two-step dance is repeated until convergence is attained. 
In order to find the update rules, the following system is solved
\begin{equation}
\label{sys:optimality}
    \begin{cases}
       \nabla_{\matr{L}} f\parent{\matr{L^{*}}, \matr{R}} = 0, \\
       \nabla_{\matr{R}} f\parent{\matr{L}, \matr{R}^{*}} = 0.
    \end{cases}
\end{equation}

We find
\begin{equation}
\label{eq:solFull}
    \begin{cases}
       \matr{L^{*}} = \matr{M} \matr{R} \parent{\matr{R}^{\top} \matr{R}}^{-1} , \\
       \matr{R^{*}} = \matr{M}^{\top} \matr{L} \parent{\matr{L}^{\top} \matr{L}}^{-1}.
    \end{cases}
\end{equation}



\begin{comment}
\textcolor{red}{It is well-known that the multiplication of a matrix of size $d_{1} \times d_{2}$ with a matrix of size $d_{2} \times d_{3}$ gives a matrix of size $d_{1} \times d_{3}$.}
This can be done in $\O{d_{1} d_{2} d_{3}}$. 
Furthermore, inverting a matrix of size $d \times d$, has a complexity of $\O{d^{3}}$ if we use Gauss-Jordan elimination, and $\O{d^{2.807}}$	if we use Strassen's algorithm.
Recent authors like Coppersmith, Winograd and Le Gall~\cite{DBLP:journals/corr/Gall14} have managed to even lower the complexity to $\O{d^{2.3728639}}$.
However, this is not so important, as we will see later. 

Hence, computing $\matr{L^{*}}$ costs $\O{r\parent{r^{2} + r \ubar{d} + d_{1} d_{2}}}$. 
The complexity of $\matr{R^{*}}$ is identical. 
We see that the term $r d_{1} d_{2}$ is way larger than $r^{3}$, hence it is not important to look for the cheapest method of inverting the square $r \times r$ matrix. 
\end{comment}

Of course, we usually do not have $\Omega = \squared{d_{1}} \times \squared{d_{2}}$.
Thus, the objective function is~\eqref{eq:ObjectiveLR} and the system~\eqref{sys:optimality} needs to be solved again.
The solution will not be as elegant as~\eqref{eq:solFull} since all the entries are not sampled in real situations.
Therefore, some notations are introduced.


First, let us fix an index $i$.
We say that $\sum_{\parent{\cdot, j} \in \Omega} M_{ij}$ is the sum of all the entries $M_{ij}$ such that $\parent{i, j} \in \Omega$. 
Hence, the dot in $\parent{\cdot, j}$ means that the first index is fixed, and that the sum is taken over the second index. 
Similarly, when an index $j$ is fixed, the notation $\sum_{\parent{i, \cdot} \in \Omega} M_{ij}$ has the same interpretation: the second index is fixed, and the sum is taken over the first index.
Solving~\eqref{sys:optimality} gives
\begin{equation*}
    \begin{cases}
       \matr{L^{*}}_{i:} = \sum_{\parent{\cdot, j} \in \Omega} M_{ij} \matr{R}_{j:} \squared{\sum_{\parent{\cdot, j} \in \Omega} \matr{R}_{j:}^{\top} \matr{R}_{j:}}^{-1}, \\
       \matr{R^{*}}_{j:} = \sum_{\parent{i, \cdot} \in \Omega} M_{ij} \matr{L}_{i:} \squared{\sum_{\parent{i, \cdot} \in \Omega} \matr{L}_{i:}^{\top} \matr{L}_{i:}}^{-1}. \\
    \end{cases}
\end{equation*}

The advantage of ALS is that there are several ways to distribute its computation, as it is stated in~\cite{10.1007/978-3-540-68880-8_32}.







