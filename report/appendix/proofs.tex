

\section{Proof of Proposition~\ref{prop:equivalenceRelation}} \label{app:proofEquivalenceRelation}


\begin{proof}
  Three properties need to be verified. 
    \begin{enumerate}
      \item Reflexive: $\parent{\matr{L}, \matr{R}} \thicksim \parent{\matr{L}, \matr{R}}$ for all $\parent{\matr{L}, \matr{R}} \in \R^{d_{1} \times r}_{*} \times \R^{d_{2} \times r}_{*}$. \\
            It suffices to take $\matr{T} = \matr{I}_{r \times r}$.
      \item Symmetric: $\parent{\matr{L}, \matr{R}} \thicksim \parent{\matr{G}, \matr{H}} \Leftrightarrow \parent{\matr{G}, \matr{H}} \thicksim \parent{\matr{L}, \matr{R}}$
                        for all $\parent{\matr{L}, \matr{R}} \in \R^{d_{1} \times r}_{*} \times \R^{d_{2} \times r}_{*}$
                        and $\parent{\matr{G}, \matr{H}} \in \R^{d_{1} \times r}_{*} \times \R^{d_{2} \times r}_{*}$. \\
            This has to be proved in two ways: $\parent{\Rightarrow}$ and $\parent{\Leftarrow}$.
            We will only prove the first part $\parent{\Rightarrow}$, since the second part follows by symmetry: a simple relabeling suffices. \\
            If $\parent{\matr{L}, \matr{R}} \thicksim \parent{\matr{G}, \matr{H}}$, then there exists a matrix $\matr{T} \in \text{GL}\parent{r}$ such that $\matr{G} = \matr{L} \matr{T}^{-1}$ and $\matr{H} = \matr{R} \matr{T}^{\top}$.
            Let us define the matrix $\matr{U}$ as $\matr{U} = \matr{T}^{-1}$.
            It is clear that $\matr{U} \in \text{GL}\parent{r}$.
            We have $\matr{L} = \matr{G} \matr{U}^{-1}$ and $\matr{R} = \matr{H} \matr{U}^{\top}$.
            This gives $\parent{\matr{G}, \matr{H}} \thicksim \parent{\matr{L}, \matr{R}}$.
      \item Transitive: if $\parent{\matr{L}, \matr{R}} \thicksim \parent{\matr{G}, \matr{H}}$
                        and $\parent{\matr{G}, \matr{H}} \thicksim \parent{\matr{A}, \matr{B}}$
                        then $\parent{\matr{L}, \matr{R}} \thicksim \parent{\matr{A}, \matr{B}}$
                        for all $\matr{L}, \matr{G}, \matr{A} \in \R^{d_{1} \times r}_{*}$
                        and $\matr{R}, \matr{H}, \matr{B} \in \R^{d_{2} \times r}_{*}$. \\
            There exist matrices $\matr{T}$ and $\matr{U}$, both in $\text{GL} \parent{r}$, such that
            $\matr{G} = \matr{L} \matr{T}^{-1}, \matr{H} = \matr{R} \matr{T}^{\top}$
            and
            $\matr{A} = \matr{G} \matr{U}^{-1}, \matr{B} = \matr{H} \matr{U}^{\top}$.
            Let us define the matrix $\matr{V}$ as $\matr{V} = \matr{U} \matr{T}$.
            We see that $\matr{V} \in \text{GL} \parent{r}$.
            Furthermore, we have $\matr{V}^{\top} = \matr{T}^{\top} \matr{U}^{\top}$
            and $\matr{V}^{-1} = \matr{T}^{-1} \matr{U}^{-1}$.
            Hence, we have
            $\matr{A} = \matr{L} \matr{T}^{-1} \matr{U}^{-1} = \matr{L} \matr{V}^{-1}$ and
            $\matr{B} = \matr{R} \matr{T}^{\top} \matr{U}^{\top} = \matr{R} \matr{V}^{\top}$.
            Therefore, we can write
            $\parent{\matr{L}, \matr{R}} \thicksim \parent{\matr{A}, \matr{B}}$.
    \end{enumerate}
    Since the three properties of an equivalence relation are verified, the proof is complete. 
\end{proof}







\section{Proof of Proposition~\ref{prop:ls}} \label{app:proofEquivalentObjective}

\begin{proof}
  The objective variable is a matrix $\matr{X}$ of size $d_{1} \times d_{2}$ and of rank $r$.
  Hence, this matrix can, in theory only, be represented by a vector $\matr{x}$ of size $d_{1} d_{2} \times 1$.
  Let us use $N = \abs{\Omega}$ to denote the number of known entries, as usual. 
  We define a vector $\matr{b}$ that contains all the sampled elements of $\matr{M}$, column-wise.
  Thus, $\matr{b}$ is a vector of size $N \times 1$.
  We also define a matrix $\matr{A}$ of size $N \times d_{1} d_{2}$.
  The entries of $\matr{A}$ are only binary, i.e., they are either equal to $0$ or $1$.
  In order to find out which entries are equal to $1$, let us pick an element $\parent{i, j} \in \Omega$, and let us assume that the corresponding entry $M_{ij}$ is represented at index $n$ of vector $\matr{b}$, i.e., $M_{ij} = b_{n}$.
  It must be that, using MATLAB notation, $\matr{A}_{n:} \matr{x} = X_{ij}$.
  In other words, on row $n$, the matrix $\matr{A}$ is zero everywhere, except at the column corresponding to the entry $X_{ij}$.
  
  \begin{itemize}
    \item On every row of $\matr{A}$, there is \emph{exactly} one entry that is equal to $1$, while all the other entries are equal to $0$.
    \item On every column of $\matr{A}$, there is \emph{at most} one entry that is equal to $1$, while all the other entries are equal to $0$:
    \begin{itemize}
      \item If, on the one hand, the column corresponds to an entry $X_{ij}$ such that $\parent{i, j} \in \Omega$, then this column has \emph{exactly} one entry that is equal to $1$.
      \item If, on the other hand, the column corresponds to an entry $X_{ij}$ such that $\parent{i, j} \not \in \Omega$, then this column has only zero entries.
    \end{itemize}
  \end{itemize}
  
  
  With these definitions of $\matr{A}$ and $\matr{b}$, the objective function~\eqref{eq:OmegaObjective} can be written as
  \begin{equation*}
    f \parent{\matr{x}} = \dfrac{1}{2} \sum_{n = 1}^{N} \parent{\matr{A}_{n:} \matr{x} - b_{n}}^{2}
                        = \dfrac{1}{2} \norm{\matr{A} \matr{x} - \matr{b}}_{2}^{2}.
  \end{equation*}

  Since the quantity $\matr{A} \matr{x} - \matr{b}$ is a vector, the Frobenius norm is not needed anymore, but of course it is just the vector version of the $\ell_{2}$ norm.
  This concludes the proof. 
\end{proof}
