\section{Rank of a matrix} \label{appSec:rank}

Let us use $\matr{A}$ to denote a real matrix of size $d_{1} \times d_{2}$.
If $\matr{A}$ is decomposed as columns, we write
\begin{equation}
\label{eq:colsA}
  \matr{A}
  =
  \begin{pmatrix}
      \matr{A}_{:1} & \dots & \matr{A}_{:d_{2}} 
  \end{pmatrix},
\end{equation}
with $\matr{A}_{:j} \in \R^{d_{1} \times 1}$ for $j = 1, \dots, d_{2}$.
Similarly, if $\matr{A}$ is decomposed as rows, we write
\begin{equation*}
  \matr{A}
  =
  \begin{pmatrix}
      \matr{A}_{1:}^{\top}       \\
      \vdots       \\
      \matr{A}_{d_{1}:}^{\top}      
  \end{pmatrix},
\end{equation*}
with $\matr{A}_{i:} \in \R^{d_{2} \times 1}$ for $i = 1, \dots, d_{1}$.


The column space of $\matr{A}$ is defined by
\begin{equation*}
  \mathcal{C} \parent{\matr{A}} = \newspan{\matr{A}_{:1}, \dots, \matr{A}_{:d_{2}}},
\end{equation*}
where the span of a collection of vectors is the set of all possible linear combinations of these vectors.
Similarly, the row space of $\matr{A}$ is defined by
\begin{equation*}
  \mathcal{R} \parent{\matr{A}} = \newspan{\matr{A}_{1:}, \dots, \matr{A}_{d_{1}:}}.
\end{equation*}


The \emph{range} of $\matr{A}$, also called the \emph{image}, is defined by
\begin{equation*}
  \text{ran} \parent{\matr{A}} = \bracket{y \in \R^{d_{1} \times 1} : \exists x \in \R^{d_{2} \times 1}, y = \matr{A} x} = \mathcal{C} \parent{\matr{A}}.
\end{equation*}

The \emph{null space} of $\matr{A}$, also called the \emph{kernel}, is defined by
\begin{equation*}
  \text{null} \parent{\matr{A}} = \bracket{x \in \R^{d_{2} \times 1} : \matr{A} x = 0}.
\end{equation*}

The \emph{column rank} is defined as the dimension of the column space, and the \emph{row rank} as the dimension of the row space.
A fundamental result in linear algebra is that these two ranks are always equal.
Therefore, this number is simply called the \emph{rank} of the matrix $\matr{A}$.


%It can be shown that $\rank \parent{\matr{A}} = \rank \parent{\matr{A}^{\top}}$.
%Since the column space of $\matr{A}^{\top}$ is the row space of $\matr{A}$, the proof is straightforward. 

%Let $d_{\min}$ denote the lesser of the number of rows and columns, i.e., $\ubar{d} = \min \bracket{d_{1}, d_{2}}$.
The matrix $\matr{A}$ is said to be full rank if $\rank \parent{\matr{A}} = \min \bracket{d_{1}, d_{2}}$, and 
$\matr{A}$ is said to be rank deficient if $\rank \parent{\matr{A}} < \min \bracket{d_{1}, d_{2}}$.


The famous rank-nullity theorem states that
\begin{equation}
\label{eq:rankNullity}
  \text{nullity} \parent{\matr{A}} + \rank \parent{\matr{A}} = d_{2},
\end{equation}

where the nullity of $\matr{A}$ is simply defined as $\text{nullity} \parent{\matr{A}} = \text{dim} \parent{\text{null} \parent{\matr{A}}}$.


We end this section with an important proposition.

\begin{proposition}
\label{prop:rankApp}
  Let $\matr{M} \in \R^{d_{1} \times d_{2}}$ for some positive integers $d_{1}$ and $d_{2}$.
  Let $r \in \R$ such that $r \leq \min \bracket{d_{1}, d_{2}}$.
  The following statements are equivalent:
  \begin{enumerate}[label={(\arabic*)}]
    \item $\newRank{M} = r$;
    \item $\exists \matr{A} \in \R^{d_{1} \times r}_{*}, \matr{B} \in \R^{d_{2} \times r}_{*}$
          such that $\newRank{A} = \newRank{B} = r$
          and $\matr{M} = \matr{A} \matr{B}^{\top}$.
  \end{enumerate}
\end{proposition}

\begin{proof}
  We have to prove that the two statements are equivalent.
  
  
  $\parent{1} \Rightarrow \parent{2}$ \\
  Let $\matr{A} \in \R^{d_{1} \times r}_{*}$ be a basis of $\matr{M}$.
  In that case, the columns of $\matr{A}$ form a basis of $\mathcal{C} \parent{\matr{M}}$.
  Thus $\newRank{A} = r$, and there exists $\matr{B} \in \R^{d_{2} \times r}$ such that 
  \begin{equation*}
    \begin{split}
      \matr{M} = \matr{A} \matr{B}^{\top} 
      & \Leftrightarrow
            \begin{pmatrix}
                \matr{M}_{:1} & \dots & \matr{M}_{:d_{2}} 
            \end{pmatrix}
            =     
            \begin{pmatrix}
                \matr{A}_{:1} & \dots & \matr{A}_{:r} 
            \end{pmatrix}
            \begin{pmatrix}
                B_{11} & \dots & B_{d_{2} 1} \\
                \vdots &       & \vdots       \\
                B_{1r} & \dots & B_{d_{2} r}
            \end{pmatrix}, \\
      & \Leftrightarrow
            \matr{M}_{:j} = \sum_{k = 1}^{r} B_{jk} \matr{A}_{:k},
            \quad j = 1, \dots, d_{2}.
    \end{split}
  \end{equation*}
  It remains to prove that $\newRank{B} = r$.
  We already know that $\newRank{B} \leq r$, because $\matr{B} \in \R^{d_{2} \times r}$ and $r \leq d_{2}$.
  We also know that 
  \begin{equation*}
    r = \newRank{M} \leq \min \bracket{\newRank{A}, \newRank{B^{\top}}} = \min \bracket{r, \newRank{B}},
  \end{equation*}
  or again
  \begin{equation*}
    r \leq \min \bracket{r, \newRank{B}}.
  \end{equation*}
  From this, it is clear that $\newRank{B} = r$.
  
  \vspace{5mm}
  
  $\parent{2} \Rightarrow \parent{1}$ \\
  As $\newRank{M} \leq \min \bracket{\newRank{A}, \newRank{B}} = r$, we have to prove that $\newRank{M} = r$.
  By using~\eqref{eq:rankNullity}, this is equivalent to proving that $\text{nullity} \parent{\matr{M}} = d_{2} - r$.
  We have
  \begin{equation*}
    \begin{split}
        & \matr{M} \matr{x} = \matr{0} \\
        & \Leftrightarrow \matr{A} \matr{B}^{\top} \matr{x} = \matr{0} \\
        & \Leftrightarrow \matr{B}^{\top} \matr{x} \in \text{ker} \parent{\matr{A}} \\
        & \Leftrightarrow \matr{B}^{\top} \matr{x} = \matr{0} \\ 
        & \Leftrightarrow \matr{x} \in \text{ker} \parent{\matr{B}^{\top}},
    \end{split}
  \end{equation*}
  where the third implication stems from $\text{ker} \parent{\matr{A}} = \bracket{0}$, since $\matr{A}$ has full rank.
  We know that $\text{nullity} \parent{\matr{B}^{\top}} = \text{dim} \parent{\text{ker} \parent{\matr{B}^{\top}}} = d_{2} - r$.
  Therefore, $\text{nullity} \parent{\matr{M}} = d_{2} - r$.
  Hence, $\newRank{M} = r$.
  
  \vspace{5mm}
  
  The proof is complete.
\end{proof}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Matrix norms} \label{appSec:norms}

The theory about matrix norms is vast. 
As a reminder, a function $\norm{\cdot} : \R^{d_{1} \times d_{2}} \mapsto \R$ is called a \emph{matrix norm} on $\R^{d_{1} \times d_{2}}$ if for all $\matr{A}, \matr{B} \in \R^{d_{1} \times d_{2}}$ and all $\alpha \in \R$ we have
\begin{enumerate}
  \item Positivity: $\norm{\matr{A}} \geq 0$, with equality if and only if $\matr{A} = \matr{0}$.
  \item Homogeneity: $\norm{\alpha \matr{A}} = \abs{\alpha} \norm{\matr{A}}$.
  \item Subadditivity: $\norm{\matr{A} + \matr{B}} \leq \norm{\matr{A}} + \norm{\matr{B}}$.
\end{enumerate}

%A matrix norm is simply a vector norm on the finite dimensional vector spaces $\parent{\R^{d_{1} \times d_{2}}, \R}$ of matrices of size $d_{1} \times d_{2}$.
%However, all matrix norms do not have to be seen as norms induced by vector norms.

Three types of norms can be defined, with some special cases.
\begin{enumerate}
  \item Matrix norms induced by vector norms, or operator norms: 
        \begin{equation*}
          \norm{\matr{A}}_{\parent{a, b}} = \sup_{\matr{x} \neq 0} \dfrac{\norm{\matr{A} \matr{x}}_{a}}{\norm{\matr{x}}_{b}},
        \end{equation*}
        where $\matr{x}$ is a vector of size $d_{2} \times 1$,
        hence $\norm{\cdot}_{a}$ denotes a vector norm for vectors of size $d_{1} \times 1$
        and $\norm{\cdot}_{b}$ denotes a vector norm for vectors of size $d_{2} \times 1$.
        Often one takes $a = b = p$, hence $p$ can denote a norm for an object of size $d_{1} \times d_{2}$, of size $d_{1} \times 1$ and of size $d_{2} \times 1$.
  \item Entry-wise matrix norms: 
        \begin{equation*}
          \norm{\matr{A}}_{\ell_p} = \parent{\sum_{i=1,j=1}^{d_{1}, d_{2}} \abs{A_{ij}}^p}^{1/p}.
        \end{equation*}
        The famous Frobenius norm is a special case of the entry-wise matrix norm with $p = 2$.
        We have
        \begin{equation*}
          \norm{\matr{A}}_{\ell_2}
          = \norm{\matr{A}}_{F}
          = \norm{\matr{A}}_{2}
          = \parent{\sum_{i=1,j=1}^{d_{1}, d_{2}} \abs{A_{ij}}^2}^{1/2}.
        \end{equation*}
  \item Schatten norms: 
        \begin{equation*}
          \norm{\matr{A}}_{S_p} = \parent{\sum_{i=1}^{d_{\min}} \sigma_i^p}^{1/p},
        \end{equation*}
        where $\sigma_1, \dots, \sigma_{d_{\min}}$ are the singular values of $\matr{A}$ and $d_{\min}$ is the minimum between $d_{1}$ and $d_{2}$.
        The nuclear norm is a special case of the Schatten norm with $p = 1$. 
        We have
        \begin{equation*}
          \norm{\matr{A}}_{S_1}
          = \norm{\matr{A}}_{*}
          = \sum_{i=1}^{d_{\min}} \sigma_{i}.
        \end{equation*}
\end{enumerate} 



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Derivatives} \label{appSec:derivatives}

This corresponds to~\cite[Appendix~A.5]{absil2009optimization}.
The concept of a derivative for functions between two finite-dimensional normed vector spaces is presented.

Let $\mathcal{E}$ and $\mathcal{F}$ be two finite-dimensional vector spaces over $\R$.
A particular case is $\mathcal{E} = \R^{m}$ and $\mathcal{F} = \R^{n}$.
A function $f : \mathcal{E} \mapsto \mathcal{F}$ is \emph{Fréchet-differentiable} at a point $x \in \mathcal{E}$ if there exists a linear operator
\begin{equation*}
  \mathrm{D} f \parent{x} : \mathcal{E} \to \mathcal{F} : h \mapsto \mathrm{D} f \parent{x} \squared{h},
\end{equation*}
called the \emph{Fréchet differential} or the \emph{Fréchet derivative} of $f$ at $x$, such that
\begin{equation*}
    f \parent{x + h} = f \parent{x} + \mathrm{D} f \parent{x} \squared{h} + o \parent{\norm{h}};
\end{equation*}
in other words,
\begin{equation*}
  \lim_{y \to x} \dfrac{\norm{f \parent{y} - f \parent{x} - \mathrm{D} f \parent{x} \squared{y - x}}}{\norm{y - x}} = 0.
\end{equation*}

The element $\mathrm{D} f \parent{x} \squared{h} \in \mathcal{F}$ is called the \emph{directional derivative} of $f$ at $x$ along $h$.
We use the same notation $\mathrm{D} f \parent{x}$ for the differential of a function $f$ between two manifolds $\M_{1}$ and $\M_{2}$;
then $\mathrm{D} f \parent{x}$ is a linear operator from the vector space $T_{x} \M_{1}$ to the vector space $T_{f \parent{x}} \M_{2}$.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\section{Kronecker product and matrix vectorization} \label{appSec:kronecker}
Let us take two matrices $\matr{A} \in \R^{m \times n}$ and $\matr{B} \in \R^{p \times q}$.
The \emph{Kronecker product} of $\matr{A}$ and $\matr{B}$ is defined as
\begin{equation*}
  \matr{A} \otimes \matr{B}
  =
  \begin{bmatrix}
    A_{11} \matr{B} & \cdots & A_{1n} \matr{B}    \\
    \vdots          & \ddots & \vdots             \\
    A_{m1} \matr{B} & \cdots & A_{mn} \matr{B}
  \end{bmatrix}
  \in \R^{mp \times nq}.
\end{equation*}

More compactly, we have
\begin{equation*}
  (A\otimes B)_{p(i-1)+k, q(j-1)+l} = A_{ij} B_{kl},
\end{equation*}
for every $\parent{i, j} \in \bracket{1, \dots, m} \times \bracket{1, \dots, n}$
and $\parent{k, l} \in \bracket{1, \dots, p} \times \bracket{1, \dots, q}$.

Let us go back to the case where $\matr{A}$ denotes a real matrix of size $d_{1} \times d_{2}$.
If we decompose $\matr{A}$ as columns just like in~\eqref{eq:colsA}, then the $\newVec{\cdot}$ operator creates a column vector from matrix $\matr{A}$ by stacking the columns below one another.
We have
\begin{equation*}
  \newVec{\matr{A}} =
    \begin{bmatrix}
      \matr{A}_{:1}   \\
      \vdots   \\
      \matr{A}_{:n} 
    \end{bmatrix}
    \in \R^{d_{1} d_{2} \times 1}.
\end{equation*}


With this definition, the most important result linking the Kronecker product and the $\newVec{\cdot}$ operator can be stated.
Given three matrices of compatible sizes $\matr{A} \in \R^{d_{1} \times d_{2}}$, $\matr{X} \in \R^{d_{2} \times d_{3}}$ and $\matr{B} \in \R^{d_{3} \times d_{4}}$, we have
\begin{equation*}
  \newVec{\matr{A} \matr{X} \matr{B}} = \parent{\matr{B}^{\top} \otimes \matr{A}} \newVec{\matr{X}}.
\end{equation*}

%%%%%%%%% 

\begin{comment}
A corollary of this theorem is
\begin{equation*}
  \newVec{\matr{A} \matr{B}}
  = \parent{\matr{B}^{\top} \otimes \matr{A}} \newVec{\matr{I}}
  = \parent{\matr{I}        \otimes \matr{A}} \newVec{\matr{B}}
  = \parent{\matr{B}^{\top} \otimes \matr{I}} \newVec{\matr{A}}.
\end{equation*}
\end{comment}

%%%%%%%%%

The most important property of the $\newVec{\cdot}$ operator is
\begin{equation*}
  \newTrace{\matr{A}^{\top} \matr{B} \matr{C} \matr{D}^{\top}}
  = \newVec{\matr{A}}^{\top} \parent{\matr{D} \otimes \matr{B}} \newVec{\matr{C}}.
\end{equation*}

If we take $\matr{B} = \matr{I}$ and relabel $\matr{C}$ and $\matr{D}$, we obtain
\begin{equation}
\label{eq:niceTraceTheorem}
  \newTrace{\matr{A}^{\top} \matr{B} \matr{C}^{\top}}
  = \newVec{\matr{A}}^{\top} \parent{\matr{C} \otimes \matr{I}} \newVec{\matr{B}}.
\end{equation}

For compatible matrices $\matr{X}$ and $\matr{Y}$, we have
\begin{equation*}
  \newTrace{\matr{X} \matr{Y}} = \newTrace{\matr{Y} \matr{X}} = \newTrace{\parent{\matr{X} \matr{Y}}^{\top}},
\end{equation*}
and the full version with three compatible matrices $\matr{X}$, $\matr{Y}$ and $\matr{Z}$ is
\begin{equation*}
  \newTrace{\matr{X} \matr{Y} \matr{Z}} 
  = \newTrace{\matr{Y} \matr{Z} \matr{X}}
  = \newTrace{\matr{Z} \matr{X} \matr{Y}}.
\end{equation*}
More generally, the trace is \emph{invariant under cyclic permutations}.
This can be used to rewrite~\eqref{eq:niceTraceTheorem} as
\begin{equation}
\label{eq:niceTrace}
  \newTrace{\matr{A} \matr{B} \matr{C}}
  = \newVec{\matr{B}^{\top}}^{\top} \parent{\matr{A}^{\top} \otimes \matr{I}} \newVec{\matr{C}}.
\end{equation}

%%%%%%%%%%%%

\begin{comment}
Let us give three corollaries
\begin{enumerate}
  \item \begin{equation*}
    \newTrace{\matr{A} \matr{B} \matr{C}} = \newVec{\matr{A}^{\top}}^{\top} \parent{\matr{I} \otimes \matr{B}} \newVec{\matr{C}}.
  \end{equation*}
  \item \begin{equation*}
    \newTrace{\matr{A}^{\top} \matr{C} \matr{D}^{\top}}
    = \newVec{\matr{A}}^{\top} \parent{\matr{D} \otimes \matr{I}} \newVec{\matr{C}}
  \end{equation*}
  \item \begin{equation*}
    \newTrace{\matr{A} \matr{B}} = \newVec{\matr{A}^{\top}}^{\top} \newVec{\matr{B}}
  \end{equation*}
\end{enumerate}
\end{comment}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Matrix exponential and logarithm of a matrix} \label{appSec:expLog}

Let us use $\mathbb{P}_{n}$ to denote the cone of $n \times n$ symmetric positive definite matrices.
We recall that a matrix $\matr{G} \in \mathbb{P}_{n}$ has the following eigen-decomposition
\begin{equation*}
  \matr{G} = \matr{U} \matr{\Lambda} \matr{U}^{\top},
\end{equation*}
where $\matr{U}$ is an orthogonal matrix containing the eigenvectors, and $\matr{\Lambda}$ is a diagonal matrix containing the eigenvalues.


Then the matrix $\matr{G}^{a}$ is defined for each $a \in \R$ as
\begin{equation*}
  \matr{G}^{a} = \matr{U} \matr{\Lambda}^{a} \matr{U}^{\top},
\end{equation*}
where $\matr{\Lambda}^{a}$ is a diagonal matrix with the diagonal elements of $\matr{\Lambda}$ raised to the power $a$.
In particular if $a = -\frac{1}{2}$, we have
\begin{equation*}
  \matr{G}^{-\frac{1}{2}} = \matr{U} \matr{\Lambda}^{-\frac{1}{2}} \matr{U}^{\top},
\end{equation*}
where $\matr{\Lambda}^{-\frac{1}{2}}$ is computed as the inverse of the square root of the diagonal elements of $\matr{\Lambda}$.


The definition of the logarithm of a scalar can also be extended to a matrix.
Let us define the diagonal matrix $\log \parent{\matr{\Lambda}}$, constructed as the element-wise natural logarithm of $\matr{\Lambda}$.
Then we have 
\begin{equation*}
  \log \parent{\matr{G}} = \matr{U} \log \parent{\matr{\Lambda}} \matr{U}^{\top}.
\end{equation*}



\begin{comment}
  Let us define a matrix $\tilde{\matr{G}}$ as
  \begin{equation*}
    \tilde{\matr{G}} = \matr{U}^{-1} \matr{G} \matr{U}.
  \end{equation*}
  The matrix $\tilde{\matr{G}}$ is a diagonal matrix whose diagonal elements are the eigenvalues of $\matr{G}$.
  We replace each diagonal element of $\tilde{\matr{G}}$ by its natural logarithm in order to obtain $\log \parent{\tilde{\matr{G}}}$.
  Then we have
  \begin{equation*}
    \log \parent{\matr{G}} := \matr{U} \log \parent{\tilde{\matr{G}}} \matr{U}^{\top}.
  \end{equation*}
\end{comment}






