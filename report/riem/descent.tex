As a reminder, this section is again inspired from~\cite{cambier2015}.
We want to generalize the steepest descent algorithm, from its Euclidean version
\begin{equation*}
  x_{k+1} = x_{k} - \alpha_{k} \nabla f \parent{x_{k}},
\end{equation*}
to the Riemannian one. To do so, some notions must be introduced.
To understand these notions, Figure~\ref{fig:retraction}, on page~\pageref{fig:retraction}, might prove useful.
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{retraction}
    \caption{Essential tools of Riemannian optimization: tangent space $T_{x} \M$, tangent vector $\xi$ and retraction $R_{x} \parent{\xi}$. Courtesy of~\cite{absil2009optimization}.}
    \label{fig:retraction}
\end{figure}

\subsection{Tangent space and Inner product}

A way to visualize smooth manifolds is to view them as curved shapes in the ambient space.
The smoothness of the manifold enables us to associate a \emph{tangent space}, denoted by $T_{x} \M$, to each point $x \in \M$.
Locally, the tangent space is a first-order approximation of the manifold, and it is a vector space.
The elements of a tangent space are called \emph{tangent vectors}, and they generalize the notion of directional derivatives.
In order to characterize which direction of motion from $x$ produces the steepest increase in $f$, we further need a notion of length that applies to tangent vectors.

Given a point $x \in \M$ and a tangent space $T_{x} \M$, one can define an inner product between two vectors $\xi_{x} \in T_{x} \M$ and $\eta_{x} \in T_{x} \M$.
The subscript $x$ denotes that the tangent vectors are in the tangent space $T_{x} \M$, however we will sometimes abuse notation and remove this subscript $x$ when the tangent space is obvious from the context.
This inner product is defined as
\begin{equation*}
  \angles{\cdot, \cdot}_{x}: T_{x} \M \times T_{x} \M \rightarrow \R: \parent{\xi_{x},\eta_{x}} \rightarrow \angles{\xi_{x}, \eta_{x}}_{x},
\end{equation*}
with the usual properties of the inner product, i.e., it must be a bilinear, symmetric and positive-definite form.
Note that, in general, this inner product depends on the point $x$, since the tangent space depends on $x$ as well.
The norm of a vector $\xi_{x} \in T_{x} \M$ is naturally defined as 
\begin{equation*}
  \norm{\xi_{x}}_{x} = \sqrt{\angles{\xi_{x}, \xi_{x}}_{x}}.
\end{equation*}

\subsection{Riemannian metric and Riemannian distance}

A manifold whose tangent spaces are endowed with a smoothly varying inner product is called a \emph{Riemannian manifold}.
The smoothly varying inner product $g$ is called the \emph{Riemannian metric}.
The following notations
\begin{equation*}
  g \parent{\xi_{x}, \eta_{x}} = g_{x} \parent{\xi_{x}, \eta_{x}} = \angles{\xi_{x}, \eta_{x}} = \angles{\xi_{x}, \eta_{x}}_{x}
\end{equation*}
will be equivalently used to denote the inner product of two elements $\xi_{x}$ and $\eta_{x}$ in $T_{x}\M$.
%Strictly speaking, a Riemannian manifold is thus a triple $\parent{\M, \A^{+}, g}$, where $\M$ is a manifold, $\A^{+}$ is the maximal atlas, and $g$ is a Riemannian metric on $\M$.
%Nevertheless, when the Riemannian metric $g$ and the maximal atlas $\A^{+}$ are unimportant or clear from the context, we simply talk about ``the Riemannian manifold $\M$''.

The \emph{Riemannian distance} $d$ between two points $x$ and $y$ on a Riemannian manifold $\parent{\M, g}$ is introduced.
It can be shown that the Riemannian distance is a metric, because it is a symmetric and positive-definite form, that respects the triangular inequality.
We refer the reader to~\cite[Section~3.6]{absil2009optimization} for a rigorous definition. 


%%%%%%%%

\begin{comment}
Now let us define a curve on a manifold. The mapping $\gamma : \squared{0, 1} \rightarrow \M$ can be seen as a parametrized curve from $\R$ to the Riemannian manifold $\M$.
We need to introduce $\dot{\gamma} \parent{t}$ which is the temporal derivative of $\gamma \parent{t}$.
The length of such a curve $\gamma$ is defined as
\begin{equation*}
  L \parent{\gamma} = \int_{a}^{b} \sqrt{g \parent{\dot{\gamma} \parent{t}, \dot{\gamma} \parent{t}}} \mathrm{d} t.
\end{equation*}

We can also introduce the \emph{Riemannian distance} $d$ between two points $x$ and $y$ on a Riemannian manifold $\parent{\M, g}$.
For this we need to define $\Gamma$, the set of all curves in $\M$ joining points $x$ and $y$.
Then, the Riemannian distance is defined as
\begin{equation*}
  d : \M \times \M \mapsto \R : d \parent{x, y} = \inf_{\Gamma} L \parent{\gamma}.
\end{equation*}

It can be shown that the Riemannian distance is a metric, because it is a symmetric and positive-definite form, that respects the triangular inequality.
\end{comment}


\subsection{Gradient}

The most crucial notion in smooth optimization is that of the gradient.
With the definition of the norm in the above lines, the normalized direction of steepest ascent is defined as
\begin{equation*}
  \argmaxi{\xi \in T_{x}\M : \norm{\xi}_{x} = 1} \mathrm{D} f \parent{x} \squared{\xi},
\end{equation*}
where $\mathrm{D} f \parent{x} \squared{\xi}$ is the Fréchet derivative of $f$ at $x$ in the direction $\xi$.
Here, one should write $\xi_{x}$ instead of $\xi$ but the $x$ was dropped to ease notations. 
A reminder of the Fréchet derivative can be found in~\cite{frechet2008} or in Appendix~\ref{appSec:derivatives}.

With this, the \emph{gradient} of $f$ at $x$, denoted by $\grad f \parent{x}$, is defined as the unique element of $T_{x} \M$ that satisfies
\begin{equation*}
  \angles{\grad f \parent{x}, \xi}_{x} = \mathrm{D} f \parent{x} \squared{\xi} \ \forall \ \xi \in T_{x} \M.
\end{equation*}

It is no surprise, yet it is a remarkable result that the direction of $\grad f \parent{x}$ is the steepest ascent direction of $f$ at $x$.
In other words, we have
\begin{equation*}
  \dfrac{\grad f \parent{x}}{\norm{\grad f \parent{x}}} = \argmaxi{\xi \in T_{x}\M : \norm{\xi}_{x} = 1} \mathrm{D} f \parent{x} \squared{\xi}.
\end{equation*}




This being said, when working with embedded manifolds, we can find an easy way to compute the gradient.
The function $f$ is defined from $\M$ to $\R$.
Let us assume that $\M$ is an $n$-dimensional manifold.
Then we define $\bar{f}$ as an extension of $f$ from $\M$ to $\R^{n}$, i.e., $\bar{f}$ is defined from $\R^{n}$ to $\R$.
The Euclidean gradient of $\bar{f}$ at $x$ is computed, and we use $\nabla_{x} \bar{f} \parent{x} \in \R^{n}$ to denote this direction.
Finally, this direction is projected onto the tangent space at $x$.
Therefore, we define $\mathcal{P}_{x}$ as the orthogonal projector from $\R^{n}$ onto $T_{x} \M$.
We obtain
\begin{equation*}
    \grad f(x) = \mathcal{P}_{x} \parent{\nabla_{x} \bar{f}(x)}.
\end{equation*}






\subsection{Exponential mapping and Retraction} \label{subsec:retraction}


In order to solve problem~\eqref{eq:optimization}, we need to move while staying on the manifold.
Therefore, let us assume that we have a direction $\xi \in T_{x} \M$.
The algorithm starts at a point $x \in \M$, and moves in the direction of the tangent vector $\xi$.

We define a geodesic as a parameterized curve $\gamma$ that is locally distance-minimizing with respect to the Riemannian metric $d$.
The exponential map $\Exp_{x} \parent{\xi} : T_{x} \M \rightarrow \M$ maps $\xi \in T_{x} \M$ to $y \in \M$ such that there is a geodesic with $\gamma \parent{0} = x$, $\gamma \parent{1} = y$ and $\ddt{\gamma} \parent{0} = \xi$.
%If there is a unique geodesic connecting $x, y \in \M$, then the exponential map will have a well-defined inverse $\Exp_{x}^{-1} \parent{y} : \M \rightarrow T_{x} \M$, such that the length of the connecting geodesic is $d \parent{x, y} = \norm{\Exp_{x}^{-1} \parent{y}}$.


In many cases, the exponential map is not easy to compute.
Instead, it is much easier to use the retraction as a first-order approximation of the exponential.
If $R_{x} : T_{x} \M \rightarrow \M$ is a retraction mapping, then we use $R_{x}^{-1} : \M \rightarrow T_{x} \M$ to denote its inverse, when it is well-defined.
Informally, the retraction from $x$ in a direction $\xi \in T_{x} \M$, denoted as $R_{x} \parent{\xi}$, is a point on the manifold $\M$ obtained by coming from $x$ in the direction $\xi$.
Formally, we refer to~\cite[Definition~4.1.1]{absil2009optimization}.


\begin{comment}
Formally, we have the following definition.
\begin{definition}[Retraction]
A \emph{retraction} on $\M$ is a smooth function 
\begin{equation*}
  R_{x}: T_{x} \M \rightarrow \M
\end{equation*}
such that
  \begin{itemize}
    \item $R_{x} \parent{0_{x}} = x$, where $0_{x}$ is the zero vector of $T_{x} \M$;
    \item $\mathrm{D} R_{x} \parent{0_{x}} = I_{T_{x} \M}$, where $\mathrm{D}$ denotes the directional derivative and $I_{T_{x} \M}$ is the identity mapping from $T_{x} \M$ to $T_{x} \M$.
  \end{itemize}
\end{definition}
\end{comment}


For example, let us take the sphere, $\M = \mathbb{S}^{n-1}$, endowed with the natural metric inherited through immersion in $\R^{n}$.
If we consider a point $x \in \M$ and a tangent vector $\xi$ at this point, it is clear that the new point $x + \xi$ does not belong to the sphere.
This is because the sphere is not a vector space.
Hence, we define a retraction which simply consists of an addition in the ambient space $\R^{n}$, followed by a projection onto the sphere.
Numerically, this is a very simple operation compared to the exponential mapping which requires the explicit computation of the geodesic distance.




In that case, one might be tempted to see optimization on manifolds as a special case of a projected gradient descent method.
However, this is not the case.
On the one hand, with the projected gradient method, we compute a classical gradient in the ambient space and it is projected on the search space. 
On the other hand, with methods from optimization on an embedded manifold, we start by projecting the classical gradient to the tangent space.
Then, when the retraction is a projection, we move in this direction, and we project on the embedded manifold.
If the manifold is a sphere, the second method goes a bit further but the results can be vastly different on a ``bumpy'' manifold.
Actually, the constraint defining the manifold is \emph{encoded in the gradient}. 
In that viewpoint, the direction of the Riemannian gradient is more relevant than the direction of the classical Euclidean gradient because the latter does not take the constraint into account. 


\subsection{Steepest descent}

We are now able to define the equivalent of the steepest descent algorithm on a manifold $\M$.
As will be explained in Chapter~\ref{ch:SGD}, we start by choosing an initial iterate, say $x_{0}$, and we set $k=0$.
Until $x_{k}$ is close enough to the optimum $x^{\star}$, the iterate is updated with
\begin{equation*}
  x_{k+1} = R_{x_{k}} \parent{- \alpha_{k} \grad f \parent{x_{k}}},
\end{equation*}
where $\alpha_{k}$ is a positive step-size, and then we set $k \leftarrow k+1$.
Regarding the step-size, the Armijo’s backtracking procedure is often used, as in~\cite[Definition~4.2.2]{absil2009optimization}.
Different ways to pick the step-size are discussed in Section~\ref{sec:SGD:step}.
It is worth noticing that $\alpha_{k} \grad f \parent{x_{k}}$ is well-defined.
Indeed, we have $\grad f \parent{x_{k}} \in T_{x} \M$, $T_{x} \M$ is a vector space and $\alpha_{k} \in \R$.



