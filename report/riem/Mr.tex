



\subsection{Quotient geometry of the full-rank factorization} \label{subsec:riem:fact}


In practice, the matrix $\matr{X} \in \M_{r}$ cannot be stored as a $d_{1} \times d_{2}$ matrix.
Indeed, it requires storing $d_{1} d_{2}$ numbers, which can be equal to $10^{11}$ in the Netflix case.
We know from Chapter~\ref{ch:LRMC} that the manifold dimension is $\parent{d_{1} + d_{2} - r} r$.
Since we often have $r \ll \min \bracket{d_{1}, d_{2}}$, it is clear that $d_{1} d_{2}$ is much larger than the manifold dimension.
Therefore, as it is suggested in~\cite{DBLP:journals/corr/abs-1209-0430, AbsOse2013.04}, a popular way to parameterize fixed-rank matrices is through \emph{matrix factorization}.
One popular matrix factorization for fixed-rank non-symmetric matrices is reviewed, and we study the underlying Riemannian geometry of the resulting search space.
This fixed-rank matrix factorizations is called the \emph{full-rank factorization}.
It arises from the thin singular value decomposition (SVD) of a rank-$r$ matrix $\matr{X}$, i.e.
%The two other factorizations described in~\cite{DBLP:journals/corr/abs-1209-0430} are the \emph{polar factorization} and the \emph{subspace-projection factorization}.
%They will not be mentioned in this work.
%These three factorizations arise from the thin singular value decomposition (SVD) of a rank-$r$ matrix $\matr{X}$, i.e.
\begin{equation}
\label{eq:SVD}
  \matr{X} = \matr{U} \matr{\Sigma} \matr{V}^{\top},
\end{equation}
where $\matr{U}$ is a $d_{1} \times r$ matrix with orthogonal columns, 
$\matr{\Sigma}$ is a $r \times r$ diagonal matrix with positive entries
and $\matr{V}$ is a $d_{2} \times r$ matrix with orthogonal columns.
As a reminder, the Stiefel manifold is defined as $\text{St}\parent{r, d} = \bracket{\matr{U} \in \R^{d \times r} : \matr{U}^{\top} \matr{U} = \matr{I}_{r \times r}}$.
The set $\text{Diag}_{++}\parent{r}$ is defined as the set of $r \times r$ diagonal matrices with positive entries.
Hence, we have
$\matr{U} \in \text{St}\parent{r, d_{1}}$,
$\matr{\Sigma} \in \text{Diag}_{++}\parent{r}$ and
$\matr{V} \in \text{St}\parent{r, d_{2}}$.
The SVD exists for any matrix $\matr{X} \in \R_{r}^{d_{1} \times d_{2}}$, as it is proved in~\cite{golub1996matrix}.
The decomposition~\eqref{eq:SVD} is illustrated in Figure~\ref{fig:SVD}.


\begin{figure}[h!]
    \centering
    \begin{equation*}
      \renewcommand\matscale{1.1}
          \matbox{7}{6}{d_{1}}{d_{2}}{X} \
          = \ 
          \matbox{7}{2}{d_{1}}{r}{U} \ 
          \matbox{2}{2}{r}{r}{\Sigma} \ 
          \matbox{2}{6}{r}{d_{2}}{V^{\top}}
    \end{equation*}
    \caption{The thin singular value decomposition (SVD) of a rank-$r$ matrix of size $d_{1} \times d_{2}$.}
    \label{fig:SVD}
\end{figure}




Notice that $\matr{\Sigma} = \matr{\Sigma}^{\top}$.
For the sequel we need to define $\matr{\Sigma}^{\frac{1}{2}}$, which is a diagonal matrix composed of the square root of the elements of $\matr{\Sigma}$.
It is easy to check that $\matr{\Sigma} = \matr{\Sigma}^{\frac{1}{2}} \matr{\Sigma}^{\frac{1}{2}}$, and $\matr{\Sigma}^{\frac{1}{2}} = \parent{\matr{\Sigma}^{\frac{1}{2}}}^{\top}$.
The full-rank factorization is obtained when the SVD is rearranged as
\begin{equation}
\label{eq:factLR}
  \matr{X}  = \parent{\matr{U} \matr{\Sigma}^{\frac{1}{2}}} \parent{\matr{\Sigma}^{\frac{1}{2}} \matr{V}^{\top}}
            = \matr{L} \matr{R}^{\top},
\end{equation}
where $\matr{L} = \matr{U} \matr{\Sigma}^{\frac{1}{2}} \in \R^{d_{1} \times r}_{*}$ and $\matr{R} = \matr{V} \matr{\Sigma}^{\frac{1}{2}} \in \R^{d_{2} \times r}_{*}$.
We recall that $\R^{d \times r}_{*}$ is the set of \emph{full column rank} $d \times r$ matrices.
The factorization~\eqref{eq:factLR} is illustrated in Figure~\ref{fig:factLR}.


\begin{figure}[h!]
    \centering
    \begin{equation*}
      \renewcommand\matscale{1.1}
          \matbox{7}{6}{d_{1}}{d_{2}}{X} \
          = \ 
          \matbox{7}{2}{d_{1}}{r}{L} \  
          \matbox{2}{6}{r}{d_{2}}{R^{\top}}
    \end{equation*}
    \caption{The full-rank factorization of a rank-$r$ matrix of size $d_{1} \times d_{2}$.}
    \label{fig:factLR}
\end{figure}


The resulting factorization is not unique because the transformation
\begin{equation}
\label{eq:invariance}
  \parent{\matr{L},\matr{R}} \mapsto \parent{\matr{L} \matr{T}^{-1}, \matr{R} \matr{T}^{\top}}
\end{equation}
leaves the original matrix $\matr{X}$ unchanged, where $\matr{T}$ is a regular $r \times r$ matrix, that is, an element of the General Linear group,
$\matr{T} \in \text{GL}\parent{r} = \bracket{\matr{T} \in \R^{r \times r} : \det\parent{\matr{T}} \neq 0}$.
This symmetry stems from the fact that the row and column spaces are invariant to the change of coordinates.


%\todo[inline]{How to avoid confusion with $\matr{T}$, the sampled matrix defined in Chapter 1?}

The classical remedy to remove this indeterminacy in the case of symmetric positive semi-definite matrices is the Cholesky factorization. 
In the case of non-symmetric matrices, the LU decomposition is used. 
However, in the case of optimization on manifolds, these techniques will not be used.
Instead, the invariance mapping~\eqref{eq:invariance} is encoded in an abstract search space by optimizing over a quotient manifold $\M_{r}$ that will be defined next.
In other words, we cannot simply optimize over the product space $\overline{\M}_{r} = \R^{d_{1} \times r}_{*} \times \R^{d_{2} \times r}_{*}$.
Let us use the same notations as in Subsection~\ref{sub:quotient}.
The equivalence relation $\thicksim$ is defined by
\begin{equation}
\label{eq:equivalenceRelation}
  \parent{\matr{L}, \matr{R}} \thicksim \parent{\matr{G}, \matr{H}}
  \Leftrightarrow
  \exists \ \matr{T} \in \text{GL}\parent{r} : \matr{G} = \matr{L} \matr{T}^{-1}, \matr{H} = \matr{R} \matr{T}^{\top}.
\end{equation}

The following proposition verifies that this is indeed an equivalence relation.
\begin{proposition}
\label{prop:equivalenceRelation}
  The relation defined in~\eqref{eq:equivalenceRelation} is an equivalence relation.
\end{proposition}

\begin{proof}
  See Appendix~\ref{app:proofEquivalenceRelation}.
\end{proof}



The product space $\overline{\M}_{r}$ defined above can also be called the total space.
The set of all elements that are equivalent to a point $x = \parent{\matr{L}, \matr{R}} \in \overline{\M}_{r}$ is called the \emph{equivalence class} containing $x$, and is defined as
\begin{equation}
\label{eq:equivClass}
  \squared{x} = \squared{\parent{\matr{L}, \matr{R}}}
              = \bracket{\parent{\matr{L} \matr{T}^{-1}, \matr{R} \matr{T}^{\top}}: \matr{T} \in \text{GL}\parent{r}}.
\end{equation}

The set of all equivalence classes of $\thicksim$ in $\overline{\M}_{r}$ is called the \emph{quotient of $\overline{\M}_{r}$ by $\thicksim$}, and is defined as
\begin{equation*}
  \M_{r} = \overline{\M}_{r}/\thicksim \ = \bracket{\squared{x} : x \in \overline{\M}_{r}}.
\end{equation*}


We will thus use the notation $\overline{\M}_{r} / \text{GL}\parent{r}$ for $\overline{\M}_{r} / \thicksim$.
The set $\text{GL}\parent{r}$ is called the \emph{fiber space}.
The set of equivalence classes is called the \emph{quotient space} and it is denoted as
\begin{equation}
\label{eq:lowRankQuotient}
  \M_{r}
      = \overline{\M}_{r} / \text{GL}\parent{r}
      = \parent{\R^{d_{1} \times r}_{*} \times \R^{d_{2} \times r}_{*}} / \text{GL}\parent{r}.
\end{equation}



At this point, the reader might be confused about $\M_{r}$, the set of matrices of fixed rank.
Is $\M_{r}$ an embedded or a quotient manifold?
In fact, it can be viewed in both ways.
Expressing $\M_{r}$ in the form of~\eqref{eq:lowRankEmbedded} shows the embedded structure.
However, in the sequel we more often use the quotient formulation~\eqref{eq:lowRankQuotient}, since it is more useful here.


%%%%

\begin{comment}
Let us summarize: the factorization~\eqref{eq:fact} is not unique because of the invariance~\eqref{eq:invariance}.
Therefore, we encode the invariance map~\ref{eq:invariance} in an abstract search space by optimizing over the quotient space $\M_{r}$, instead of the product space $\overline{\M}_{r}$.
Each equivalence class is defined as in XXX.
The set of equivalence classes is denoted as in XXX.
The product space $\R^{d_{1} \times r}_{*} \times \R^{d_{2} \times r}_{*}$ is called the \emph{total space}, denoted by $\overline{\M}_{r}$.
The set $\text{GL}\parent{r}$ is called the \emph{fiber space}.
The set of equivalence classes $\mathcal{X}$ is called the \emph{quotient space}.
\end{comment}

%%%%


\begin{comment}
Let us now give some extra information from~\cite{Meyer:2011:LRU:3104482.3104551}.
We say that a factorization $\matr{X} = \matr{L} \matr{R}^{\top}$ is \emph{balanced} if $\matr{L}^{\top} \matr{L} = \matr{R}^{\top} \matr{R}$.
Balanced factorizations are well-known in model reduction and system approximation, as it is stated in~\cite{helmke1994}.
They ensure good numerical conditioning and robustness to noise.
The authors also show that balanced factorizations are characterized as the critical points of a special cost function $b$ defined as
\begin{equation*}
  b \parent{\matr{L}, \matr{R}} = \norm{\matr{L}}^{2}_{F} + \norm{\matr{R}}^{2}_{F}.
\end{equation*}
within an equivalence class~\eqref{eq:equivClass}. 

\todo[inline]{Is this useful?}
\end{comment}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Retraction} \label{subsec:riem:retraction}



%\todo[inline]{Is this the right place to put this?}

Let us take a tangent vector $\xi_{x} \in T_{x} \overline{\M}_{r}$ with $\xi_{x} = \parent{\xi_{\matr{L}}, \xi_{\matr{R}}}$.
Assume that we are at point $x \in \overline{\M}_{r}$, and we want to move in the direction $\xi_{x}$.
If $x = \parent{\matr{L}, \matr{R}}$, then this is equivalent to saying two things:
we are at $\matr{L} \in \R^{d_{1} \times r}_{*}$, and we want to move in the direction $\xi_{\matr{L}}$;
we are at $\matr{R} \in \R^{d_{2} \times r}_{*}$, and we want to move in the direction $\xi_{\matr{R}}$.
A simple and efficient retraction is provided by the following formulas
\begin{equation}
\label{eq:simpleRetractionSum}
  \begin{cases}
    R_{\matr{L}} \parent{\xi_{\matr{L}}} = \matr{L} + \xi_{\matr{L}}, \\
    R_{\matr{R}} \parent{\xi_{\matr{R}}} = \matr{R} + \xi_{\matr{R}}.
  \end{cases}
\end{equation}
This might seem weird, but since the considered embedded manifold has the form~\eqref{eq:tangentAlmostEuclidean}, it makes sense that the simplest retraction is a sum.
The inverse retraction is defined as
\begin{equation*}
  \begin{cases}
    R^{-1}_{\matr{L}} \parent{\xi_{\matr{L}}} = \xi_{\matr{L}} - \matr{L}, \\
    R^{-1}_{\matr{R}} \parent{\xi_{\matr{R}}} = \xi_{\matr{R}} - \matr{R}.
  \end{cases}
\end{equation*}

\begin{comment}
Now, let us look at~\cite[Eq.~(2)]{Fla-1802.09128}.
In this very recent paper, the authors construct a sequence of iterates $\bracket{x_{k}}_{k \geq 0}$ in $\M$, produced from the first-order optimization of $f$,
\begin{equation*}
  x_{k+1} = R_{x_{k}} \parent{- \alpha_{k+1} \nabla f_{k+1} \parent{x_{k}}},
\end{equation*}
that are converging to a strict local minimum of $f$, denoted by $x^{\star}$.
Then, they consider and analyze the convergence of a streaming average of iterates $\bracket{\tilde{x}_{k}}_{k \geq 0}$ defined as
\begin{equation*}
  \tilde{x}_{k+1} = R_{\tilde{x}_{k}} \parent{\dfrac{1}{k+1} R_{\tilde{x}_{k}}^{-1} \parent{x_{k+1}}}.
\end{equation*}

For us, this becomes
\begin{equation*}
  \begin{cases}
    \tilde{\matr{L}}_{k + 1} = R_{\tilde{\matr{L}}_{k}} \parent{\dfrac{1}{k + 1} R_{\tilde{\matr{L}}_{k}}^{-1} \parent{\matr{L}_{k + 1}}}, \\
    \tilde{\matr{R}}_{k + 1} = R_{\tilde{\matr{R}}_{k}} \parent{\dfrac{1}{k + 1} R_{\tilde{\matr{R}}_{k}}^{-1} \parent{\matr{R}_{k + 1}}}.
  \end{cases}
\end{equation*}

Or again
\begin{equation*}
  \begin{cases}
    \tilde{\matr{L}}_{k + 1} = \tilde{\matr{L}}_{k} + \parent{\dfrac{1}{k + 1} \squared{\matr{L}_{k + 1} - \tilde{\matr{L}}_{k}}}, \\
    \tilde{\matr{R}}_{k + 1} = \tilde{\matr{R}}_{k} + \parent{\dfrac{1}{k + 1} \squared{\matr{R}_{k + 1} - \tilde{\matr{R}}_{k}}}.
  \end{cases}
\end{equation*}
\end{comment}

%Therefore, we should look at~\cite[Section~3.4]{AbsOse2013.04} first.

%%%%%%%%%%%%%%%%


\begin{comment}
\todo[inline]{Is this useful?}

We end this section with a simple remark.
We know that the rank $r$ represents the number of latent features in the model.
Hence, if we take $r \geq \max \bracket{d_{1}, d_{2}}$, then the matrices $\matr{L}$ and $\matr{R}$ will be quite special.
Indeed, we know that $\matr{X} = \matr{L} \matr{R}^{\top}$.
Hence we can write
\begin{equation*}
  \matr{X} = 
      \begin{bmatrix}
          \matr{X} & \matr{0}_{d_{1} \times \parent{r - d_{1}}} 
      \end{bmatrix}
      \begin{bmatrix}
          \matr{1}_{d_{2} \times d_{2}} \\
          \matr{0}_{\parent{r - d_{2}} \times d_{1}} 
      \end{bmatrix},
\end{equation*}
or
\begin{equation*}
  \matr{X} = 
      \begin{bmatrix}
          \matr{1}_{d_{1} \times d_{1}} & \matr{0}_{d_{1} \times \parent{r - d_{1}}} 
      \end{bmatrix}
      \begin{bmatrix}
          \matr{X} \\
          \matr{0}_{\parent{r - d_{2}} \times d_{1}} 
      \end{bmatrix}
\end{equation*}
and the matrices $\matr{L}$ and $\matr{R}$ are then easily identified.
\end{comment}

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{comment}
\subsection{Polar factorization (beyond SVD)}

\begin{equation*}
  \matr{X} = \matr{U} \matr{B} \matr{V}^{\top}
\end{equation*}

where $\matr{U} \in \text{St}\parent{r, d_{1}}$, $\matr{V} \in \text{St}\parent{r, d_{2}}$ and
$\matr{B}$ is now a $r \times r$ symmetric positive definite matrix, that is, an element of
\begin{equation*}
  S_{++} \parent{r} = \bracket{\matr{B} \in \R^{r \times r} : \matr{B}^{\top} = \matr{B} \succ 0}.
\end{equation*}


\subsection{Subspace-projection factorization (beyond QR decomposition)}

\begin{equation*}
  \matr{X} = \matr{U} \parent{\matr{\Sigma} \matr{V}}^{\top} = \matr{U} \matr{Y}^{\top}
\end{equation*}

where $\matr{U} \in \text{St}\parent{r, d_{1}}$, and $\matr{Y} \in \R^{d_{2} \times r}_{*}$.
\end{comment}
