There exist two very different manifold structures of interest, namely \emph{embedded} and \emph{quotient manifolds}.
Both are manifolds in the sense of Section~\ref{sec:riem:manifold}, but they are conceptually quite different.

\subsection{Embedded manifolds}
On the one hand, \emph{embedded manifolds} are manifolds that can easily be described by constraints in the ambient $\R^{n}$ space.
They are easy to handle because it is often straightforward to visualize them as curved shapes in the ambient space.
Let us give two examples, respectively Example 5.25 and Example 5.30 from~\cite{lee2003introduction}.
First, the sphere
\begin{equation*}
  \mathbb{S}_{n-1} =  \bracket{\matr{x} \in \R^{n}: \norm{\matr{x}}_2 = 1},
\end{equation*}
is an embedded manifold of $\R^{n}$.
Indeed, it is simply defined by constraints on $\R^{n}$, and it coincides with the previous definition of a manifold.
The set of low-rank matrices 
\begin{equation}
\label{eq:lowRankEmbedded}
  \R^{d_{1} \times d_{2}}_{r} = \M_{r} = \bracket{\matr{X} \in \R^{d_{1} \times d_{2}}: \rank \parent{\matr{X}} = r},
\end{equation}
can also be seen as an embedded manifold of $\R^{d_{1} \times d_{2}}$.

%\todo[inline]{Explain why it coincides with the previous definition of a manifold.}


\subsection{Quotient manifolds} \label{sub:quotient}
On the other hand, \emph{quotient manifolds} are manifolds described by means of equivalence classes.
A point on the manifold will be a class, and it will be represented by one element of the class.
The conventions of~\cite[Section~3.6.2]{absil2009optimization} will be followed for the notations.
Let $\overline{\M}$ be a manifold equipped with an equivalence relation $\thicksim$, i.e., a relation that is reflexive, symmetric and transitive.
The set of all elements that are equivalent to a point $x \in \overline{\M}$ is called the \emph{equivalence class} containing $x$, and is defined as
\begin{equation*}
  \squared{x} = \bracket{y \in \overline{\M} : y\thicksim x}.
\end{equation*}
The set of all equivalence classes of $\thicksim$ in $\overline{\M}$ is called the \emph{quotient of $\overline{\M}$ by $\thicksim$}, and is defined as
\begin{equation*}
  \M = \overline{\M}/\thicksim \ = \bracket{\squared{x} : x \in \overline{\M}}.
\end{equation*}

Notice that the points of $\M$ are subsets of $\overline{\M}$.
The mapping $\pi : \overline{\M} \mapsto \M$ defined by $x \mapsto \squared{x}$ is called the \emph{natural projection} or \emph{canonical projection}.
Clearly, $\pi \parent{x} = \pi \parent{y}$ if and only if $x\thicksim y$, so we have $\squared{x} = \pi^{-1} \parent{\pi \parent{x}}$.

%\begin{comment}
The set $\overline{\M}$ is called the \emph{total space}, or the \emph{product space}, of the quotient $\M$.
Now we will show that $\M$ is a \emph{quotient manifold}, or a \emph{quotient space}, by defining it.

Let $\parent{\overline{\M}, \A^{+}}$ be a manifold with an equivalence relation $\thicksim$ and let $\mathcal{B}^{+}$ be a manifold structure on the set $\M$.
The manifold $\parent{\M, \mathcal{B}^{+}}$ is called a \emph{quotient manifold} of $\parent{\overline{\M}, \A^{+}}$ if the natural projection $\pi$ is a submersion.
As a reminder, the mapping $\pi$ is a submersion if its differential (that is, the best linear approximation of $\pi$) is everywhere surjective. 
We know from~\cite[Proposition~3.4.1]{absil2009optimization} that if $\overline{\M}$ is a manifold and $\M$ is a quotient of $\overline{\M}$, then $\M$ admits at most one manifold structure that makes it a quotient manifold of $\overline{\M}$.
Given a quotient $\M$ of a manifold $\overline{\M}$, the set $\M$ is called a \emph{quotient manifold} if it admits a (unique) quotient manifold structure.
In this case, the equivalence relation $\thicksim$ is said to be \emph{regular}, and the set $\M/\thicksim$ endowed with this manifold structure is referred to as ``the manifold $\M$''.

%\end{comment}



In Section~\ref{sec:riem:Mr}, these notions are applied to our manifold of interest. 
