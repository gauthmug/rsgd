
\emph{Informally}, one can think of an $n$-dimensional manifold $\M$ as a set that can be identified to $\R^{n}$ \emph{locally} through some one-to-one correspondence. 
\emph{Formally}, we start by defining $\M$ as a set.
Then, the concepts of \emph{charts} and \emph{atlases} need to be defined.

\begin{definition}[Chart]
An $n$-dimensional \emph{chart} $\varphi$ of $\M$ from $\U \subseteq \M$ to an open subset $Y \subseteq \R^{n}$ is a bijection from $\U$ to $Y$.
It is denoted by $\parent{\U, \varphi}$.
\end{definition}

Intuitively, a chart locally assigns some coordinates in $Y$ to a point of $\U$.
However, in order for the algorithms to be well-defined, we need to have charts that give compatible information.
Therefore, each point of the set $\M$ must be at least in one chart domain $\U$. 
Furthermore, if a point $x \in \M$ belongs to the domains of two charts, say $\parent{\U_{1}, \varphi_{1}}$ and $\parent{\U_{2}, \varphi_{2}}$, then the two charts must ``match''.
The concept of atlas takes these requirements into account. 

%\todo[inline]{Double italics?}


\begin{definition}[Atlas]
An \emph{atlas} $\A$ of $\M$ into $\R^{n}$ is a collection of charts $\parent{\U_{\alpha}, \varphi_{\alpha}}$ of $\M$ such that the following two conditions are satisfied.
  \begin{enumerate}
      \item The union corresponds to the manifold: $\bigcup_{\alpha} \U_{\alpha} = \M$.
      \item For any pair $\alpha$, $\beta$ with $\U_{\alpha} \cap \U_{\beta} \neq \emptyset$,
            the sets $\varphi_{\alpha} \parent{\U_{\alpha} \cap \U_{\beta}}$
            and $\varphi_{\beta} \parent{\U_{\alpha} \cap \U_{\beta}}$
            are open sets in $\R^{n}$,
            and the change of coordinates
            \begin{equation*}
              \varphi_{\alpha \beta} = \varphi_{\beta} \circ \varphi_{\alpha}^{-1} : \R^{n} \mapsto \R^{n}
            \end{equation*}
            is smooth on its domain $\varphi_{\alpha} \parent{\U_{\alpha} \cap \U_{\beta}}$.
            The change of coordinates $\varphi_{\alpha \beta}$ is often called the \emph{transition map}.
            We say that the elements of an atlas \emph{overlap smoothly}.
            A smooth function is formally defined in~\cite{absil2009optimization}.
  \end{enumerate}
\end{definition}

The careful reader will already have noticed that many atlases can be defined for a given set $\M$.
Indeed, two atlases $\A_{1}$ and $\A_{2}$ are said to be \emph{equivalent} if $\A_{1} \cup \A_{2}$ is an atlas.
To get rid of all ambiguity, $\A^{+}$ is defined as the \emph{maximal atlas} of $\M$ such that it contains all possible charts of $\M$.
The atlas $\A^{+}$ can also be called the \emph{complete atlas}.

We now have the required tools to define a manifold.
\begin{definition}[Manifold]\label{def:man}
An $n$-dimensional manifold is a couple $\parent{\M, \A^{+}}$ where $\M$ is a set and $\A^{+}$ is a maximal atlas from $\M$ to $\R^{n}$.
\end{definition}

We say that the manifold is smooth since all the transition maps are smooth, that is, derivatives of all orders exist.
%Equivalently, we call such a manifold a $C^{\infty}$-manifold.
In the present document, unless stated otherwise, all manifolds are considered to be smooth.




The definitions of this section allow us to formalize the idea that a manifold $\parent{\M, \A^{+}}$ is locally equivalent to $\R^{n}$.
We can also simply talk about ``the manifold $\M$'' if the maximal atlas is obvious from the context.

