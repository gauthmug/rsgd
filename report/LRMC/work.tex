
Formally, the LRMC problem can be regarded as an optimization problem, as follows 
\begin{equation}
\label{LRMC:origin}
\begin{aligned}
  & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
  & & \rank \parent{\matr{X}} \\
  & \text{subject to}
  & & \P\parent{\matr{X}} = \P\parent{\matr{M}}.
\end{aligned}
\end{equation}

Looking at this formulation, the difficulties are not apparent. 
However, rank minimization is (almost always) an intractable problem: it is in fact provably NP-hard and the only known algorithm to solve it is computationally too expensive, as it is shown in~\cite{chistov1984complexity}.

The solution suggested by~\cite{candes2009exact} was to rewrite the problem as
\begin{equation}
\label{LRMC:CR}
  \begin{aligned}
    & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
    & & \norm{\matr{X}}_{*} \\
    & \text{subject to}
    & & \P\parent{\matr{X}} = \P\parent{\matr{M}},
  \end{aligned}
\end{equation}
where $\norm{\matr{X}}_{*}$ is the nuclear norm of $\matr{X}$ defined as the sum of the singular values of $\matr{X}$.
The nuclear norm is in fact the Schatten norm with $p = 1$.
A reminder about matrix norms is given in Appendix~\ref{appSec:norms}.
A family of low-rank inducing norms and regularizers, which includes the nuclear norm as a special case, was recently introduced in~\cite{DBLP:journals/corr/abs-1612-03186}.
The authors of~\cite{boyd2003} have shown that the nuclear norm is the tightest convex relaxation of the rank. 
In the context of \emph{exact} LRMC, i.e., if the strict constraint~\eqref{eq:strictEquality} is imposed,
the authors have shown that this formulation recovers the original underlying matrix under some mild assumptions.
Namely, if the number $\abs{\Omega}$ of sampled entries obeys
\begin{equation*}
  \abs{\Omega} \geq C d^{\frac{6}{5}} r \log \parent{d},
\end{equation*}
for some positive numerical constant $C$, then with very high probability, most $d_{1} \times d_{2}$ matrices of rank $r$ can be perfectly recovered by solving a simple convex optimization program.
Here, it is assumed that $d = d_{1} = d_{2}$, but this discussion would apply to arbitrary rectangular matrices as well.
The advantage of focusing on square matrices is a simplified exposition and reduction in the number of parameters we need to keep track of.

Exact matrix completion, i.e., with the constraint~\eqref{eq:strictEquality}, is used in many problems, e.g., in fluid dynamics as in~\cite{zarjovgeoJFM17}.
Strict equality is imposed, similarly to the case of covariance completion, where precise measurements have to be matched.
However, one can see that this constraint is very strict.
Indeed, in real applications, the observations are noisy, thus imposing this equality can cause problems.
These problems come from the fact that the matrix $\matr{X}$ tries to perfectly match $\matr{M}$, including the noise.
This phenomenon is called \emph{overfitting}; in short it refers to the situation where the model fits the known entries too well, while deviating too much from the mean on the unknown entries.
Overfitting and underfitting are important concepts in machine learning, and they will be introduced in Section~\ref{sec:SGDB:rank}.
This being said, let us now look at the problem where strict equality must not be imposed.
In order to avoid overfitting, the authors of~\cite{candes2010matrix} have defined the problem
\begin{equation}
\label{LRMC:noisy}
  \begin{aligned}
    & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
    & & \norm{\matr{X}}_{*} \\
    & \text{subject to}
    & & \norm{\P\parent{\matr{X}} - \P\parent{\matr{M}}}_{F} \leq \delta,
  \end{aligned}
\end{equation}
for some small value of $\delta \in \R$.
Of course, since the projection $\P$ is linear, it is equivalent to write $\P\parent{\matr{X}} - \P\parent{\matr{M}}$ or $\P\parent{\matr{X} - \matr{M}}$.
The second formulation is neater, but one should remember that the matrix $\matr{X} - \matr{M}$ cannot be computed, since $\matr{M}$ is only known on the set $\Omega$.

The methods described above are focused on minimizing the rank, or some relaxation of the rank, and the constraint ensures that the new matrix is close to the original data. 
Now the tables are turned: the rank is fixed \emph{a priori}, and the error between the new matrix and the original data is minimized.
Hence, the difference between the matrices $\matr{X}$ and $\matr{M}$ on the set $\Omega$ is minimized.
If the rank $r$ of the target matrix is fixed \textit{a priori}, then the problem can be stated as
\begin{equation}
\label{LRMC:prob}
  \begin{aligned}
    & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
    & & \dfrac{1}{2} \norm{\P \parent{\matr{X} - \matr{M}}}_{F}^{2} \\
    & \text{subject to}
    & & \text{rank} \parent{\matr{X}} = r.
  \end{aligned}
\end{equation}



A regularization term is often added in order to avoid overfitting.
Hence, a term $\lambda \norm{\matr{X}}^{2}_{F}$ would be added to the objective function, for some non-negative regularization parameter $\lambda \geq 0$.
The optimization algorithm then tries to ensure that $\norm{\matr{X}}^{2}_{F}$ is as small as possible.
One can think of the term $\norm{\matr{X}}^{2}_{F}$ as a way to quantify the model complexity. 
Usually, a factor $\frac{1}{2}$ is added in front such that the gradient is easier to write. 
The careful reader will readily have noticed that overfitting is already avoided by the constraint $\text{rank} \parent{\matr{X}} = r$ with a small value for the rank $r$.
This is a good point, but fixing the rank of $\matr{X}$ to a small value does not avoid its Frobenius norm to explode.
Hence, some formulations include the term $\frac{\lambda}{2} \norm{\matr{X}}^{2}_{F}$ in the objective function, but we will always set $\lambda = 0$.
Indeed, since stochastic gradient descent (SGD) methods are used in this thesis, we are not fully optimizing the cost function, and it is unlikely that $\norm{\matr{X}}^{2}_{F}$ explodes.
In real applications however, a non-zero value for $\lambda$ can be defined, since this usually allows to improve the performance on unseen data.


%\todo[inline]{Why can we be confident in setting $\lambda = 0$ all the time?}

Regarding the constraint $\text{rank} \parent{\matr{X}} = r$, one might ask why we did not write $\text{rank} \parent{\matr{X}} \leq r$ instead.
In turns out that the search space could be defined as $\M_{\leq r}$, with
\begin{equation*}
  \M_{\leq r} = \bracket{\matr{X} \in \R^{d_{1} \times d_{2}} : \rank \parent{\matr{X}} \leq r},
\end{equation*}
but in practice the optimization variable $\matr{X}$ always has the highest rank possible. 
The search space can thus be written as $\M_{r}$, with
\begin{equation*}
  \M_{r} = \bracket{\matr{X} \in \R^{d_{1} \times d_{2}} : \rank \parent{\matr{X}} = r}.
\end{equation*}
This space, which is a set, can be endowed with a manifold structure, a concept that will be introduced in Chapter~\ref{ch:riem}, see Definition~\ref{def:man}.
Hence, the machinery of optimization on manifolds described in~\cite{absil2009optimization} can be used.
The general philosophy of optimization on manifolds is to recast a constrained optimization problem in the Euclidean space into an unconstrained optimization on a non-linear search space that encodes the constraint. 
The idea of a direct optimization over the set of all fixed-rank matrices, with a generalization of classical non-linear conjugate gradients, leads to the famous low-rank geometric conjugate gradient method, often denoted by LRGeomCG, described in~\cite{vandereycken2013low}.

Algorithms for low-rank matrix completion based on preconditioned Riemannian optimization over a single Grassmannian manifold are presented in~\cite{Boumal2015200, boumal2011rtrmc}.
The authors have defined a method called ``RCGMC'' for Robust Conjugate Gradient Matrix Completion.
Another version called ``RCGMC 2'' was developed by applying second-order Riemannian trust-region methods.
\nomenclature{RCGMC}{Robust Conjugate Gradient Matrix Completion}


Now that recent methods have been defined, let us focus on the model.
A popular way to tackle the rank-constraint in~\eqref{LRMC:prob} is by using a factorization model.
Suppose that one wishes to recover a rectangular $d_{1} \times d_{2}$ matrix $\matr{M}$ of rank $r$.
Such a matrix $\matr{M}$ can be represented by $d_{1} d_{2}$ numbers, but it only has $\parent{d_{1} + d_{2} - r} r$ degrees of freedom.
Indeed, it is stated in~\cite[Proposition~1.14]{helmke1994} that the dimension of the manifold of $d_{1} \times d_{2}$ matrices of rank $r$ is $\parent{d_{1} + d_{2} - r} r$, as long as $\max \bracket{d_{1}, d_{2}} > 1$.
Among others, this can be revealed by counting the number of parameters in the singular value decomposition (SVD), i.e., the number of degrees of freedom associated with the description of the singular values and of the left and right singular vectors.


Let us talk about the procedure defined in~\cite{doi:10.1137/080738970} to define synthetic data.
A fraction of the entries are randomly removed with uniform probability.
Since the dimension of $\M_{r}$ is $\parent{d_{1} + d_{2} - r} r$, the number of known entries is a multiple of this dimension.
This multiple is called the \emph{over-sampling ratio}, often abbreviated by OS ratio.
The OS ratio determines the number of known entries.
For example, if $\text{OS} = 5$, it means that $\abs{\Omega} = 5 \parent{d_{1} + d_{2} - r} r$ of randomly and uniformly selected entries are known \textit{a priori} out of a total of $d_{1} d_{2}$ entries.

\begin{comment}
One can also define the proportion $p$ of known entries as the number of known entries over the total number of entries as
\begin{equation*}
  p = \dfrac{\abs{\Omega}}{d_{1}d_{2}} = \dfrac{\text{OS} \parent{d_{1} + d_{2} - r} r}{d_{1}d_{2}}.
\end{equation*}
\end{comment}

We are reminded in~\cite{Bon2013} that a gradient descent method could be applied to solve problem~\eqref{LRMC:prob}.
However, the matrix is potentially of high dimension, $d_{1} \approx 10^6$ and $d_{2} \approx 10^5$ in the Netflix case.
Hence, computing the full gradient is not feasible.
Therefore, a standard method to reduce the computational burden is to draw random elements of $\Omega$, say, element $\parent{i,j}$, and perform gradient descent ignoring the remaining entries.
The updated matrix
\begin{equation*}
  \matr{X}_{+} = \matr{X} - \alpha \nabla_{\matr{X}} \parent{M_{ij} - X_{ij}},
\end{equation*}
is obtained, where $\alpha$ is the step-size.
Unfortunately, this matrix does not have rank $r$.
Seeking the matrix of rank $r$ which best approximates it can be numerically costly, especially for very large $d_{1}$ and $d_{2}$.
Basically, the idea would be to project $\matr{X}_{+}$ on the manifold of fixed-rank matrices, i.e., $\M_{r}$.
This would involve a SVD.
Instead, a more natural way to enforce the rank constraint is to endow the parameter space with a Riemannian metric, and to perform a gradient step \emph{within the manifold $\M_{r}$}.
This approach of Riemannian optimization will be at the core of this thesis.
%At this point, it might be useful to recall that, as it is stated in~\cite[Section~3.1.2]{AbsOse2013.04}, the $\O{r^{3}}$ flop count of the SVD above concerns an operation that cannot in general be performed with finitely many elementary arithmetic operations.
%Therefore, it involves an iterative process and a stopping criterion, which may have a considerable impact on the computation time in the case where we do not have $r \ll \min \bracket{d_{1}, d_{2}}$.


%\todo[inline]{Complexity careful with SVD, look at.}







%\todo[inline]{Put this somewhere else.}

Finally, two recent PhD theses were very useful to get acquainted with the subject of LRMC.
The thesis~\cite{MishraPhD} is primarily motivated by the LRMC problem that is viewed as a least squares problem on a matrix manifold with symmetries, whereas optimization on manifolds is exhaustively discussed in~\cite{boumal2014optimization}.




