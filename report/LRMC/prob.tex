
The low-rank matrix completion problem, also called the LRMC problem, is the task of filling in the missing entries of a matrix from which only a subset of the entries are observed. 
\nomenclature{LRMC}{Low-Rank Matrix Completion}
Of course, in the sampled entries, we have some noise or even some outliers.
If the original matrix was perfectly noiseless, the problem could be stated as recovering a matrix $\matr{M} \in \R^{d_{1} \times d_{2}}$, observed only on a subset $\Omega$ of its entries, by finding a matrix $\matr{X} \in \R^{d_{1} \times d_{2}}$ such that
\begin{equation}
\label{eq:strictEquality}
  X_{ij} = M_{ij}, \ \forall \ (i,j) \in \Omega,
\end{equation}
where the matrix $\matr{X}$ has the lowest possible rank. 
The reader can find some linear algebra reminders in Appendix~\ref{appSec:rank}, including a basic definition of the rank.
Intuitively, the rank of a matrix can be thought of as the \emph{complexity} of a matrix:
if a matrix has a low rank, it means that many of its columns (or rows) are linearly dependent, hence these columns (or rows) do not give more information about the matrix;
if a matrix has a high rank, it means that its columns (or rows) are very different from one another, hence every column (or row) adds more information about the matrix.


Here we stress the fact that $\matr{M}$ is given only on $\Omega$.
In other words, the sample set $\Omega$ is defined as the set of all $\parent{i,j}$ such that $M_{ij}$ is sampled, i.e., $M_{ij}$ is observed.
Rigorously, the entry $M_{ij}$ is sampled if and only if $\parent{i,j} \in \Omega$.

%\todo[inline]{Use different letter because $\matr{M}$ is already used in Chapter~\ref{ch:riem}?}



The equality constraint~\eqref{eq:strictEquality} is often expressed differently.
For this, the \emph{orthogonal sampling operator} first needs to be defined.
It is a projection $\P: \R^{d_{1} \times d_{2}} \rightarrow \R^{d_{1} \times d_{2}}$ defined as
\begin{equation*}
  \squared{\P(\matr{X})}_{ij}
  = \left\{
        \begin{array}{ll}
                X_{ij},  & \text{ if } (i,j) \in \Omega, \\
                0,       & \text{ otherwise.}
        \end{array}
    \right.
\end{equation*}

Hence, the constraint~\eqref{eq:strictEquality} can be written as
\begin{equation}
\label{eq:projEquality}
  \P\parent{\matr{X}} = \P\parent{\matr{M}}.
\end{equation}


A standard way of approaching the problem is to minimize the rank.
However, in this thesis we will cast the problem as a fixed-rank optimization problem, assuming that the rank $r$ is known \emph{a priori}, i.e.,
\begin{equation*}
%\label{LRMC:prob}
  \begin{aligned}
    & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
    & & \dfrac{1}{2} \norm{\P \parent{\matr{X}} - \P \parent{\matr{M}}}_{F}^{2} \\
    & \text{subject to}
    & & \text{rank} \parent{\matr{X}} = r.
  \end{aligned}
\end{equation*}

\begin{comment}
  
Formally, the LRMC problem can be regarded as an optimization problem, as follows 
\begin{equation}
\label{LRMC:origin}
  \begin{aligned}
    & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
    & & \rank \parent{\matr{X}} \\
    & \text{subject to}
    & & \P\parent{\matr{X}} = \P\parent{\matr{M}}.
  \end{aligned}
\end{equation}

However, this is a NP-hard problem, and one can already see that the strict equality~\eqref{eq:projEquality} can cause problems, as it will be explained next.
Therefore, a value for the rank $r$ is fixed \emph{a priori}, and the formulation becomes
\begin{equation*}
%\label{LRMC:prob}
  \begin{aligned}
    & \underset{\matr{X} \in \R^{d_{1} \times d_{2}}} \min
    & & \dfrac{1}{2} \norm{\P \parent{\matr{X}} - \P \parent{\matr{M}}}_{F}^{2} \\
    & \text{subject to}
    & & \text{rank} \parent{\matr{X}} = r.
  \end{aligned}
\end{equation*}
\end{comment}


\begin{example}
Consider the partially sampled matrix $\matr{M}$ defined as
\begin{equation*}
  \matr{M} =
  \begin{bmatrix}
    1 & 2 & 3  \\
    2 & 4 & \star  \\
    \star & \star & 9
\end{bmatrix},
\end{equation*}
where each unsampled entry is denoted as $\star$.
We have $\Omega = \left\{\parent{1, 1}; \parent{1, 2}; \parent{1, 3}; \parent{2, 1}; \parent{2, 2}; \parent{3, 3}\right\}$.
There are an infinite number of completions of $\matr{M}$.
Let us give two examples, namely the matrices
\begin{equation*}
  \matr{X}_{1} =
  \begin{bmatrix}
    1 & 2 & 3  \\
    2 & 4 & {\color{blue} 6}  \\
    {\color{blue} 3} & {\color{blue} 6} & 9
\end{bmatrix},
 \quad
  \matr{X}_{2} =
  \begin{bmatrix}
    1 & 2 & 3  \\
    2 & 4 & {\color{blue} 6}  \\
    {\color{blue} 3} & {\color{blue} 7} & 9
\end{bmatrix}.
\end{equation*}


We see that $\rank \parent{\matr{X}_{1}} = 1$, while $\rank \parent{\matr{X}_{2}} = 2$.
Hence, if the rank $r$ is fixed to $1$ for this problem, then $\matr{X}_{1}$ is a solution to the LRMC problem, while $\matr{X}_{2}$ is not.
%One can also think about other situations where the LRMC problem does not have a unique solution.  
Of course, this is only a small example: in real applications the matrices that we are trying to fill are much larger.
\end{example}


%\todo[inline]{Is this example useful ?}

