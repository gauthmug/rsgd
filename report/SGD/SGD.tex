

Why does the gradient descent method work so well?
A gradient (at a point) is the slope of the tangent to the function (at that point).
It points to the direction of the largest \emph{increase} of the function. 
Hence, to minimize the function, it makes sense to follow the \emph{opposite direction} of the gradient.
This means that we choose $p_{k} = - \nabla f\parent{x_{k}}$.

Let us define $N = \abs{\Omega}$, i.e., the number of observed entries of the matrix $\matr{M}$.
%We know from XXX that the complexity of this algorithm is $\O{N}$, which can be prohibitive in most cases.
We know that $N$ can be very large, hence the sublinear complexity rate is problematic.
Therefore, we only compute a \emph{fraction of the gradient}.

%\todo[inline]{Complexity?}

As often in machine learning, the cost function of interest is formulated as a sum over the training examples.
Let us clarify that: one training example is picked, say $n \in \squared{N}$, where $N$ corresponds to the total number of training examples. 
For this data point, the true value of the output is known, but we can also predict this value by using our algorithm.
This gives a predicted value.
A cost function $f_{n} \parent{x}$ is defined to measure the misfit between the predicted value, for any given value of $x$, and the true value.
In other words, $f_{n}$ is the cost contributed by the $n$-th training example. 
This is done for all the training examples.
We obtain
\begin{equation}
\label{eq:generalObjectiveSum}
  f\parent{x} = \dfrac{1}{N} \sum_{n = 1}^{N} f_{n}\parent{x}.
\end{equation}


%\todo[inline]{Is it clear what $f_{n}\parent{x}$ represents?}


Now, the idea of stochastic gradient descent, or SGD, can be defined: at each iteration $k$, one training example is randomly selected, say $n \in \squared{N}$, the direction $g_{k}$ is computed as
\begin{equation*}
  g_{k} = \nabla f_{n}\parent{x_{k}},
\end{equation*}
and the search direction is defined as $p_{k} = - g_{k}$.
The idea is the following: the quantity $\nabla f_{n} \parent{x_{k}}$ is cheap to compute and acts as an estimate for the full gradient $\nabla f\parent{x_{k}}$.
Let us define the random variable $\mathcal{N}$ to denote the chosen training example, i.e., $\mathcal{N} \in \squared{N}$.
\nomenclature{SGD}{Stochastic Gradient Descent}

%\todo[inline]{Maybe $\mathcal{N}$ is a better notation than $\mathcal{N}$.}


As usual, we use $n$ to denote a realization of the random variable $\mathcal{N}$.
Since the training example $n$ is chosen at random with uniform probability, we have
\begin{equation*}
  \E \squared{g_{k}}
  = \sum_{n = 1}^{N} \Prob \squared{\mathcal{N} = n} \nabla f_{n} \parent{x_{k}}
  = \dfrac{1}{N} \sum_{n = 1}^{N} \nabla f_{n}\parent{x_{k}}
  = \nabla f\parent{x_{k}},
\end{equation*}
therefore, $\nabla f_{\mathcal{N}} \parent{x_{k}}$ is said to be an \emph{unbiased} estimator of $\nabla f\parent{x_{k}}$.
However, it is clear that $\nabla f_{\mathcal{N}} \parent{x_{k}}$ is not always a descent direction!
Indeed, the direction $\nabla f_{\mathcal{N}} \parent{x_{k}}$ is the gradient with respect to only \emph{one term in the full objective function}, therefore it can point in a direction that is far different from $\nabla f \parent{x_{k}}$.  
Hence, we would like to find a better estimate.
That is why the notion of \emph{batch} is defined.

%\todo[inline]{Better definition for the batch?}

%\todo[inline]{Always ask the question: should it be the random $\mathcal{N}$ or should it be a specific $n$?}


At each iteration $k$, instead of choosing a single training example, we can choose a couple of training examples.
These training examples are called the batch.
Formally, at each iteration $k$, a subset $\B \subseteq \squared{N}$ of the training examples is randomly chosen.
A new direction is obtained, that is written as
\begin{equation*}
  g_{k} = \dfrac{1}{\abs{\B}} \sum_{n \in \B} \nabla f_{n}\parent{x_{k}},
\end{equation*}
and then again the search direction is defined as $p_{k} = - g_{k}$.

We use $b$ to denote the size of the batch, i.e., the batch-size, such that $b = \abs{\B}$.
In the above computation, a subset $\B \subseteq \squared{N}$ of the training examples is randomly chosen.
For each of these selected examples $n \in \B$, the respective gradients are computed at the same current point, that is, $\nabla f_{n} \parent{x_{k}}$.
This computation can be parallelized easily.
This is how current deep-learning applications utilize Graphics Processing Units, or GPUs. 
\nomenclature{GPU}{Graphics Processing Unit}
We run over $b$ threads in parallel.
Let us assume that we have access to $T$ machines.
In short, we go through three steps.
\begin{enumerate}
  \item Shuffle the data so as to have a representative subset of the full data-set on each machine.
  \item Run SGD on each machine locally and compute $f_{t}$, the cost according to the subset assigned to machine $t$, for $t = 1, \dots, T$.
  \item Aggregate the result from all the machines, i.e., compute the final cost $f = \sum_{t = 1}^{T} f_{t}$.
\end{enumerate}


%\todo[inline]{Coherence}

This enables the famous back-propagation algorithm for deep neural networks, which is used in a wide variety of fields including image and speech recognition.
Recent papers like~\cite{10.1007/978-3-642-33347-7_17, DBLP:journals/corr/PaineJYLH13} go further into that.

Note that the extreme case $\B = \squared{N}$ corresponds to the above described (full) gradient descent method.
The other extreme is $b = 1$, which was described earlier.
Most of Chapter~\ref{ch:SGDBetter} deals with $b = 1$.
In general, if $b$ is small, then $g_{k}$ is a bad estimate of $\nabla f\parent{x_{k}}$,
and if $b$ is large, then $g_{k}$ is a good estimate of $\nabla f\parent{x_{k}}$.
Hence, the trade-off already appears.
Therefore, we define a different batch at each iteration, which is denoted by $\B_{k}$.
The notation $b_{k}$ is used to denote the size of the batch at iteration $k$, i.e., $b_{k} = \abs{\B_{k}}$.


At this point, an important remark has to be made regarding this trade-off.
On the one hand, if $b_{k}$ is large, then we only need a few iterations to converge, but each iteration is extremely costly. 
On the other hand, if $b_{k}$ is small, then we need a lot of iterations to converge, but each iteration is cheap.
Therefore, when the evolution of the error is displayed, the number of iterations cannot be taken as an indicator of the speed of convergence.
Instead, the \emph{running time} is used to compare the convergence speed of different algorithms. 
These aspects and the trade-off are discussed in Section~\ref{sec:SGDB:batch}.



















