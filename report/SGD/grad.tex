
As a reminder, for us, the objective function writes
\begin{equation}
\literallabel{eq:OmegaObjective}
  {f\parent{\matr{X}}  = \dfrac{1}{2} \norm{\P \parent{\matr{X}} - \P \parent{\matr{M}}}_{F}^{2}
                      = \dfrac{1}{2} \sum_{\parent{i,j} \in \Omega}^{} \squared{M_{ij} - X_{ij}}^2},
\end{equation}
but since $\matr{X} \in \M_{r}$, we can write $\matr{X} = \matr{L} \matr{R}^{\top}$ as it is explained in Subsection~\ref{subsec:riem:fact}.
The objective function becomes
\begin{equation}
\label{eq:ObjectiveLR}
  f\parent{\matr{L}, \matr{R}} = \dfrac{1}{2} \sum_{\parent{i,j} \in \Omega}^{} \squared{M_{ij} - \parent{\matr{L} \matr{R}^{\top}}_{ij}}^2,
\end{equation}
with $\parent{\matr{L} \matr{R}^{\top}}_{ij} = \angles{\matr{L}_{i:}, \matr{R}_{j:}} = \sum_{k = 1}^{r} L_{ik} R_{jk}$.
We can define the cost functions corresponding to every individual data point, that is
\begin{equation*}
%\label{eq:objIndividual}
    f_{ij}\parent{\matr{L}, \matr{R}} = \frac{1}{2} \squared{M_{ij} - \parent{\matr{L} \matr{R}^{\top}}_{ij}}^2,
\end{equation*}
for all $\parent{i,j} \in \Omega$, such that we can write
\begin{equation*}
%\label{eq:objSum}
  f\parent{\matr{L}, \matr{R}} = \sum_{\parent{i,j} \in \Omega}^{} f_{ij}\parent{\matr{L}, \matr{R}}. 
\end{equation*}


In order to see the similarity with~\eqref{eq:generalObjectiveSum}, the objective function~\eqref{eq:OmegaObjective} can be written as 
\begin{equation}
\label{eq:OmegaObjectiveFull}
  f\parent{\matr{X}} = \dfrac{1}{\abs{\Omega}} \sum_{\parent{i,j} \in \Omega}^{} \squared{M_{ij} - X_{ij}}^2.
\end{equation}
However, it is clear that the objective functions~\eqref{eq:OmegaObjective} and~\eqref{eq:OmegaObjectiveFull} are completely equivalent, from an optimization point of view. 



\begin{comment}
Sometimes, instead of~\eqref{eq:OmegaObjective}, we write 
\begin{equation}
\label{eq:OmegaObjectiveFull}
  f\parent{\matr{X}} = \dfrac{1}{\abs{\Omega}} \sum_{\parent{i,j} \in \Omega}^{} \squared{M_{ij} - X_{ij}}^2,
\end{equation}
the individual functions are
\begin{equation*}
    f_{ij}\parent{\matr{X}} = \squared{M_{ij} - X_{ij}}^2,
\end{equation*}
hence we have
\begin{equation*}
  f\parent{\matr{X}} = \dfrac{1}{\abs{\Omega}} \sum_{\parent{i,j} \in \Omega}^{} f_{ij}\parent{\matr{X}}. 
\end{equation*}
in order to see the similarity with~\eqref{eq:generalObjectiveSum}.
However, it is clear that from an optimization point of view, the objective functions~\eqref{eq:OmegaObjective} and~\eqref{eq:OmegaObjectiveFull} are completely equivalent.
\end{comment}

%\todo[inline]{Noise}



Now let us choose one entry of the matrix $\matr{M}$.
This entry corresponds to a pair $\parent{i,j}$, hence to one term in the sum.
For this fixed element $\parent{i,j}$ of the sum, we derive the gradient entry $\parent{i',k}$ for $\matr{L}$. 
Analogously, for the same fixed element $\parent{i,j}$ of the sum, we derive the gradient entry $\parent{j',k}$ for $\matr{R}$. 
We find
\begin{align*}
    \pp{f_{ij}}{L_{i',k}} \parent{\matr{L}, \matr{R}} &=
      \begin{cases}
        - \squared{M_{ij} - \parent{\matr{L} \matr{R}^{\top}}_{ij}} R_{jk},   & \text{if } i' = i, \\
        0,                                                                    & \text{otherwise},
      \end{cases} \\
    \pp{f_{ij}}{R_{k,j'}} \parent{\matr{L}, \matr{R}} &=
      \begin{cases}
        - \squared{M_{ij} - \parent{\matr{L} \matr{R}^{\top}}_{ij}} L_{ik},   & \text{if } j' = j, \\
        0,                                                                    & \text{otherwise}.
      \end{cases}
\end{align*}


Let $\matr{S}_{\parent{i,j}}$ be the residual matrix of this subproblem, i.e.,
$\matr{S}_{\parent{i,j}} = \parent{\matr{L} \matr{R}^{\top}}_{ij} - M_{ij} = \matr{L}_{i:} \matr{R}_{j:}^{\top} - M_{ij}$.
Of course, in this case, $\matr{S}_{\parent{i,j}}$ is a scalar, not a matrix.
Hence, we can write
\begin{align}
\label{eq:gradL}
  \nabla_{\matr{L}} f_{ij} \parent{\matr{L}, \matr{R}}
  &=
  \parent{
  \begin{array}{c}
    \matr{0}_{1 \times r} \\
    \vdots  \\
    \matr{0}_{1 \times r} \\
    \matr{S}_{\parent{i,j}} \matr{R}_{j:} \\
    \matr{0}_{1 \times r} \\
    \vdots  \\
    \matr{0}_{1 \times r} \\
  \end{array}
  }
  \hspace{-5pt}
  \begin{array}{l}
     \\ \\
     \\
  \end{array}
  \begin{array}{l}
     \\ \leftarrow \text{row } i \\
     \\
  \end{array}
\end{align}
with $\matr{R}_{j:} \in \R^{1 \times r}$ which is a row vector, and $\nabla_{\matr{L}} f_{ij} \parent{\matr{L}, \matr{R}}$ is therefore a matrix of size $d_{1} \times r$, as it should be.
We also find
\begin{equation}
\label{eq:gradR}
  \nabla_{\matr{R}} f_{ij} \parent{\matr{L}, \matr{R}}
  =
  \parent{
  \begin{array}{c}
    \matr{0}_{1 \times r} \\
    \vdots  \\
    \matr{0}_{1 \times r} \\
    \matr{S}_{\parent{i,j}} \matr{L}_{i:} \\
    \matr{0}_{1 \times r} \\
    \vdots  \\
    \matr{0}_{1 \times r} \\
  \end{array}
  }
  \hspace{-5pt}
  \begin{array}{l}
     \\ \\
     \\
  \end{array}
  \begin{array}{l}
     \\ \leftarrow \text{row } j \\
     \\
  \end{array} 
\end{equation}
with $\matr{L}_{i:} \in \R^{1 \times r}$ which is a row vector, and $\nabla_{\matr{R}} f_{ij} \parent{\matr{L}, \matr{R}}$ is therefore a matrix of size $d_{2} \times r$, as it should be.
Then, the general SGD updates consist of picking one entry $\parent{i, j} \in \Omega$, and computing
\begin{equation}
\label{eq:SGDRiemann}
    \begin{cases}
       \matr{L}_{k+1} = R_{\matr{L}_{k}} \parent{ - \alpha_{k} \nabla_{\matr{L}} f_{ij} \parent{\matr{L}_{k}, \matr{R}_{k}}}, \\
       \matr{R}_{k+1} = R_{\matr{R}_{k}} \parent{ - \alpha_{k} \nabla_{\matr{R}} f_{ij} \parent{\matr{L}_{k}, \matr{R}_{k}}}.
    \end{cases}
\end{equation}
where $R_{\matr{L}}$ and $R_{\matr{R}}$ are retractions~\eqref{eq:simpleRetractionSum} defined in Subsection~\ref{subsec:riem:retraction}.
In order to ease the notations, we often write
\begin{equation}
\label{eq:SGDRiemannWithPlus}
    \begin{cases}
       \matr{L}_{+} = R_{\matr{L}} \parent{ - \alpha \nabla_{\matr{L}} f_{ij} \parent{\matr{L}, \matr{R}}}, \\
       \matr{R}_{+} = R_{\matr{R}} \parent{ - \alpha \nabla_{\matr{R}} f_{ij} \parent{\matr{L}, \matr{R}}}.
    \end{cases}
\end{equation}

By using the definition of the retractions, this becomes
\begin{equation}
\label{eq:SGD1naive}
    \begin{cases}
       \matr{L}_{+} = \matr{L} - \alpha \nabla_{\matr{L}} f_{ij} \parent{\matr{L}, \matr{R}}, \\
       \matr{R}_{+} = \matr{R} - \alpha \nabla_{\matr{R}} f_{ij} \parent{\matr{L}, \matr{R}}.
    \end{cases}
\end{equation}

The structure of $\nabla_{\matr{L}} f_{ij} \parent{\matr{L}, \matr{R}}$ enables us to see that the matrix $\matr{L}_{+}$ is equal to the matrix $\matr{L}$, except for row $i$.
Hence, the entire matrix $\matr{L}$ must not be updated, but only $\matr{L}_{i:}$, which represents row $i$ of the matrix $\matr{L}$ at the previous iteration.
The same can be said for $\matr{R}$.
%The matrix $\matr{R}_{+}$ will be equal to the matrix $\matr{R}$, except for row $j$.
%Hence we do not have to update $\matr{R}$ but only $\matr{R}_{j:}$, which represents row $j$ of the matrix $\matr{R}$ at the previous iteration.
Therefore, the system~\eqref{eq:SGD1naive} is rewritten as
\begin{equation}
\label{eq:SGD1smart}
    \begin{cases}
       \matr{L}_{i:, +} = \matr{L}_{i:} - \alpha \matr{S}_{\parent{i,j}} \matr{R}_{j:}, \\
       \matr{R}_{j:, +} = \matr{R}_{j:} - \alpha \matr{S}_{\parent{i,j}} \matr{L}_{i:}.
    \end{cases}
\end{equation}




With this, the SGD updates could be completely defined if the batch-size was equal to one, i.e., $b=1$.
However, we will now consider a general stochastic gradient setup, as it is done in~\cite{DBLP:journals/corr/MishraS16a}, where we pick $b$ known entries at a time.
Due to the cost function structure, as we have seen, we end up updating only a maximum of $b$ rows of $\matr{L}$ and $\matr{R}$ at a time.
Let $b_{\matr{L}}$ rows of $\matr{L}$ and $b_{\matr{R}}$ rows of $\matr{R}$ be updated when $b$ known entries are picked, with $b_{\matr{L}} \leq b$ and $b_{\matr{R}} \leq b$.
At this point, the reader might wonder why we need the inequalities $b_{\matr{L}} \leq b$ and $b_{\matr{R}} \leq b$.
One should remember that different data points might correspond to the same row of $\matr{L}$ or $\matr{R}$.
Indeed, to fix the ideas, let us take $b = 3$.
Let us assume that the data points have indices $\parent{i, j_{1}}$, $\parent{i, j_{2}}$ and $\parent{i, j_{3}}$. 
These three different data points only update one row of $\matr{L}$, while they update three rows of $\matr{R}$.
In this case, we thus have $b_{\matr{L}} = 1$ and $b_{\matr{R}} = 3$.

Let $\matr{L}_{b}$ be the corresponding submatrix of $\matr{L}$ with the $b_{\matr{L}}$ rows, i.e., its size is $b_{\matr{L}} \times r$.
Similarly, let $\matr{R}_{b}$ be the corresponding submatrix of $\matr{R}$ with the $b_{\matr{R}}$ rows, i.e., its size is $b_{\matr{R}} \times r$.

An interpretation is that, each time we pick $b$ known entries, we have a subproblem of completing a matrix $\matr{M}_{b}$ of size $b_{\matr{L}} \times b_{\matr{R}}$ with $b$ known entries at indices $\Omega_{b}$.
We try to find a matrix $\matr{X}_{b} = \matr{L}_{b} \matr{R}_{b}^{\top}$ such that $\matr{X}_{b}$ is as close to $\matr{M}_{b}$ as possible on the set $\Omega_{b}$.
Let $\matr{S}_{b}$ be the residual matrix of this subproblem, i.e., $\matr{S}_{b} = \mathcal{P}_{\Omega_{b}} \parent{\matr{L}_{b} \matr{R}_{b}^{\top} - \matr{M}_{b}}$.
The matrix $\matr{S}_{b}$ is of size $b_{\matr{L}} \times b_{\matr{R}}$, just like $\matr{M}_{b}$.
Then, by looking at~\eqref{eq:gradL} and~\eqref{eq:gradR}, the partial derivatives are
\begin{equation*}
    \begin{cases}
       \nabla_{\matr{L}_{b}} f\parent{\matr{L}, \matr{R}} = \matr{S}_{b} \matr{R}_{b}, \\
       \nabla_{\matr{R}_{b}} f\parent{\matr{L}, \matr{R}} = \matr{S}_{b}^{\top} \matr{L}_{b}.
    \end{cases}
\end{equation*}
Hence, by looking at~\eqref{eq:SGD1smart} and the partial derivatives, the classical SGD updates are
\begin{equation}
  \label{sgd:simple}
    \begin{cases}
       \matr{L}_{b, +} = \matr{L}_{b} - \alpha \matr{S}_{b} \matr{R}_{b}, \\
       \matr{R}_{b, +} = \matr{R}_{b} - \alpha \matr{S}_{b}^{\top} \matr{L}_{b}.
    \end{cases}
\end{equation}

At this point, the careful reader will notice that the order matters. 
To fix the ideas, let us assume that we start by computing $\matr{L}_{b, +}$.
After this first step, we hope that $\matr{L}_{b, +}$ is in some sense ``better'' than the previous $\matr{L}_{b}$.
Then, the matrix $\matr{R}_{b, +}$ is computed, and we notice that $\matr{L}_{b}$ appears in this computation.
Since the matrix $\matr{L}_{b, +}$ has already been computed, it would make sense to replace $\matr{L}_{b}$ by $\matr{L}_{b, +}$ in the computation of $\matr{R}_{b, +}$.
This idea comes from the famous forward and backward substitution, in the Gauss–Seidel method, which is explained in~\cite[Section~3.1]{golub1996matrix}.


\begin{comment}
As a reminder, the Gauss–Seidel method is an iterative technique for solving the system of linear equations $\matr{A} \matr{x} = \matr{b}$.
Such a system is easy to solve with by an iterative process called \emph{forward substitution} if $\matr{A}$ is a lower triangular matrix, and analogously back substitution if $\matr{A}$ is an upper triangular matrix.
\end{comment}


However, this technique cannot be used.
Indeed, one must see the pair $\parent{\matr{L}, \matr{R}}$ as a decomposition of the \emph{same variable}.
Therefore, we must update the matrices together.

%\todo[inline]{Better explanation?}
