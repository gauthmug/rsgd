In the machine learning community, the name \emph{learning rate} is often preferred over \emph{step-size}.
The condition $\alpha_{k} \geq 0$ should be verified for all iterations $k$.
Indeed, since the search direction is usually a descent direction, we do not want to reverse the direction.
The challenges in finding a good $\alpha_{k}$ are both in avoiding that the learning rate is too long, or too short.
Therefore, a backtracking line search is defined as in Algorithm~\ref{algo:bls}.
This is heavily used in Riemannian optimization.

%\begin{comment}
\begin{algorithm}
\begin{algorithmic}[0]
\Procedure{Backtracking Line Search}{$\alpha_{0} > 0, \tau \in \parent{0,1}$}
   \State $l \gets 0$
   %\State $e \gets \infty$
   \While{$f\parent{x_{k} + \alpha_{l} p_{k}} > f\parent{x_{k}}$}
      \State $\alpha_{l+1} \gets \tau \alpha_{l}$
      %\State $\v V^{(k+1)} \gets \argmin_{\v V \in \R^{r \times n}} \|\P (\v M - \v U^{(k+1)} \cdot \v V)\|_{\ell_1}$
      %\State $e \gets \|\P (\v M - \v U^{(k)} \cdot \v V^{(k)})\|_{\ell_1} - \|\P (\v M - \v U^{(k+1)} \cdot \v V^{(k+1)})\|_{\ell_1}$
      \State $l \gets l+1$
   \EndWhile   
   \State \textbf{return $\alpha_{l}$} 
\EndProcedure
\end{algorithmic}
\caption{Backtracking Line Search}
\label{algo:bls}
\end{algorithm}


%\todo[inline]{Since we do not use this, we should remove it, right?}


\begin{comment}
This method prevents the step from getting too small, but it does not prevent steps that are too long relative to the decrease in the objective function.
To improve the method, we want to prevent long steps relative to the decrease in the objective function.
We can do this with the \emph{Armijo} condition which is
\begin{equation}
\label{step:armijo}
  f \parent{x_{k} + \alpha_{k} p_{k}} \leq f \parent{x_{k}} + \alpha_{k} \beta g_{k}^{\top} p_{k}.
\end{equation}
\end{comment}



However, since the idea of SGD is to have very cheap iterations, it would not make sense to compute a cheap search direction, and then look for an optimal step-size. 
In other words, it is useless to find the perfect distance if the direction is only a rough estimate.
Therefore, five cheap ways to choose the step-size are discussed.
\begin{enumerate}
  \item \label{item:constant} The easiest choice is to take a constant step-size: $\alpha_{k} = \alpha$ for all $k$.
  
  \item \label{item:geometric} We can also define a geometric sequence: $\alpha_{k} = \rho^{k} \alpha_{0}$ for some initial step-size $\alpha_{0}$ and common ratio $\rho$.
        If $\rho < 1$ then the step-sizes are decreasing; if $\rho > 1$ then the step-sizes are increasing.
  
  \item \label{item:counter} In stochastic approximation, asking that the step-sizes are \emph{square summable but not summable} is a common rule.
        This rule states that
        \begin{equation*}
          \sum_{k = 1}^{\infty} \alpha_{k} = \infty,
          \quad
          \sum_{k = 1}^{\infty} \alpha_{k}^{2} < \infty.
        \end{equation*}
        The first one is the so-called \emph{infinite travel condition}, while the second one ensures that the step-sizes do not ``explode''.
        The intuition is that the step-sizes have to go to zero, but not too fast.
        One typical example is
        \begin{equation*}
          \alpha_{k} = \dfrac{c_{1}}{c_{2} + k},
        \end{equation*}
        where $c_{1} > 0$ and $c_{2} \geq 0$.
        In particular, the simplest choice is $c_{1} = 1$ and $c_{2} = 0$.
        This is a very simple rule, with a count variable, that ensures that the step-size decreases at every iteration.
  
  \item \label{item:bold} The \emph{bold driver heuristic} is a famous heuristic in the case of SGD.
        First, we remind that an epoch is a sweep through the entire training set.
        The heuristic needs to monitor the error after each epoch.
        We have two possibilities.
        \begin{enumerate}
          \item The error decreases, then we can be confident that the direction is good, and the algorithm can move further more aggressively.
                The step-size is increased: $\alpha_{k+1} = \rho \alpha_{k}$, with $\rho = 1.1$, unless stated otherwise.
          \item The error increases, then we want the algorithm to move more cautiously.
                The step-size is decreased: $\alpha_{k+1} = \sigma \alpha_{k}$, with $\sigma = 0.5$, unless stated otherwise.
        \end{enumerate}
        The name of this heuristic comes from the analogy with the learning process of a young and inexperienced car driver.
        %This ``brutally heuristic'' method converges in a time that is comparable to, and usually better than that of standard (batch) backtracking line search with an optimal and fixed learning rate, like the ones described here.
        %The important difference is that the time-consuming \emph{meta-optimization} phase for choosing $\alpha_{k}$ is avoided.
        Of course, different values of $\rho$ and $\sigma$ are possible but performance does not depend critically on this choice.
        
  \item \label{item:exponential} One last common schedule is the \emph{exponential decay protocol}.
        It has the mathematical form
        \begin{equation*}
          \alpha_{k} = \alpha_{0} \cdot \exp{\bracket{-c k}}
        \end{equation*}
        where $\alpha_{0}$ and $c$ are hyperparameters and $k$ is the iteration number.        
\end{enumerate}


Finally, one last method is called \emph{Adagrad}.
It is not included in the list, because it is an expensive method.
It comes from~\cite{duchi11} and it adapts the learning rate to the parameters.
Intuitively, the learning rate is small for parameters associated with frequently occurring features, and it is large for parameters associated with infrequent features.
For this reason, it is well-suited for dealing with sparse data.
This has been used in a very recent field, namely image recognition.
Indeed, the authors of~\cite{Dean:2012:LSD:2999134.2999271} have found that Adagrad greatly improved the robustness of SGD.
Therefore, it was used for training large-scale neural networks at Google.
Among other things, the goal of this particular neural network was to recognize cats in YouTube videos.







