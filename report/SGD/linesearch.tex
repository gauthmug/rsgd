
As we have already said in the introduction of this chapter, the generic line search method consists in two important steps: determining a \emph{search direction} and a \emph{step-size}.
Let us assume that we are minimizing a function $f : \M \mapsto \R$, and that we have access to a retraction $R_{x} : T_{x} \M \mapsto \M$.
We choose an initial iterate, say $x_{0}$, and we set $k=0$.
Until we think that $x_{k}$ is close enough to the optimum $x^{\star}$, we go through four steps.
\begin{enumerate}
  \item Compute a search direction $p_{k}$ from the iterate $x_{k}$.
        Usually we want to ensure that this direction is a descent direction, such that for a small step away from $x_{k}$ in the direction $p_{k}$, the objective function is reduced.        
  \item Compute a suitable step-size $\alpha_{k} > 0$.
        Usually we want to choose $\alpha_{k}$ such that
        \begin{equation}
        \label{step:naive}
          f \parent{x_{k} + \alpha_{k} p_{k}} < f \parent{x_{k}}.
        \end{equation}
        The computation of the step-size is called a line search, and this is usually an inner iterative loop.
  \item Update the iterate: $x_{k+1} \leftarrow R_{x_{k}} \parent{\alpha_{k} p_{k}}$. 
  \item Update the iteration counter: $k \leftarrow k + 1$.
\end{enumerate}

Note that in the sequel it cannot always be guaranteed that $p_{k}$ is a descent direction.
Furthermore, we will not always be able to state that $\alpha_{k}$ satisfies~\eqref{step:naive}.
Finally, the careful reader will have noticed that the third step can be simplified if $\M = \R^{n}$.
Indeed, in this case, it becomes $x_{k+1} \leftarrow x_{k} + \alpha_{k} p_{k}$.

Let us look at a special case to illustrate a well-known example of the generic line search method.
Assume that $f : \R^{n} \mapsto \R$ is a convex and differentiable function.
A review of convex functions can be found in~\cite{boyd2004convex}. 
Let us further assume that the gradient of $f$ is a Lipschitz function with constant $L > 0$, that is
\begin{equation*}
  \norm{\nabla f \parent{\matr{x}} - \nabla f \parent{\matr{y}}} \leq L \norm{\matr{x} - \matr{y}} \ \forall \matr{x}, \matr{y} \in \R^{n}.
\end{equation*} 
Then, let us take $p_{k} = - \nabla f \parent{x_{k}}$ and $\alpha_{k} = \alpha = \frac{1}{L}$.
It is known that these choices satisfy
\begin{equation*}
  f \parent{x_{k}} - f \parent{x^{\star}} \leq \dfrac{L \norm{x_{0} - x^{\star}}^{2}_{2}}{2 k}.
\end{equation*}
This method is called the \emph{gradient descent method}, because the search direction is the gradient.
We see that it has a convergence rate of $\O{\frac{1}{k}}$, or again that it takes $\O{\frac{1}{\epsilon}}$ iterations to obtain $f \parent{x_{k}} - f \parent{x^{\star}} \leq \epsilon$.
This convergence is really slow; it is said to be sublinear.
