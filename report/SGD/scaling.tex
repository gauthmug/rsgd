

Can we take into account more precise information?
The idea is to define scaling factors $s_{\matr{L}}$ and $s_{\matr{R}}$ in order to rewrite~\eqref{sgd:simple} as
\begin{equation}
\label{eq:sgdWithScalingGeneral}
    \begin{cases}
       \matr{L}_{b, +} = \matr{L}_{b} - \alpha \matr{S}_{b} \matr{R}_{b} {\color{blue} s_{\matr{L}}}, \\
       \matr{R}_{b, +} = \matr{R}_{b} - \alpha \matr{S}_{b}^{\top} \matr{L}_{b} {\color{blue} s_{\matr{R}}}.
    \end{cases}
\end{equation}

The scaling factors $s_{\matr{L}}$ and $s_{\matr{R}}$ are small matrices of size $r \times r$.
These factors are first defined in order to take into account \emph{global} information,
then \emph{local} information is taken into account as well.


\subsection{Global curvature} \label{subsec:global}


Newton's optimization algorithm takes into account second-order information.
It can be defined as follows
\begin{equation*}
  x_{k+1} = x_{k} - \alpha_{k} \squared{\nabla^2 f\parent{x_{k}}}^{-1} \nabla f\parent{x_{k}}.
\end{equation*}


In many cases, using the full Hessian information is computationally costly.
As it is explained in~\cite{wright1999numerical}, a popular compromise between convergence and numerical efficiency is to scale the gradient by the diagonal elements of the Hessian.
Hence, Newton's method can be seen as a \emph{scaled} gradient descent algorithm.
This is also explained in~\cite[Section~1.3]{nesterov2003}.
The scaling factors $s_{\matr{L}}$ and $s_{\matr{R}}$ are called \emph{preconditioners}, i.e., approximations of the inverse Hessian.

For a matrix $\matr{A}$, the vectorization operator $\newVec{\matr{A}}$ stacks the columns of $\matr{A}$ on top of each other.
This is formally reviewed in Appendix~\ref{appSec:kronecker}.
Hence, we can define $\newVec{\matr{L}} \in \R^{d_{1} r \times 1}$ and $\newVec{\matr{R}} \in \R^{d_{2} r \times 1}$, and write the objective function $f$ as a function of these vectorizations.
Let us define the variable
\begin{equation*}
  y =
  \parent{
  \begin{array}{c}
    \newVec{\matr{L}} \\
    \newVec{\matr{R}} \\
  \end{array}
  }
  \in \R^{r \parent{d_{1} + d_{2}} \times 1}.
\end{equation*}
The full Hessian matrix with respect to $y$ is a square matrix of size $r \parent{d_{1} + d_{2}} \times r \parent{d_{1} + d_{2}}$.
One can prove graphically that $f$, i.e., the objective function~\eqref{eq:ObjectiveLR}, is not jointly convex in $\newVec{\matr{L}}$ and $\newVec{\matr{R}}$, hence the Hessian is not necessarily positive definite.
However, $f$ is strictly convex in $\newVec{\matr{L}}$ if $\newVec{\matr{R}}$ is fixed, and \emph{vice versa}, hence the diagonal elements of the Hessian are strictly positive.
We compute
\begin{equation}
\label{eq:weirdHessian}
  \nabla^{2}_{y} f \parent{y}
  = 
  \begin{bmatrix}
      \nabla^{2}_{\newVec{\matr{L}}} f\parent{\matr{L}, \matr{R}} & \star  \\
      \star                                                       & \nabla^{2}_{\newVec{\matr{R}}} f\parent{\matr{L}, \matr{R}}
  \end{bmatrix}
  \in \R^{r \parent{d_{1} + d_{2}} \times r \parent{d_{1} + d_{2}}},
\end{equation}
where the stars $\star$ denote the off-diagonal blocks.
Hence, we have
\begin{equation*}
  \newDiag{\nabla^{2}_{y} f \parent{y}}
  = 
  \begin{bmatrix}
      \newDiag{\nabla^{2}_{\newVec{\matr{L}}} f\parent{\matr{L}, \matr{R}}} & \matr{0}_{d_{1} r \times d_{2} r}  \\
      \matr{0}_{d_{2} r \times d_{1} r}                                     & \newDiag{\nabla^{2}_{\newVec{\matr{R}}} f\parent{\matr{L}, \matr{R}}}
  \end{bmatrix}
  \in \R^{r \parent{d_{1} + d_{2}} \times r \parent{d_{1} + d_{2}}}.
\end{equation*}



The Hessian matrices are computed as
\begin{equation*}
    \begin{cases}
       \nabla^{2}_{\newVec{\matr{L}}} f\parent{\matr{L}, \matr{R}} = \matr{R}^{\top} \matr{R} \otimes \matr{I}_{d_{1} \times d_{1}} \in \R^{r d_{1} \times r d_{1}}, \\
       \nabla^{2}_{\newVec{\matr{R}}} f\parent{\matr{L}, \matr{R}} = \matr{L}^{\top} \matr{L} \otimes \matr{I}_{d_{2} \times d_{2}} \in \R^{r d_{2} \times r d_{2}},
    \end{cases}
\end{equation*}
where $\otimes$ denotes the Kronecker product, which is also reviewed in Appendix~\ref{appSec:kronecker}.
The diagonalizations are 
\begin{equation}
\label{eq:incorrect}
    \begin{cases}
       \newDiag{\nabla^{2}_{\newVec{\matr{L}}} f\parent{\matr{L}, \matr{R}}} = \newDiag{\matr{R}^{\top} \matr{R}} \otimes \matr{I}_{d_{1} \times d_{1}}, \\
       \newDiag{\nabla^{2}_{\newVec{\matr{R}}} f\parent{\matr{L}, \matr{R}}} = \newDiag{\matr{L}^{\top} \matr{L}} \otimes \matr{I}_{d_{2} \times d_{2}},
    \end{cases}
\end{equation}
where $\newDiag{\matr{R}^{\top} \matr{R}}$ is a diagonal matrix extracting the diagonal of $\matr{R}^{\top} \matr{R}$, and similarly for $\matr{L}^{\top} \matr{L}$.

%\todo[inline]{Equation~\eqref{eq:incorrect} looks incorrect.}

%\todo[inline]{How do we know it's not $\matr{I}_{d_{1} \times d_{1}} \otimes \matr{R}^{\top} \matr{R}$ instead of $\matr{R}^{\top} \matr{R} \otimes \matr{I}_{d_{1} \times d_{1}}$?}



%%%%%%%%%%%%%%

\begin{comment}
Hence we find the scaling factors
\begin{equation*}
    \begin{cases}
       s_{\matr{L}} = \squared{\diag \parent{\matr{R}^{\top} \matr{R}}}^{-1}, \\
       s_{\matr{R}} = \squared{\diag \parent{\matr{L}^{\top} \matr{L}}}^{-1},
    \end{cases}
\end{equation*}
that we can incorporate in~\eqref{eq:sgdWithScalingGeneral},
where $\diag \parent{\matr{R}^{\top} \matr{R}}$ is a diagonal matrix extracting the diagonal of $\matr{R}^{\top} \matr{R}$, and similarly for $\matr{L}^{\top} \matr{L}$.
This seems to be a reasonable scaled update for algorithms directed towards the matrix completion problem~\eqref{LRMC:prob}.
This is because, when we complete a low-rank matrix, we implicitly try to learn the dominant subspace, by learning on a smaller set of known entries. 
\end{comment}

%%%%%%%%%%%%%%

Now, let us talk about metrics. 
The same notations as in Section~\ref{sec:riem:Mr} are used.
The pair $\parent{\matr{L}, \matr{R}}$ is denoted by $x \in \overline{\M}_{r}$.
As a reminder, the total space is $\overline{\M}_{r} = \R^{d_{1} \times r}_{*} \times \R^{d_{2} \times r}_{*}$.
The tangent space of the total space at $x = \parent{\matr{L}, \matr{R}} \in \overline{\M}_{r}$ has the expression
\begin{equation}
\label{eq:tangentAlmostEuclidean}
  T_{x} \overline{\M}_{r} = \R^{d_{1} \times r}\times \R^{d_{2} \times r}.
\end{equation}
The metric that is proposed in~\cite{BM-1211.1550} is defined as $g_{x}$ and maps $\xi_{x}, \eta_{x} \in T_{x} \overline{\M}_{r}$ to a scalar value in $\R$.
The tangent vector $\xi_{x} \in T_{x} \overline{\M}_{r}$ has $\parent{\xi_{\matr{L}}, \xi_{\matr{R}}}$ as matrix representation,
i.e., $\xi_{\matr{L}} \in \R^{d_{1} \times r}$ and $\xi_{\matr{R}} \in \R^{d_{2} \times r}$,
and similarly for $\eta_{x} \in T_{x} \overline{\M}_{r}$.
We have
\begin{equation*}
  g_{x} \parent{\xi_{x}, \eta_{x}}
  = \newVec{\xi_{\matr{L}}}^{\top} \parent{\matr{R}^{\top} \matr{R} \otimes \matr{I}_{d_{1} \times d_{1}}} \newVec{\eta_{\matr{L}}}
  + \newVec{\xi_{\matr{R}}}^{\top} \parent{\matr{L}^{\top} \matr{L} \otimes \matr{I}_{d_{2} \times d_{2}}} \newVec{\eta_{\matr{R}}},
\end{equation*}
and \eqref{eq:niceTrace} enables us to rewrite this as
\begin{equation}
\label{eq:metricGlobal}
  g_{x} \parent{\xi_{x}, \eta_{x}}
  = \Tr \bracket{\parent{\matr{R}^{\top} \matr{R}} \xi_{\matr{L}}^{\top} \eta_{\matr{L}}}
  + \Tr \bracket{\parent{\matr{L}^{\top} \matr{L}} \xi_{\matr{R}}^{\top} \eta_{\matr{R}}},
\end{equation}
with $\Tr \bracket{\cdot}$ the matrix trace operator.

%\begin{comment}
We end up with the scaling factors
\begin{equation}
\label{SGD:Netwon}
    \begin{cases}
       s_{\matr{L}} = \squared{\matr{R}^{\top} \matr{R}}^{-1}, \\
       s_{\matr{R}} = \squared{\matr{L}^{\top} \matr{L}}^{-1},
    \end{cases}
\end{equation}
that are incorporated in~\eqref{eq:sgdWithScalingGeneral}.
%\end{comment}

\subsection{Local and global curvature} \label{subsec:localAndGlobal}

Starting from~\eqref{eq:metricGlobal}, let us look at the first of the two traces.
The square matrix $\matr{R}^{\top} \matr{R}$ can be seen as containing \emph{global} second order information of the cost function, since it captures knowledge about the entire matrix $\matr{R}$.
However, in a stochastic setup, we only modify $b_{\matr{R}}$ rows of $\matr{R}$, namely we only update the submatrix $\matr{R}_{b}$.
In~\cite{DBLP:journals/corr/MishraS16a}, this leads to the argument that more weight should be given to the submatrix $\matr{R}_{b}^{\top} \matr{R}_{b}$, since it contains \emph{local} second order information of the cost function.
Therefore, $\parent{\matr{R}^{\top} \matr{R}}$ in~\eqref{eq:metricGlobal} is replaced by $\squared{\mu \parent{\matr{R}^{\top} \matr{R}} + \parent{1 - \mu} \parent{\matr{R}_{b}^{\top} \matr{R}_{b}}}$,
for some non-negative scalar $\mu \in \squared{0, 1}$ that weights $\matr{R}^{\top} \matr{R}$ and $\matr{R}_{b}^{\top} \matr{R}_{b}$ differently.
A normalization factor $\frac{b}{d_{\max}}$ has to be added to the first term in this sum, to ensure that the Frobenius norm of $\matr{R}^{\top} \matr{R}$ and $\matr{R}_{b}^{\top} \matr{R}_{b}$ are of the same order.
%Hence, we end up replacing $\parent{\matr{R}^{\top} \matr{R}}$ in~\eqref{eq:metricGlobal} by $\squared{\frac{b}{d_{\max}} \mu \parent{\matr{R}^{\top} \matr{R}} + \parent{1 - \mu} \parent{\matr{R}_{b}^{\top} \matr{R}_{b}}}$,
Of course, we do the same thing for $\matr{L}$.
Thus, the metric~\eqref{eq:metricGlobal} becomes
\begin{equation}
\label{eq:metricGlobalAndLocal}
\begin{split}
  g_{x} \parent{\xi_{x}, \eta_{x}}
  &= \Tr \bracket{\squared{\frac{b \mu}{d_{\max}}  \parent{\matr{R}^{\top} \matr{R}} + \parent{1 - \mu} \parent{\matr{R}_{b}^{\top} \matr{R}_{b}}} \xi_{\matr{L}}^{\top} \eta_{\matr{L}}} \\
  & \quad + \Tr \bracket{\squared{\frac{b \mu}{d_{\max}} \parent{\matr{L}^{\top} \matr{L}} + \parent{1 - \mu} \parent{\matr{L}_{b}^{\top} \matr{L}_{b}}} \xi_{\matr{R}}^{\top} \eta_{\matr{R}}}.
\end{split}
\end{equation}

The scaling factors are defined as
\begin{equation}
\label{eq:scalingFactorsScaledSGD}
    \begin{cases}
      s_{\matr{L}} = \squared{\frac{b \mu}{d_{\max}} \parent{\matr{R}^{\top} \matr{R}} + \parent{1 - \mu} \parent{\matr{R}_{b}^{\top} \matr{R}_{b}}}^{-1}, \\
      s_{\matr{R}} = \squared{\frac{b \mu}{d_{\max}} \parent{\matr{L}^{\top} \matr{L}} + \parent{1 - \mu} \parent{\matr{L}_{b}^{\top} \matr{L}_{b}}}^{-1},
    \end{cases}
\end{equation}
that are incorporated in~\eqref{eq:sgdWithScalingGeneral}.
Overall, these scaling factors act as efficient \emph{preconditioners} for the standard SGD updates~\eqref{sgd:simple}, as it was seen above.


\begin{comment}
where $\bar{d} = \max\bracket{d_{1}, d_{2}}$, $\frac{b}{\bar{d}}$ is a normalization parameter,
and $\mu$ is a nonnegative scalar in $\squared{0,1}$ that weights the \emph{full} matrices and the \emph{batch} matrices differently,
i.e., for $s_{\matr{L}}$ it weights $\matr{R}^{\top} \matr{R}$ and $\matr{R}_{b}^{\top} \matr{R}_{b}$ differently, and similarly for $s_{\matr{R}}$.
This suggestion comes from~\cite{DBLP:journals/corr/MishraS16a}.
\end{comment}

Let us analyze the two extreme cases.
If on the one hand $\mu = 0$, then we only use local curvature information.
If on the other hand $\mu = 1$, then we only use global curvature information, and we are back at~\eqref{SGD:Netwon}, except that we have added a normalization factor $\frac{b}{d_{\max}}$.


%\todo[inline]{Is it necessary to do a itemization here?}

For any other $\mu \in \parent{0,1}$, we take both the local and the global information into account. 
A short analysis of the effect of $\mu$ can be found in~\cite{DBLP:journals/corr/MishraS16a}.
In problem instances where numerous entries are already known, i.e., $\abs{\Omega}$ is large, the influence of $\mu$ is minimal.
However, for ill-conditioned data, making use of local information is more critical, and a smaller value of $\mu$ is more appropriate, e.g., $\mu = 0.5$.
Since the goal of this master's thesis is not to make this analysis, we will always take $\mu = 0.5$, unless stated otherwise.


\subsection{Scale invariance}

The issue of \emph{scale invariance} refers to the behavior of algorithms which behave equivalently when initialized, say, either with
$\parent{\matr{L}_{0}, \matr{R}_{0}}$ or with $\parent{\matr{L}_{0} \matr{T}^{-1}, \matr{R}_{0} \matr{T}^{\top}}$ for all non-singular matrices $\matr{T} \in \text{GL}\parent{r}$.
The scaling that has been described resolves the issue of scale invariance that exists in matrix factorization models.
Indeed, as it was stated in Subsection~\ref{subsec:riem:fact}, the non-uniqueness of matrix factorization implies that $\matr{X}$ remains unchanged under the action~\eqref{eq:invariance}, for all non-singular matrices.
%Equivalently, $\matr{X} = \matr{L} \matr{R}^{\top} = \matr{L} \matr{T}^{-1} \parent{\matr{R} \matr{T}^{\top}}^{\top}$. 
It is straightforward to show that the scaled SGD updates~\eqref{eq:sgdWithScalingGeneral} with~\eqref{eq:scalingFactorsScaledSGD} are scale invariant, whereas the standard SGD updates~\eqref{sgd:simple} are not:
let us use $\matr{L}_{b, +}$ and $\tilde{\matr{L}}_{b, +}$ to denote the update if the algorithm is initialized with
$\parent{\matr{L}_{0}, \matr{R}_{0}}$ and $\parent{\matr{L}_{0} \matr{T}^{-1}, \matr{R}_{0} \matr{T}^{\top}}$ respectively.
The same notation is used for $\matr{R}$.
With the scaled SGD, we find
\begin{equation*}
    \begin{cases}
       \tilde{\matr{L}}_{b, +} = \matr{L}_{b, +} \matr{T}^{-1}, \\
       \tilde{\matr{R}}_{b, +} = \matr{R}_{b, +} \matr{T}^{\top},
    \end{cases}
\end{equation*}
which enables us to prove scale invariance.
For the standard SGD, such a result cannot be obtained. 


%\todo[inline]{This has already been introduced in Section 2, careful not to repeat!}