In this repository, you will find my Master thesis [EPL1617-1143].

Title: Stochastic Gradient Methods for Matrix Completion

Supervisors: Pierre-Antoine ABSIL, Rodolphe SEPULCHRE

Readers: Estelle MASSART, Shuyu DONG

Slides: www.bit.ly/EPL18

For a precise README dedicated to the code, see the file "READMECode.txt" in the folder "code".