Notes mémoire

%%%%%%%%%%%%%

minutes Skype Rodolphe Sepulchre
date: 14 avril 2017

PCA

PAA et Rodolphe ont écrit un livre sur l’optimisation sur des variétés ("optimisation on manifolds")

trouver la plus grande valeur propre, avec des certaines propriétés (creuses, rang faible)

Netflix challenge (matrix competition)
rows: users
columns: movies
matrix = rating
entries (1 to 5)
most of the entries are “Not Available”
we want to full in the matrix: with the hypothesis that the matrix is of low rank, we can factorize the metric with “types of users” and “types of movies”. then the number of entries of the new matrix is equal to the number of known entries in the original matrix
—> the problem becomes a regression problem

si on rajoute les contraintes de rang, le problème d’optimisation devient super dur (non convexe, bcp de non-linéarités) 

on optimise sur un espace de matrice avec un rang fixé —> optimisation on manifold known

comment adapter l’algorithme quand on reçoit les données petit à petit?
	⁃	tout recommencer?
	⁃	rajouter une itération?
	⁃	= online learning


on tourne toujours autour des mêmes questions: on veut approximer des matrices, avec des certaines propriétés

les problèmes élémentaires dans un espace linéaire
	⁃	de très grande taille
	⁃	appartienne à un espace non-linéaire


but du TFE: appliquer cette thèse à un sujet que je vais découvrir à Lausanne, par exemple


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

date: lundi 11 septembre 2017
minutes PAA

stochastique gradient method + matrix completion
fonction objectif = grande somme de termes positifs ou nuls

%%%%%%%%%%%%%%%%%%%%%%%%%%

date: octobre 2017
minutes PAA

Piste de Hendrickx: biaiser la somme pour introduire de l’indépendance

les entrées ne sont pas choisies de manière indépendante
du coup il faut faire attention aux poids pour s’assurer que XXX

la fonction objectif de Léopold est une grande somme de terme, donc on peut utiliser le shuffling de Estelle

avoir une formule pour le gradient des différents termes




%%%%%%%%%%%%%%%%%%%%%%%%%

date: lundi 19 février
minutes Rodolphe, première réunion à Cambridge

il y a différentes variétés, et on peut appliquer des algorithmes classiques (gradient, CG, Newton, quasi-Newton) sur chaque variété donc ça fait pas mal de possibilités
lire le papier de Bamdev: https://arxiv.org/abs/1603.04989

deviner le rang de la matrice pour faire l’hypothèse de rang faible: pourrait-on essayer quelque chose comme le “gap statistic” ? Voir: https://statweb.stanford.edu/~gwalther/gap
--> il n'y a pas de "vrai rang"



questions à poser à PAA
tensor completion ?
MATLAB or Python ? PyManOpt instead of ManOpt
find the best rank of the matrix ?

%%%%%%%%%%%%%%%%%%%%%%%%%%

date: lundi 26 mars 2018
minutes PAA & Estelle Massart

Question. On a souvent l'hypothèse de rang faible, donc X = LR^{T}. Est-ce qu'on doit optimiser L et R individuellement, comme Bamdev (https://arxiv.org/pdf/1603.04989.pdf), ou bien est-ce qu'on doit optimiser X directement ?
Réponse. On ne construit jamais la matrice X en pratique, donc il est évident qu'on optimise L et R individuellement 


Question. Comment faire du shuffling si le "batch size" est plus grand que un ? Et comment faire du shuffling quand le "batch size" change avec les itérations ?
Réponse. A moi de chercher. En attendant, la question de base est: est-ce utile de faire du shuffling avec le problème de complétion de matrices. Le reste, on verra après

Question. Comment comparer la vitesse de convergence ? que montrer sur les plots ?
Réponse. Dans la compétition de Netflix, on montre souvent la RMSE, qu'on veut minimiser. On montre ça sur l'axe y. Sur l'axe x, on montre le nombre d'itérations ou alors le temps de calcul. 


Question. Faisons-nous vraiment de l'optimisation sur une variété ? Je n'utilise même pas Manopt...
Réponse. Oui, on utilise des métriques, qui définissent la notion de plus forte pente. Vu que la variété est matricielle, c'est normal qu'un tas d'opérations matricielles soient utilisées.
TO DO: regarder d'autres métriques de Bamdev; métrique sur un espace quotient ou non; justifier pourquoi une métrique est meilleure qu'une autre pour tel ou tel problème 


%%%%%%%%%%%%%%%%%%%%%%

date: mardi 27 mars 2019
minutes presentation skills

ne pas se tenir derrière la table
ne pas toujours montrer le plan
S'il faut montrer le plan, on le montre après l'accroche et l'intro
ne pas montrer des slides avec que des maths ! 
ne pas utiliser un template
montrer sa capacité de recul et de synthèse
faire attention à la visibilté des figures



ne pas commencer avec "Hi everyone"
ne pas dire "okay... so... eum..."
ne pas mettre la date sur le premier slide
ne pas mettre le logo




ne pas donner un cours magistral, plutôt donner un aperçu et expliquer pourquoi c'est utile
soit les gens regardent les slides, soit ils t'écoutent
ne pas dire "ceci sera facile, vous verrez"



ne pas suivre l'ordre chronologique du mémoire dans la présentation


mettre les images en full screen

ne pas regarder ses slides
ne pas lire son texte
il dit beaucoup "alors"
utiliser des abréviations



%%%%%%%%%%%%%%%%

note to self: écrire un résumé au début de chaque section pour rappeler le but de la section
qui doit comprendre le mémoire ? un étudiant lambda en Master2 MAP 



%%%%%%%%%%%%%%%

date: lundi 23 avril 2018
minutes PAA

Question. Est-ce que la longueur de pas est importante ? Comment appliquer Armijo si on n'a pas accès au gradient ? Quel choix est fait dans le papier d'Estelle ?
Réponse. Oui, la longueur de pas doit être décroissante, et Armijo n'est pas d'actualité ici parce qu'on n'a pas accès au gradient complet.
Question. Shuffling impair ?

Question. Est-ce qu'on peut parcourir tous les points, plusieurs fois ?
Réponse. Oui, mais alors on ne pourra pas résoudre le problème de Netflix. Ce n'est pas grave, on peut regarder d'autres applications de complétion matricielle.
Exemples:
- http://goldberg.berkeley.edu/jester-data/
- http://bit.ly/2FOYb1J

Question. Est-ce que c'est okay de ne pas parcourir tous les points ?
Réponse. Oui, c'est okay

Question. Comment utiliser la technique de "averaging" de l'équation (2) dans https://arxiv.org/pdf/1802.09128.pdf ?
Réponse. D'abord calculer une suite d'itérés, puis on calcule une nouvelle suite


TODO: éventuellement le faire en C++ ?
PAA: différences entre C++ et matlab: si les matrices sont grandes, pas de différence; si les matrices sont petites, C++ sera plus rapide (efficacité pour faire des boucles)
temps de calcul des BLAS

TODO: regarder la courbure de \M_{r}



%%%%%%%%%%%%%%%

date: lundi 7 mai 2018
minutes PAA

Question. Est-ce que notre technique est okay pour un "online update" ?
Réponse. Non

Question. Stepsize
Réponse. Suite de stepsize décroissante avec méthode de count
				+ pas initial bien choisi à la Nesterov (estimer la constante de Lipschitz) 

Question. Comment implémenter la rétraction de 1802.09128 ?
Réponse. Voir ici: https://sites.uclouvain.be/absil/2013.04

Question. Notation de $A_{ij}$ ou $\matr{A}_{ij}$ ?
Réponse. Plutôt $A_{ij}$

Question. Comment représenter une expérience random ? Dix essais, moyennes et deviations standard ?

Question. Date à laquelle je peux envoyer une version V1 à faire relire ?
Réponse. Lundi 14 mai, matin


%%%%%%%%%%%%%%%

Question. Est-ce normal que je n'aie encore aucune preuve ? Quid de la preuve sur quotient manifold ?
Réponse. Ce n'est pas un drame.

Question. Step-size ? Suggestion: bold driver. Problème: ce n'est pas une suite décroissante, donc on sort du cadre du papier d'Estelle
Réponse. A tester. On peut essayer d'éviter que le step-size augmente quand on s'approche de l'optimum.

Question. Optimisation. On garde le précédent si on se rend compte que l'on a bougé dans la mauvaise direction ?
Réponse. En réalité, on ne veut pas évaluer la fonction de cout.

Question. Comment empêcher de commencer par monter ?

Question. Est-ce que notre méthode est vraiment stochastique ? J'ai l'impression qu'on ne fait que parcourir les points dans un certain ordre, au lieu d'un autre, mais ou parcourt de toutes façon tous les points...

Question. Que pensez-vous de Digital McKinsey ?
Réponse. Trop générique.

Question. Noir et rouge ou ligne pleine et ligne pointillée ? Qui doit imprimer le mémoire ?
Réponse. A toi de choisir. Oui, le mémoire devra être imprimé, par moi.

Question. Quid des codes MATLAB ?
Réponse. Les mettre sur Bitbucket, pas besoin de les soumettre sur dial.mem, et surtout pas en Annexes.

Question. Talk about complexity ? If yes, example ?
Réponse. Difficile mais bonne idée.

Question. Couper le Chapitre 3 en deux parties ?
Réponse. Oui

Question. Enlever la liste de figures et de tables ?
Réponse. Peut-être


%%%%%%%%%%%%%%% 



