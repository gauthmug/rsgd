function p = riffle_shuffle(p,flag)
% Performs one riffle shuffle
%
% p = riffe_shuffle(p, option)
%
% % Parameters
% -----------
% INPUT
%   p: original vector to be shuffled
%   option: string that indicates if we do an in-shuffle or an out-shuffle
%
% OUTPUT
%   p: shuffled vector
%
%
%
% For more information on riffle shuffles, see
% http://mathworld.wolfram.com/RiffleShuffle.html
%
% Authors:
%   P.-A. Absil, 2015-07-30
%   Update E.Massart September 2015.
%   Source: https://github.com/EMassart/InductiveSequences/blob/master/methods/riffle_shuffle.m
%
%   Slight modifications by
%   Gauthier Muguerza, <gauthier.muguerza@gmail.com>, 2018
%
% The implementation is a bit intricate but it avoids for loops.
% It is inspired from
% http://www.mathworks.com/matlabcentral/fileexchange/16919-interleave



% Let's say that p(1) is the bottom of the deck and p(end) the top.

if mod(length(p),2)==0
    %fprintf('**** Even \n');
    l = length(p)/2;
    
    p_1 = p(1:l); % bottom half deck (in the right hand)
    p_2 = p(l+1:end); % top half deck (in the left hand)
    
    
    % Prepare a cell in which to put the two half decks "side by side":
    p_tmp = cell(2,l);
    
    % Interleave the two half decks
    switch flag
        case 'in'
            %fprintf('**** In-shuffle \n');
            p_tmp(1,1:l) = num2cell(p_2);
            p_tmp(2,1:l) = num2cell(p_1);
        case 'out'
            %fprintf('**** Out-shuffle \n');
            p_tmp(1,1:l) = num2cell(p_1);
            p_tmp(2,1:l) = num2cell(p_2);
    end
    
    
else
    %fprintf('**** Odd \n');
    l = floor(length(p)/2);
    switch flag
        case 'in'
            %fprintf('**** In-shuffle \n');
            p_1 = p(1:l);
            p_2 = p(l+1:end);
            
            p_tmp = cell(2,l+1);
            p_tmp(1,1:l+1) = num2cell(p_2);
            p_tmp(2,1:l) = num2cell(p_1);
            %flag = 'out';
            
        case 'out'
            %fprintf('**** Out-shuffle \n');
            p_1 = p(1:l+1);
            p_2 = p(l+2:end);
            
            p_tmp = cell(2,l+1);
            p_tmp(1,1:l+1) = num2cell(p_1);
            p_tmp(2,1:l) = num2cell(p_2);
            %flag = 'in';
    end
    
end

p = [p_tmp{:}];

end
