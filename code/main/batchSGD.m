function [X, infos] = batchSGD(Xinit, n, m, r, data_ls, data_ts, options)
% Scaled batch stochastic gradient descent algorithm for matrix completion.
%
% [X, infos] = batchSGD(Xinit, n, m, r, data_ls, data_ts, options)
%
%
% Parameters
% -----------
% Xinit: Stuctured array with low-rank factors, Xinit.L and Xinit.R
% n: number of rows
% m: number of colummns
% r: rank
%
% data_ls: data_ls.rows: rows of the given entries
%        : data_ls.cols: columns of the given entries
%        : data_ls.entries: values of the given entries
%        : data_ls.nentries: number of given entries
%
% data_ts: data_ts.rows: rows of the test entries
%        : data_ts.cols: columns of the test entries
%        : data_ts.entries: values of the test entries
%        : data_ts.nentries: number of test entries
%
% options.maxiterations: maximum iterations
% options.msetol: tolerance on the mean square error
% options.batchflag: TODO
% options.batchsize: TODO
% options.relresidualtol: tolerance on the relative residual error
% options.mu: the parameter that combines local and global Hessian information
% options.lambda: regularization parameter
% options.stepsize: initial step-size. Default is use a guess. described below.
% options.shuffle: string that indicates how the data points are read through
%
% Output:
% -------
% X: Structured array with final low-rank factors, X.L and X.R
% infos: Structured array with additional information
%
%
% Bamdev Mishra and Rodolphe Sepulchre,
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.


% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with slight modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
% inspired from
% Estelle Massart: https://sites.uclouvain.be/absil/2016-12/article_inductive_v2_techrep.pdf



% Set default parameters
if isempty(Xinit)
    Xinit.L = randn(n, r);
    Xinit.R = randn(m, r);
end
X = Xinit;

if ~isfield(options, 'shuffle') || isempty(options.shuffle)
    options.shuffle = 'no';
end
if ~isfield(options,'maxiterations'); options.maxiterations = 100; end % Max iterations
if ~isfield(options,'batchflag'); options.batchflag = 'constant'; end % Batch flag
if ~isfield(options,'batchsize'); options.batchsize = 1; end % Batchsize
if ~isfield(options,'msetol'); options.msetol = 1e-8; end % Tolerance on the mean square error
if ~isfield(options,'relresidualtol'); options.relresidualtol = 1e-4; end % Tolerance on the relative residual error
if ~isfield(options,'mu'); options.mu = 0.5; end % Mu parameter
if ~isfield(options,'lambda'); options.lambda = 0; end % Regularization parameter


% Options
maxiterations = options.maxiterations;
batchflag = options.batchflag;
batchsize = options.batchsize;
msetol = options.msetol;
relresidualtol = options.relresidualtol;
mu = options.mu;
lambda = options.lambda;
shuffle = options.shuffle;




meansqentries_ls = (data_ls.entries'*data_ls.entries)/data_ls.nentries;
M = data_ls.nentries; % Number of known entries.
LtL = X.L'*X.L; % Global curvature information
RtR = X.R'*X.R;



if ~isfield(options, 'stepsize') || isempty(options.stepsize)
    
    fprintf('hello');
    if shuffle
        idxentries = randperm(M, 10);
    else
        idxentries = 1:10;
    end
    
    idxrows = data_ls.rows(idxentries);
    idxcols = data_ls.cols(idxentries);
    entries_batchsize = data_ls.entries(idxentries);
    % Assuming no repetitions.
    [idxrowsshort, ~, indrowsfromshort, lengthrowsshort] = myunique(idxrows, batchsize);
    [idxcolsshort, ~, indcolsfromshort, lengthcolsshort] = myunique(idxcols, batchsize);
    [omega_batchsize, indicator_batchsize] = create_omega_batchsize(entries_batchsize, lengthrowsshort, lengthcolsshort, indrowsfromshort, indcolsfromshort, batchsize);
    
    % Batch size
    X_batchsize.L = X.L(idxrowsshort, :);
    X_batchsize.R = X.R(idxcolsshort, :);
    
    
    % Local + Global
    scaleLtL = (mu*batchsize/n)*LtL + (1 - mu)*(X_batchsize.L'*X_batchsize.L);
    scaleRtR = (mu*batchsize/m)*RtR + (1 - mu)*(X_batchsize.R'*X_batchsize.R);
    
    
    % Current residual and gradient computations
    residual_batchsize = (indicator_batchsize.*(X_batchsize.L*X_batchsize.R')) ...
        - omega_batchsize;
    
    
    % Batch size: gradient computation
    grad_X_batchsize.L = residual_batchsize*X_batchsize.R;
    grad_X_batchsize.R = residual_batchsize'*X_batchsize.L;
    
    % Scaled gradient computation
    scaledgrad_X_batchsize.L = grad_X_batchsize.L/scaleRtR;
    scaledgrad_X_batchsize.R = grad_X_batchsize.R/scaleLtL;
    
    % Stepsize
    options.stepsize = 0.5*stepsize_guess(X_batchsize, scaledgrad_X_batchsize, residual_batchsize, indicator_batchsize);
    
end
stepsize = options.stepsize;








% Global cost computation
preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
errors_ls = preds_ls - data_ls.entries;
cost_ls = ((errors_ls'*errors_ls) + lambda*(LtL(:)'*RtR(:)))/data_ls.nentries;

% Global gradient computation, for comparisons
residual = sparse(data_ls.rows,data_ls.cols,errors_ls,n,m);
sqgradnorm = norm(residual*X.R + lambda*(X.L*RtR), 'fro')^2 ...
    +norm(residual'*X.L + lambda*(X.R*LtL), 'fro')^2;
gradnorm = 2*sqrt(sqgradnorm)/M;


fprintf('Iteration %0.3d, Cost %7.3e, Gradnorm %7.3e\n', 0, cost_ls, gradnorm);

% If interested in computing recovery
if ~isempty(data_ts)
    meansqentries_ts = (data_ts.entries'*data_ts.entries)/data_ts.nentries;
    
    preds_ts = sum(X.L(data_ts.rows,:).*X.R(data_ts.cols,:), 2);
    errors_ts = preds_ts - data_ts.entries;
    cost_ts = (errors_ts'*errors_ts)/data_ts.nentries;
    infos.cost_test = cost_ts;
    infos.relresidual_test = sqrt(cost_ts/meansqentries_ts);
    
    infos.nmae_jester = sum(abs(errors_ts))/(20*data_ts.nentries);
    
end

% Initialization
infos.cost = cost_ls;
infos.time = 0;
infos.gradnorm = gradnorm;
infos.relresidual = sqrt(cost_ls/meansqentries_ls);
infos.numpasses = 0;




switch batchflag
    case 'increase'
        batchsize = 1;
        batchIncrease = floor(M/maxiterations);
end

% Number of datapoints that are being visited
count = 0;

% Main code
for outiter = 1 : maxiterations
    
    
    % Begin the time counter for the epoch
    t_begin = tic();
    
    switch batchflag
        case 'increase'
            batchsize = batchsize + batchIncrease;
        case 'shuffle'
            batchsize = randi(M);
    end
    
    %batchsize
    
    count = count + batchsize;
    
    entries_order = randi(M,1,M);
    
    % Pick a batchsize of rows, cols, and entries
    idxentries = entries_order(1 : min(batchsize, M));
    if min(batchsize, M) == M
        idxentries = entries_order(end - batchsize + 1 : end);
    end
    
    %idxentries
    
    
    idxrows = data_ls.rows(idxentries);
    idxcols = data_ls.cols(idxentries);
    entries_batchsize = data_ls.entries(idxentries);
    
    % Assuming repetitions.
    % idxrows (and idxcols) could contain repeats for the current
    % batchsize. Hence, we take only the unique rows and columns
    % that are required and form a small incomplete matrix
    % omega_batchsize and the corresponding indicator matrix.
    % For example, idxrows ---> idxrowsshort and
    % idxcols ---> idxcolsshort.
    [idxrowsshort, ~, indrowsfromshort, lengthrowsshort] = myunique(idxrows, batchsize);
    [idxcolsshort, ~, indcolsfromshort, lengthcolsshort] = myunique(idxcols, batchsize);
    [omega_batchsize, indicator_batchsize] = create_omega_batchsize(entries_batchsize, lengthrowsshort, lengthcolsshort, indrowsfromshort, indcolsfromshort, batchsize);
    
    
    
    % Batch size
    X_batchsize.L = X.L(idxrowsshort, :);
    X_batchsize.R = X.R(idxcolsshort, :);
    
    
    % Local + Global
    scaleLtL = (mu*batchsize/m)*LtL + (1 - mu)*(X_batchsize.L'*X_batchsize.L); % max(m, n) = m;
    scaleRtR = (mu*batchsize/m)*RtR + (1 - mu)*(X_batchsize.R'*X_batchsize.R);
    
    
    % Current residual and gradient computations
    residual_batchsize = (indicator_batchsize.*(X_batchsize.L*X_batchsize.R')) ...
        - omega_batchsize;
    
    
    % Batch size: gradient computation
    grad_X_batchsize.L = residual_batchsize*X_batchsize.R  + lambda*(X_batchsize.L*RtR);
    grad_X_batchsize.R = residual_batchsize'*X_batchsize.L + lambda*(X_batchsize.R*LtL);
    
    % Scaled gradient computation
    scaledgrad_X_batchsize.L = grad_X_batchsize.L/scaleRtR;
    scaledgrad_X_batchsize.R = grad_X_batchsize.R/scaleLtL;
    
    
    %  Batch size updates
    stepsizescaledgrad_X_batchsizeL = stepsize*scaledgrad_X_batchsize.L;
    stepsizescaledgrad_X_batchsizeR = stepsize*scaledgrad_X_batchsize.R;
    
    X_batchsize_new.L = X_batchsize.L - stepsizescaledgrad_X_batchsizeL;
    X_batchsize_new.R = X_batchsize.R - stepsizescaledgrad_X_batchsizeR;
    
    % Update the relevant the rows of L and R.
    X.L(idxrowsshort, :) = X_batchsize_new.L;
    X.R(idxcolsshort, :) = X_batchsize_new.R;
    
    % Update LtL and RtR, the global curvature information
    LtL = LtL - X_batchsize.L'*X_batchsize.L ...
        + X_batchsize_new.L'*X_batchsize_new.L;
    RtR = RtR - X_batchsize.R'*X_batchsize.R ...
        + X_batchsize_new.R'*X_batchsize_new.R;
    
    
    
    % The timing for the code below is not stored
    
    % Global cost computation
    preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
    errors_ls = preds_ls - data_ls.entries;
    cost_ls = ((errors_ls'*errors_ls) + lambda*(LtL(:)'*RtR(:)))/data_ls.nentries;
    infos.cost = [infos.cost; cost_ls];
    infos.relresidual = [infos.relresidual; sqrt(cost_ls/meansqentries_ls)];
    
    
    % Global gradient computation, for comparisons
    residual = sparse(data_ls.rows,data_ls.cols,errors_ls,n,m);
    sqgradnorm = norm(residual*X.R + lambda*(X.L*RtR), 'fro')^2 ...
        +norm(residual'*X.L + lambda*(X.R*LtL), 'fro')^2;
    gradnorm = 2*sqrt(sqgradnorm)/M;
    infos.gradnorm = [infos.gradnorm; gradnorm];
    
    
    fprintf('Iteration %0.3d, Cost %7.3e, Gradnorm %7.3e, Stepsize %7.3e, Count %d, mu %2.2f\n', outiter, cost_ls, gradnorm, full(stepsize), count, mu);
    
    % If interested in computing recovery
    if ~isempty(data_ts)
        preds_ts = sum(X.L(data_ts.rows,:).*X.R(data_ts.cols,:), 2);
        errors_ts = preds_ts - data_ts.entries;
        cost_ts = (errors_ts'*errors_ts)/data_ts.nentries;
        infos.cost_test = [infos.cost_test; cost_ts];
        infos.relresidual_test = [infos.relresidual_test; sqrt(cost_ts/meansqentries_ts)];
        infos.nmae_jester = [ infos.nmae_jester ; sum(abs(errors_ts))/(20*data_ts.nentries)];
    end
    
    infos.time = [infos.time; infos.time(end) + toc(t_begin)];
    infos.numpasses = [infos.numpasses; outiter];
    
    
    
    % Stopping criteria: mean square error
    if cost_ls < msetol
        fprintf('MSE cost sufficiently decreased.\n');
        break;
    end
    
    % Stopping criteria: relative residual
    if sqrt(cost_ls/meansqentries_ls) < relresidualtol;
        fprintf('Relative residual sufficiently decreased.\n');
        break;
    end
    
    %Bold driver protocol
    if infos.cost(end) > infos.cost(end-1) || isnan(infos.cost(end))
        stepsize = stepsize/2;
    else
        stepsize = 1.1*stepsize;
    end
    

    
   
    
    
end

infos.rmse = sqrt(infos.cost);

end


% Initial step-size guess
function stepsize =  stepsize_guess(X_batchsize, scaledgrad_X_batchsize, residual_batchsize, indicator_batchsize)
% Stepsize guess
T = (indicator_batchsize.*(X_batchsize.L*scaledgrad_X_batchsize.R' ...
    + scaledgrad_X_batchsize.L*X_batchsize.R'));

% Two degree polynomial
a2 = T(:)'*T(:); %norm(T,'fro')^2;
a1 = -2*(residual_batchsize(:)'*T(:)); %trace(residual_batchsize'* T);
stepsize = -0.5*a1/a2; % original: -0.5*a1/a2;
end

