function [ashort, indashortfroma, indafromashort, tmax] =  myunique(a, n)
% Similar to the function "unique" with the 'stable' option, but faster.
% on the tested benchmarks.
% Type "help unique".
% tmax is the number of unique array elements.
%
% Input: a: an array of numbers.
%        n: the length of the array.
%
% Bamdev Mishra and Rodolphe Sepulchre,
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.

% Original author: Bamdev Mishra, Aug. 27, 2015.
% Contributors:
% Change log:
    tmax = n;
    ashort = a;
    indafromashort = 1:n;
    indashortfroma = 1:n;
    
    if n > 1
        
        [asorted, inda2asorted] = sort(a, 'ascend'); % O(nlog(n))
        indasorted2a(inda2asorted) = 1:n; % O(n)
        
        if find(diff(asorted) == 0) % O(n)
            for jj = 1:n % O(n)
                if jj == 1,
                    ashort(1) = a(1);
                    indafromashort(1) = 1;
                    tmax = 1;
                    indashortfroma(1) = 1;
                else
                    indasorted2ajj = indasorted2a(jj);
                    if indasorted2ajj - 1 > 0 && ...
                            asorted(indasorted2ajj) == asorted(indasorted2ajj - 1)
                        t = indafromashort(inda2asorted(indasorted2ajj - 1));
                        indafromashort(jj) = t;
                    else
                        tmax = tmax + 1;
                        indafromashort(jj) = tmax;
                        ashort(tmax) = a(jj);
                        indashortfroma(tmax) = jj;
                    end
                end
            end
            ashort = ashort(1:tmax);
            indashortfroma = indashortfroma(1:tmax);
        end
    end
end
