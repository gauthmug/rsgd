function [X, infos] = scaledSGDOne(Xinit, n, m, r, data_ls, data_ts, options)
% Scaled stochastic gradient descent algorithm for matrix completion with batchsize equal to one.
%
% [X, infos] = scaledSGDOne(Xinit, n, m, r, data_ls, data_ts, options)
%
%
% Parameters
% -----------
% Xinit: Stuctured array with low-rank factors, Xinit.L and Xinit.R
% n: number of rows
% m: number of colummns
% r: rank
%
% data_ls: data_ls.rows: rows of the given entries
%        : data_ls.cols: columns of the given entries
%        : data_ls.entries: values of the given entries
%        : data_ls.nentries: number of given entries
%
% data_ts: data_ts.rows: rows of the test entries
%        : data_ts.cols: columns of the test entries
%        : data_ts.entries: values of the test entries
%        : data_ts.nentries: number of test entries
%
% options.maxiterations: maximum iterations
% options.msetol: tolerance on the mean square error
% options.relresidualtol: tolerance on the relative residual error
% options.mu: the parameter that combines local and global Hessian information
% options.lambda: regularization parameter
% options.stepsize: initial step-size. Default is use a guess described below.
% options.shuffle: string that indicates how the data points are read through
%
% Output:
% -------
% X: Structured array with final low-rank factors, X.L and X.R
% infos: Structured array with additional information
%
%
% Bamdev Mishra and Rodolphe Sepulchre,
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with slight modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
% inspired from
% Estelle Massart: https://sites.uclouvain.be/absil/2016-12/article_inductive_v2_techrep.pdf
% in order to optimize the code for batchsize equal to one

d = max(m,n);
% Set default parameters
if isempty(Xinit)
    Xinit.L = randn(n, r);
    Xinit.R = randn(m, r);
end
X = Xinit;

if ~isfield(options, 'shuffle') || isempty(options.shuffle)
    options.shuffle = 'no';
end
if ~isfield(options,'maxiterations'); options.maxiterations = 100; end % Max iterations
if ~isfield(options,'msetol'); options.msetol = 1e-8; end % Tolerance on the mean square error
if ~isfield(options,'relresidualtol'); options.relresidualtol = 1e-4; end % Tolerance on the relative residual error
if ~isfield(options,'mu'); options.mu = 0.5; end % Mu parameter
if ~isfield(options,'lambda'); options.lambda = 0; end % Regularization parameter


% Options
maxiterations = options.maxiterations;
msetol = options.msetol;
relresidualtol = options.relresidualtol;
mu = options.mu;
lambda = options.lambda;
shuffle = options.shuffle;


meansqentries_ls = (data_ls.entries'*data_ls.entries)/data_ls.nentries;
M = data_ls.nentries; % Number of known entries.
LtL = X.L'*X.L; % Global curvature information
RtR = X.R'*X.R;


% Global cost computation
preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
errors_ls = preds_ls - data_ls.entries;
cost_ls = (errors_ls'*errors_ls)/data_ls.nentries;
%cost_ls = ((errors_ls'*errors_ls) + lambda*(LtL(:)'*RtR(:)))/data_ls.nentries;

% Global gradient computation, for comparisons
residual = sparse(data_ls.rows,data_ls.cols,errors_ls,n,m);
sqgradnorm = norm(residual*X.R, 'fro')^2 ...
    +norm(residual'*X.L, 'fro')^2;
gradnorm = 2*sqrt(sqgradnorm)/M;


fprintf('Iteration %0.3d, Cost %7.3e, Gradnorm %7.3e\n', 0, cost_ls, gradnorm);

% If interested in computing recovery
if ~isempty(data_ts)
    meansqentries_ts = (data_ts.entries'*data_ts.entries)/data_ts.nentries;
    
    preds_ts = sum(X.L(data_ts.rows,:).*X.R(data_ts.cols,:), 2);
    errors_ts = preds_ts - data_ts.entries;
    cost_ts = (errors_ts'*errors_ts)/data_ts.nentries;
    infos.cost_test = cost_ts;
    infos.relresidual_test = sqrt(cost_ts/meansqentries_ts);
    
    infos.nmae_jester = sum(abs(errors_ts))/(20*data_ts.nentries);
    infos.nmae_movielens = sum(abs(errors_ts))/(4.5*data_ts.nentries);
    
end

% Initialization
infos.cost = cost_ls;
infos.time = 0;
infos.gradnorm = gradnorm;
infos.relresidual = sqrt(cost_ls/meansqentries_ls);
infos.numpasses = 0;



if ~isfield(options, 'stepsize') || isempty(options.stepsize)
    
    if shuffle
        idxentries = randperm(M, 1);
    else
        idxentries = 1;
    end
    
    idxrows = data_ls.rows(idxentries);
    idxcols = data_ls.cols(idxentries);
    entries_batchsize = data_ls.entries(idxentries);
    omega_batchsize = entries_batchsize;
    
    % Batch size
    X_batchsize.L = X.L(idxrows, :);
    X_batchsize.R = X.R(idxcols, :);
    
    
    % Local + Global
    scaleLtL = (mu/d)*LtL + (1 - mu)*(X_batchsize.L'*X_batchsize.L);
    scaleRtR = (mu/d)*RtR + (1 - mu)*(X_batchsize.R'*X_batchsize.R);
    
    
    % Current residual and gradient computations
    residual_batchsize = X_batchsize.L*X_batchsize.R' - omega_batchsize;
    
    
    % Batch size: gradient computation
    grad_X_batchsize.L = residual_batchsize*X_batchsize.R;
    grad_X_batchsize.R = residual_batchsize*X_batchsize.L;
    
    % Scaled gradient computation
    scaledgrad_X_batchsize.L = grad_X_batchsize.L/scaleRtR;
    scaledgrad_X_batchsize.R = grad_X_batchsize.R/scaleLtL;
    
    % Stepsize
    options.stepsize = 0.5*stepsize_guess(X_batchsize, scaledgrad_X_batchsize, residual_batchsize, 1);
    
end
stepsize = options.stepsize;

% Main code
for outiter = 1 : maxiterations

    % Begin the time counter for the epoch
    t_begin = tic();
    
    switch options.shuffle
        case 'no'
            entries_order = 1:M;
        case 'random'
            entries_order = randperm(M);
        case 'random_replacement'
            entries_order = randi(M,1,M);
        case 'shuffling'
            if (outiter==1)
                entries_order = 1:M;
            elseif (mod(outiter,2)==1)
                entries_order = entries_order(end:-1:1);
                entries_order = riffle_shuffle(entries_order,options.flag);
            else
                entries_order = entries_order(end:-1:1);
            end
        case 'weighted'
            %update weights after every five epochs
            if (outiter==1)
                switch options.weightCost
                    case 'expensive'
                        w = importanceExpensive(X, n, m, r, data_ls, options);
                    case 'cheap'
                        w = importanceCheap(X, n, m, r, data_ls, options);
                end
                options.w = w;
            end
            
            if mod(outiter, 5) == 0
                switch options.weightCost
                    case 'expensive'
                        w = importanceExpensive(X, n, m, r, data_ls, options);
                    case 'cheap'
                        w = importanceCheap(X, n, m, r, data_ls, options);
                end
                options.w = w;
            end
            
            entries_order = randp(options.w,M,1);
            entries_order = entries_order(randperm(M));
    end
    
    
    count = 0;
    
    
    for ii = 1 : M
        
        % Update the counts
        count = count + 1;
        
        % Pick a batchsize of rows, cols, and entries
        idxentries = entries_order(ii);
        idxrows = data_ls.rows(idxentries);
        idxcols = data_ls.cols(idxentries);
        entries_batchsize = data_ls.entries(idxentries);
        omega_batchsize = entries_batchsize;
        
        
        % Batch size
        X_batchsize.L = X.L(idxrows, :);
        X_batchsize.R = X.R(idxcols, :);
        
        
        % Local + Global
        scaleLtL = (mu/d)*LtL + (1 - mu)*(X_batchsize.L'*X_batchsize.L); % max(d1, d2) = d;
        scaleRtR = (mu/d)*RtR + (1 - mu)*(X_batchsize.R'*X_batchsize.R);
        
        % Current residual and gradient computations
        residual_batchsize = X_batchsize.L*X_batchsize.R' - omega_batchsize;
        
        
        % Batch size: gradient computation
        grad_X_batchsize.L = residual_batchsize*X_batchsize.R; % + lambda*(X_batchsize.L*RtR);
        grad_X_batchsize.R = residual_batchsize*X_batchsize.L; % + lambda*(X_batchsize.R*LtL);
        
        % Scaled gradient computation
        scaledgrad_X_batchsize.L = grad_X_batchsize.L/scaleRtR;
        scaledgrad_X_batchsize.R = grad_X_batchsize.R/scaleLtL;
        
        
        % Batch size updates
        stepsizescaledgrad_X_batchsizeL = stepsize*scaledgrad_X_batchsize.L;
        stepsizescaledgrad_X_batchsizeR = stepsize*scaledgrad_X_batchsize.R;
        
        X_batchsize_new.L = X_batchsize.L - stepsizescaledgrad_X_batchsizeL;
        X_batchsize_new.R = X_batchsize.R - stepsizescaledgrad_X_batchsizeR;
        
        % Update the relevant rows of L and R.
        X.L(idxrows, :) = X_batchsize_new.L;
        X.R(idxcols, :) = X_batchsize_new.R;
        
        
        % Update LtL and RtR, the global curvature information
        LtL = LtL - X_batchsize.L'*X_batchsize.L ...
            + X_batchsize_new.L'*X_batchsize_new.L;
        RtR = RtR - X_batchsize.R'*X_batchsize.R ...
            + X_batchsize_new.R'*X_batchsize_new.R;
        
    end
    
    
    
    %if (mod(outiter, 5) == 0)
    
    
    % The two lines are not required, but might be better to do them
    % once in a whlie.
    LtL = X.L'*X.L;
    RtR = X.R'*X.R;
    
    infos.time = [infos.time; infos.time(end) + toc(t_begin)];
    infos.numpasses = [infos.numpasses; outiter];
    
    % The timing for the code below is not stored
    
    % Global cost computation
    preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
    errors_ls = preds_ls - data_ls.entries;
    cost_ls = (errors_ls'*errors_ls)/data_ls.nentries;
    infos.cost = [infos.cost; cost_ls];
    infos.relresidual = [infos.relresidual; sqrt(cost_ls/meansqentries_ls)];
    
    
    % Global gradient computation, for comparisons
    residual = sparse(data_ls.rows,data_ls.cols,errors_ls,n,m);
    sqgradnorm = norm(residual*X.R, 'fro')^2 ...
        +norm(residual'*X.L, 'fro')^2;
    gradnorm = 2*sqrt(sqgradnorm)/M;
    infos.gradnorm = [infos.gradnorm; gradnorm];
    
    
    fprintf('Iteration %0.3d, Cost %7.3e, Gradnorm %7.3e, Stepsize %7.3e, Count %d, mu %2.2f\n', outiter, cost_ls, gradnorm, full(stepsize), count, mu);
    
    % If interested in computing recovery
    if ~isempty(data_ts)
        preds_ts = sum(X.L(data_ts.rows,:).*X.R(data_ts.cols,:), 2);
        errors_ts = preds_ts - data_ts.entries;
        cost_ts = (errors_ts'*errors_ts)/data_ts.nentries;
        infos.cost_test = [infos.cost_test; cost_ts];
        infos.relresidual_test = [infos.relresidual_test; sqrt(cost_ts/meansqentries_ts)];
        infos.nmae_jester = [ infos.nmae_jester ; sum(abs(errors_ts))/(20*data_ts.nentries)];
        infos.nmae_movielens = [infos.nmae_movielens ; sum(abs(errors_ts))/(4.5*data_ts.nentries)];
    end
    
    
    % Stopping criteria: mean square error
    if cost_ls < msetol
        fprintf('MSE cost sufficiently decreased.\n');
        break;
    end
    
    % Stopping criteria: relative residual
    if sqrt(cost_ls/meansqentries_ls) < relresidualtol;
        fprintf('Relative residual sufficiently decreased.\n');
        break;
    end
    
    
    
    % Stepsize
    switch options.stepSelect
        case 'constant'
            stepsize = options.stepsize;
        case 'geometric'
            rho = 0.95;
            stepsize = stepsize*rho;
        case 'counter'
            c1 = 1;
            c2 = 1;
            stepsize = c1/(c2 + outiter);
        case 'bold'
            % Bold driver protocol
            if infos.cost(end) > infos.cost(end-1) || isnan(infos.cost(end))
                sigma = 0.5;
                stepsize = sigma*stepsize;
            else
                rho = 1.1;
                stepsize = rho*stepsize;
            end
        case 'exponential'
            % Exponential decay
            stepsize0 = 1e-1;
            expConst = 1e-1;
            stepsize = stepsize0*exp(-expConst*outiter);   
        case 'AdaGrad'
    end
    
    
    
end

infos.rmse = sqrt(infos.cost);
infos.rmse_test = sqrt(infos.cost_test);


end


% Initial step-size guess
function stepsize = stepsize_guess(X_batchsize, scaledgrad_X_batchsize, residual_batchsize)
% Stepsize guess
T = X_batchsize.L*scaledgrad_X_batchsize.R' + scaledgrad_X_batchsize.L*X_batchsize.R';

% Two degree polynomial
a2 = T(:)'*T(:); %norm(T,'fro')^2;
a1 = -2*(residual_batchsize(:)'*T(:)); %trace(residual_batchsize'* T);
stepsize = -0.5*a1/a2; % original: -0.5*a1/a2;
end

