function weights = importanceCheap(X, n, m, r, data_ls, options)
% Defining a cheap weight vector for the scaled SGD method with batchsize
% equal to one
%
% weights = importanceCheap(X, n, m, r, data_ls, options)
%
%
% Parameters
% -----------
% Xinit: Stuctured array with low-rank factors, Xinit.L and Xinit.R
% n: number of rows
% m: number of colummns
% r: rank
%
% data_ls: data_ls.rows: rows of the given entries
%        : data_ls.cols: columns of the given entries
%        : data_ls.entries: values of the given entries
%        : data_ls.nentries: number of given entries
%
% data_ts: data_ts.rows: rows of the test entries
%        : data_ts.cols: columns of the test entries
%        : data_ts.entries: values of the test entries
%        : data_ts.nentries: number of test entries
%
% options.weightsprop: string that indicates if we take the weights equal
%           to one, or proportional to delta
% opions.beta: number of non-zero weights
%
% Output:
% -------
% weights: vector of size data_ls.nentries with a probability
%           assigned to every data point
%
%
%
% This implementation is due to
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018




% Options
beta = options.beta;
weightsprop = options.weightsprop;



M = data_ls.nentries;
% Number of known entries.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('Determine the weights. \n');
weights = zeros(M,1);
delta = zeros(M,1);

for ii = 1 : M
 
    % Pick a batchsize of rows, cols, and entries
    idxentries = ii;
    idxrows = data_ls.rows(idxentries);
    idxcols = data_ls.cols(idxentries);
    entries_batchsize = data_ls.entries(idxentries);
    omega_batchsize = entries_batchsize;
    
    % Batch size
    X_batchsize.L = X.L(idxrows, :);
    X_batchsize.R = X.R(idxcols, :);
    
    % Current residual
    residual_batchsize = X_batchsize.L*X_batchsize.R' - omega_batchsize;
    delta(ii) = abs(residual_batchsize);
    
end

[~, idx] = sort(delta,'descend');
newIdx = idx(1:beta);

switch weightsprop
    case 'One'
        weights(newIdx) = 1;
    case 'Delta'
        weights(newIdx) = (delta(newIdx)).^(1/4);
end



% Normalise weights
weights = weights./sum(weights);

%nnz(weights)/M

end


