function [X, infos] = als(Xinit, n, m, r, data_ls, data_ts, options)
% Alternating least-squares algorithm for matrix completion.
%
%[X, infos] = als(Xinit, n, m, r, data_ls, data_ts, options)
%
% Parementers
% -----------
% Xinit: Stuctured array with low-rank factors, Xinit.L and Xinit.R
% n: number of rows
% m: number of colummns
% r: rank
%
% data_ls: data_ls.rows: rows of the given entries
%        : data_ls.cols: columns of the given entries
%        : data_ls.entries: values of the given entries
%        : data_ls.nentries: number of given entries
%
% data_ts: data_ts.rows: rows of the test entries
%        : data_ts.cols: columns of the test entries
%        : data_ts.entries: values of the test entries
%        : data_ts.nentries: number of test entries
%
% options.maxiterations: maximum iterations
% options.msetol: tolerance on the mean square error
% options.relresidualtol: tolerance on the relative residual error
% options.lambda: regularization parameter
% options.shuffle: if true, pick the rows (and columns) randomly 
%                  without replacement
%
%
% Output:
% -------
% X: Structured array with final low-rank factors, X.L and X.R
% infos: Structured array with additional information
%
%
% Paper:
% ------
% Zhou Y, Wilkinson D, Schreiber R, Pan R (2008) 
% "Large-scale parallel collaborative filtering for the Netflix prize". 
% In: International Conference on Algorithmic Aspects in Information 
% and Management (AAIM), pp 337–348.


% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 2016.
    
    
    if isempty(Xinit)
        Xinit.L = randn(n, r);
        Xinit.R = randn(m, r);
    end
    X = Xinit;
    
    if ~isfield(options, 'shuffle') || isempty(options.shuffle)
        options.shuffle = true;
    end
    
    if ~isfield(options,'maxiterations'); options.maxiterations = 100; end % Max iterations
    if ~isfield(options,'mestol'); options.msetol = 1e-8; end % Tolerance on the mean square error
    if ~isfield(options,'relresidualtol'); options.relresidualtol = 1e-4; end % Tolerance on the relative residual error
    if ~isfield(options,'lambda'); options.lambda = 0; end % Regularization parameter
    
    
    % Options
    maxiterations = options.maxiterations;
    msetol = options.msetol;
    relresidualtol = options.relresidualtol;
    lambda = options.lambda;
    shuffle = options.shuffle;
    
    meansqentries_ls = (data_ls.entries'*data_ls.entries)/data_ls.nentries;
    M = data_ls.nentries; % Number of known entries.
    
    
    % Form some sparse matrices for easier matlab indexing
    values = sparse(data_ls.rows,data_ls.cols,data_ls.entries,n,m);
    indicator = sparse(data_ls.rows,data_ls.cols,1,n,m);
    
    
    
    % Global cost computation
    preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
    errors_ls = preds_ls - data_ls.entries;
    cost_ls = (errors_ls'*errors_ls)/data_ls.nentries;
    
    % Global (Euclidean) gradient computation, for comparisons
    residual = sparse(data_ls.rows,data_ls.cols,errors_ls,n,m);
    sqgradnorm = norm(residual*X.R, 'fro')^2 ...
        +norm(residual'*X.L, 'fro')^2;
    gradnorm = 2*sqrt(sqgradnorm)/M;
    
    fprintf('Iteration %0.3d, Cost %7.3e, Gradnorm %7.3e\n', 0, cost_ls, gradnorm);
    
    % If interested in computing recovery
    if ~isempty(data_ts)
        meansqentries_ts = (data_ts.entries'*data_ts.entries)/data_ts.nentries;
        
        preds_ts = sum(X.L(data_ts.rows,:).*X.R(data_ts.cols,:), 2);
        errors_ts = preds_ts - data_ts.entries;
        cost_ts = (errors_ts'*errors_ts)/data_ts.nentries;
        infos.cost_test = cost_ts;
        infos.relresidual_test = sqrt(cost_ts/meansqentries_ts);
        infos.nmae_jester = sum(abs(errors_ts))/(20*data_ts.nentries);
        
    end
    
    
    
    % Initialization
    infos.cost = cost_ls;
    infos.time = 0;
    infos.gradnorm = gradnorm;
    infos.relresidual = sqrt(cost_ls/meansqentries_ls);
    infos.numpasses = 0;
    
    
    % Creat the cells for the cols
    colsindicator_cell = cell(m,1);
    colsvalues_cell = cell(m,1);
    
    for k = 1 : m,
        % Pull out the relevant indices and revealed entries for this column
        idx = find(indicator(:, k)); % find known row indices
        values_col = values(idx, k); % the non-zero entries of the column
        
        colsindicator_cell{k} = idx;
        colsvalues_cell{k} = values_col;
    end
    
    % Creat the cells for the rows
    rowsindicator_cell = cell(n,1);
    rowsvalues_cell = cell(n,1);
    
    for k = 1 : n,
        % Pull out the relevant indices and revealed entries for this row
        idx = find(indicator(k, :)); % find known row indices
        values_col = values(k, idx); % the non-zero entries of the row
        
        rowsindicator_cell{k} = idx;
        rowsvalues_cell{k} = values_col;
    end
    
    
    
    % Main code
    eyerank = eye(r);
    
    for outiter = 1 : maxiterations,
        
        % Begin the time counter for the epoch
        t_begin = tic();
        
        
        if shuffle
            row_order = randperm(n);
        else
            row_order = 1:n;
        end
        
        
        
        for ii = 1 : n % Sweep over the rows
            rownum = row_order(ii);
            
            % Pull out the relevant indices and revealed entries for this
            % row
            idx = rowsindicator_cell{rownum}; % find known row indices
            rowvalues = rowsvalues_cell{rownum}; % the non-zero entries of the column
            
            Rshort = X.R(idx, :);
            X.L(rownum, :) = (Rshort'*Rshort + lambda*eyerank)\(Rshort'*rowvalues');
            
        end
        
        if shuffle,
            col_order = randperm(m);
        else
            col_order = 1:m;
        end
        
        
        for jj = 1:m % Sweep over the cols
            colnum = col_order(jj);
            
            % Pull out the relevant indices and revealed entries for this
            % column
            idx = colsindicator_cell{colnum}; % find known row indices
            colvalues = colsvalues_cell{colnum}; % the non-zero entries of the column
            
            Lshort = X.L(idx, :);
            
            X.R(colnum, :) = (Lshort'*Lshort + lambda*eyerank)\(Lshort'*colvalues);
            
        end
        
        
        infos.time = [infos.time; infos.time(end) + toc(t_begin)];
        infos.numpasses = [infos.numpasses; infos.numpasses(end) + 1];
        
        % The timing for the code below is not stored
        
        % Global cost computation
        preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
        errors_ls = preds_ls - data_ls.entries;
        cost_ls = (errors_ls'*errors_ls)/data_ls.nentries;
        infos.cost = [infos.cost; cost_ls];
        infos.relresidual = [infos.relresidual; sqrt(cost_ls/meansqentries_ls)];
        
        
        % Global (Euclidean) gradient computation, for comparisons
        residual = sparse(data_ls.rows,data_ls.cols,errors_ls,n,m);
        sqgradnorm = norm(residual*X.R, 'fro')^2 ...
            +norm(residual'*X.L, 'fro')^2;
        gradnorm = 2*sqrt(sqgradnorm)/M;
        infos.gradnorm = [infos.gradnorm; gradnorm];
        
        
        fprintf('Iteration %0.3d, Cost %7.3e, Gradnorm %7.3e\n', outiter, cost_ls, gradnorm);
        
        % If interested in computing recovery
        if ~isempty(data_ts)
            preds_ts = sum(X.L(data_ts.rows,:).*X.R(data_ts.cols,:), 2);
            errors_ts = preds_ts - data_ts.entries;
            cost_ts = (errors_ts'*errors_ts)/data_ts.nentries;
            infos.cost_test = [infos.cost_test; cost_ts];
            infos.relresidual_test = [infos.relresidual_test; sqrt(cost_ts/meansqentries_ts)];
            infos.nmae_jester = [ infos.nmae_jester ; sum(abs(errors_ts))/(20*data_ts.nentries)];
            
        end
        
        
        % Stopping criteria: mean square error
        if cost_ls < msetol
            fprintf('MSE cost sufficiently decreased.\n');
            break;
        end
        
        % Stopping criteria: relative residual
        if sqrt(cost_ls/meansqentries_ls) < relresidualtol;
            fprintf('Relative residual sufficiently decreased.\n');
            break;
        end
        
        
    end
    
end



