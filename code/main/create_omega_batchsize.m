function [omega, indicator] = create_omega_batchsize(entries, lengthrowsshort, lengthcolsshort, indrowsshort, indcolsshort, lengthentries)
% Create a submatrix corresponding to the batch size. 
%
% Bamdev Mishra and Rodolphe Sepulchre,
% "Scaled stochastic gradient descent for low-rank matrix completion",
%Technical report, arXiv:1603.04989, 2016.

% Original author: Bamdev Mishra, Aug. 27, 2015.
% Contributors:
% Change log:    

omega = zeros(lengthrowsshort, lengthcolsshort);
    indicator = zeros(lengthrowsshort, lengthcolsshort);
    for jj = 1 : lengthentries
        omega(indrowsshort(jj), indcolsshort(jj)) = entries(jj);
        indicator(indrowsshort(jj), indcolsshort(jj)) = 1;
    end
end

