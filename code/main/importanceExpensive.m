function weights = importanceExpensive(Xinit, n, m, r, data_ls, options)
% Defining an expensive weight vector for the scaled SGD method with batchsize
% equal to one
%
% weights = importanceExpensive(Xinit, n, m, r, data_ls, options)
%
%
% Parameters
% -----------
% Xinit: Stuctured array with low-rank factors, Xinit.L and Xinit.R
% n: number of rows
% m: number of colummns
% r: rank
%
% data_ls: data_ls.rows: rows of the given entries
%        : data_ls.cols: columns of the given entries
%        : data_ls.entries: values of the given entries
%        : data_ls.nentries: number of given entries
%
% data_ts: data_ts.rows: rows of the test entries
%        : data_ts.cols: columns of the test entries
%        : data_ts.entries: values of the test entries
%        : data_ts.nentries: number of test entries
%
% options.maxiterations: maximum iterations
% options.msetol: tolerance on the mean square error
% options.relresidualtol: tolerance on the relative residual error
% options.mu: the parameter that combines local and global Hessian information
% options.lambda: regularization parameter
% options.stepsize: initial step-size. Default is use a guess described below.
% options.shuffle: string that indicates how the data points are read through
%
% Output:
% -------
% weights: vector of size data_ls.nentries with a probability
%           assigned to every data point
%
%
% Bamdev Mishra and Rodolphe Sepulchre,
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with slight modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018


d = max(m,n);
% Set default parameters
if isempty(Xinit)
    Xinit.L = randn(n, r);
    Xinit.R = randn(m, r);
end
X = Xinit;

if ~isfield(options,'mu'); options.mu = 0.5; end % Mu parameter


% Options
mu = options.mu;
stepsize = options.stepsize;



meansqentries_ls = (data_ls.entries'*data_ls.entries)/data_ls.nentries;
M = data_ls.nentries; % Number of known entries.
LtL = X.L'*X.L; % Global curvature information
RtR = X.R'*X.R;


% Global cost computation
preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
errors_ls = preds_ls - data_ls.entries;
cost_ls = (errors_ls'*errors_ls)/data_ls.nentries;
%cost_ls = ((errors_ls'*errors_ls) + lambda*(LtL(:)'*RtR(:)))/data_ls.nentries;

% Global gradient computation, for comparisons
residual = sparse(data_ls.rows,data_ls.cols,errors_ls,n,m);
sqgradnorm = norm(residual*X.R, 'fro')^2 ...
    +norm(residual'*X.L, 'fro')^2;
gradnorm = 2*sqrt(sqgradnorm)/M;


%fprintf('Iteration %0.3d, Cost %7.3e, Gradnorm %7.3e\n', 0, cost_ls, gradnorm);

% If interested in computing recovery


% Initialization
infos.cost = cost_ls;
infos.time = 0;
infos.gradnorm = gradnorm;
infos.relresidual = sqrt(cost_ls/meansqentries_ls);
infos.numpasses = 0;



original_cost = cost_ls;

weights = zeros(M,1);


for ii = 1 : M
    
    % Pick a batchsize of rows, cols, and entries
    idxentries = ii;
    idxrows = data_ls.rows(idxentries);
    idxcols = data_ls.cols(idxentries);
    entries_batchsize = data_ls.entries(idxentries);
    omega_batchsize = entries_batchsize;
    
    % Batch size
    X_batchsize.L = X.L(idxrows, :);
    X_batchsize.R = X.R(idxcols, :);
    
    % Local + Global
    scaleLtL = (mu/d)*LtL + (1 - mu)*(X_batchsize.L'*X_batchsize.L); % max(d1, d2) = d;
    scaleRtR = (mu/d)*RtR + (1 - mu)*(X_batchsize.R'*X_batchsize.R);
    
    % Current residual and gradient computations
    residual_batchsize = X_batchsize.L*X_batchsize.R' - omega_batchsize;
    
    % Batch size: gradient computation
    grad_X_batchsize.L = residual_batchsize*X_batchsize.R; % + lambda*(X_batchsize.L*RtR);
    grad_X_batchsize.R = residual_batchsize*X_batchsize.L; % + lambda*(X_batchsize.R*LtL);
    
    % Scaled gradient computation
    scaledgrad_X_batchsize.L = grad_X_batchsize.L/scaleRtR;
    scaledgrad_X_batchsize.R = grad_X_batchsize.R/scaleLtL;
    
    % Batch size updates
    stepsizescaledgrad_X_batchsizeL = stepsize*scaledgrad_X_batchsize.L;
    stepsizescaledgrad_X_batchsizeR = stepsize*scaledgrad_X_batchsize.R;
    
    X_batchsize_new.L = X_batchsize.L - stepsizescaledgrad_X_batchsizeL;
    X_batchsize_new.R = X_batchsize.R - stepsizescaledgrad_X_batchsizeR;
    
    % Update the relevant rows of L and R.
    oldL = X.L(idxrows, :);
    oldR = X.R(idxcols, :);
    
    X.L(idxrows, :) = X_batchsize_new.L;
    X.R(idxcols, :) = X_batchsize_new.R;
    
    
    % Global cost computation
    preds_ls = sum(X.L(data_ls.rows,:).*X.R(data_ls.cols,:), 2);
    errors_ls = preds_ls - data_ls.entries;
    cost_ls = (errors_ls'*errors_ls)/data_ls.nentries;
    %infos.cost = [infos.cost; cost_ls];
    %infos.relresidual = [infos.relresidual; sqrt(cost_ls/meansqentries_ls)];
    
    delta = cost_ls - original_cost;
    %cost_compare = cost_ls;
    % we hope that cost_ls < original_cost, i.e., delta < 0
    if delta > 1e-2
        weights(ii) = 0;
    else
        weights(ii) = abs(delta)^(1/4);
        %weights(ii) = 1;
    end
    
    %weights(ii);
    
    % Restore previous values
    X.L(idxrows, :) = oldL;
    X.R(idxcols, :) = oldR;
   
    
    
end

% Normalise weights
weights = weights./sum(weights);

end


