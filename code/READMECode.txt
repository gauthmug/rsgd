Master's thesis

Title: "Stochastic Gradient Methods for Matrix Completion"

Author: Gauthier Muguerza <gauthier.muguerza@gmail.com>

Supervisors: Pierre-Antoine ABSIL, Rodolphe SEPULCHRE

Readers: Estelle MASSART, Shuyu DONG

This package contains a MATLAB implementation of the algorithms presented in the report.
It also containts scripts to reproduce all the Figures and the Tables of the report.

Gauthier Muguerza, Pierre-Antoine Absil, Estelle Massart
"Stochastic Gradient Methods for Matrix Completion",
Master thesis, UCLouvain, 2018
Report: http://bit.ly/SGD2018


The implementation is a research prototype still in development and is provided AS IS. 
No warranties or guarantees of any kind are given.
Do not distribute this code or use it other than for your own research without permission of the authors.

Feedback is greatly appreciated.


Installation:
-------------

- Set current directory as your current folder in Matlab or put it in your Matlab path.
- Run "run_me_first.m" to add folders to the working path. This needs to be done at the starting of each session.
- To check that everything works, run "test_thesis.m" at Matlab command prompt
  (you should see two plots at the end).



Folders:
------
- data: all the data sets needed for the tests and the applications
- main: all the functions needed to run the scripts
- scriptFigures: scripts to reproduce all the Figures of the thesis, except Figure E.1
    (for an explanation of why Figure E.1 cannot be recreated, see script "figE1.m")
- scriptTables: scripts to reproduce all the Tables of the thesis



Disclaimer:
-----------

- The Jester dataset is downloaded from http://goldberg.berkeley.edu/jester-data/.
- The traffic dataset was sent by Shuyu Dong <shuyu.dong@uclouvain.be>
- the MovieLens 20M dataset is downloaded from https://grouplens.org/datasets/movielens/
- The Alzheimer's Disease Neuroimaging Initiative (ADNI) database is downloaded from adni.loni.usc.edu.
  The following links give some information about the Data Use Agreement:
    # http://adni.loni.usc.edu/wp-content/uploads/how_to_apply/ADNI_DSP_Policy.pdf
    # http://adni.loni.usc.edu/wp-content/uploads/how_to_apply/ADNI_Data_Use_Agreement.pdf
    # http://adni.loni.usc.edu/wp-content/uploads/how_to_apply/ADNI_Manuscript_Citations.pdf
    # http://adni.loni.usc.edu/wp-content/uploads/how_to_apply/ADNI_Acknowledgement_List.pdf
    # http://adni.loni.usc.edu/data-samples/access-data/

