clear; clc; close all;

%% Intro
% Running this code generates Table 5.1 of the thesis
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
% in the form of a TXT file called 'netflix.txt'
% containing the table in LaTeX format
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
%
%
% This code is inspired from P.-A. Absil, I. V. Oseledets
% in the context of the paper
% "Low-rank retractions: a survey and new results",
% Technical report, 2013
% https://sites.uclouvain.be/absil/2013.04
%
% This implementation is due to
% P.-A. Absil, I. V. Oseledets
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 10 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM


%% Controling the random number generator
SD = 0101918;
rng(SD);


filename = '/data/ml-20m/ratings.csv';
totalRows = 20000263;
totalRows2 = (totalRows - 1)/2;
numberRatings = 1e6; % <--- change this value


%% Ranks
ranks = [3 5 7];
sizeRanks = size(ranks,2);

nmea_mean_random_final = zeros(sizeRanks,1);
nmea_mean_shuffling_final = zeros(sizeRanks,1);
nmea_mean_weighted_final = zeros(sizeRanks,1);

count = 1;

for r = ranks
    
    maxiterations = 100;
    batchsize = 1;
    msetol = 1e-8;
    relresidualtol = 1e-8;
    mu = 0.5;
    stepsize = 1e-1;
    lambda = 1e-5;
    flag = 'in';
    random_initialization = false;
    stepSelect = 'bold';
    
    
    numberExp = 3;
    experim = 1:numberExp;
    
    train_rmse_random = zeros(maxiterations, numberExp);
    train_rmse_weighted = zeros(maxiterations, numberExp);
    train_rmse_shuffling = zeros(maxiterations, numberExp);
    
    test_rmse_random = zeros(maxiterations, numberExp);
    test_rmse_weighted = zeros(maxiterations, numberExp);
    test_rmse_shuffling = zeros(maxiterations, numberExp);
    
    test_nmae_random = zeros(maxiterations, numberExp);
    test_nmae_weighted = zeros(maxiterations, numberExp);
    test_nmae_shuffling = zeros(maxiterations, numberExp);
    
    
    
    for e = experim
        
        fprintf('Experiment %i out of %i \n', e, numberExp);
        
        nu = randi(totalRows2);
        nuNext = nu + numberRatings;
        
        %% Load MovieLens 20M data that has users as rows and movies as columns.
        data = csvread(filename,nu, 0,[nu 0 nuNext 2]);
        myDataMat = spconvert(data);
        
        data_real.cols = data(:,1); % users
        data_real.rows = data(:,2); % movies
        data_real.entries = data(:,3); % entries
        
        
        % Movies as rows. Replace movie ids by integers.
        [rows_sorted, IA, IC] = unique(data_real.rows);
        movie_ids = 1:length(rows_sorted);
        tmp = movie_ids(IC);
        data_real.rows = tmp;
        
        
        
        %% Remove the ones with entries zero
        rm_ind = find(data_real.entries == 0);
        data_real.entries(rm_ind) = [];
        data_real.rows(rm_ind) = [];
        data_real.cols(rm_ind) = [];
        
        
        
        
        %% permute them
        random_order = randperm(length(data_real.rows));
        data_real.rows = data_real.rows(random_order);
        data_real.cols = data_real.cols(random_order);
        data_real.entries = data_real.entries(random_order);
        data_real.nentries = length(data_real.entries);
        
        
        %% Train / test split
        prop_known = 0.8; % or 0.9 % Training fraction
        
        % Data training
        allidx = 1 : length(data_real.rows); % all indices
        
        M = round(prop_known*length(data_real.rows)); % Number of entries known
        idx = 1 : M; % indices for training
        Matcom.row = (data_real.rows(idx))';
        Matcom.col = (data_real.cols(idx))';
        Matcom.values_Omega = (data_real.entries(idx))';
        
        
        d1 = max(data_real.rows);
        d2 = max(data_real.cols);
        
        
        S = sparse(Matcom.row', Matcom.col', Matcom.values_Omega', d1, d2);
        [I_0, J_0, data_0] = find(S);
        Matcom.row = I_0';
        Matcom.col = J_0';
        Matcom.values_Omega = data_0';
        M = length(Matcom.row);
        
        
        data_ls.rows = Matcom.row';
        data_ls.cols = Matcom.col';
        data_ls.entries = Matcom.values_Omega';
        data_ls.nentries = length(data_ls.rows);
        
        
        % Test data
        M_test = length(data_real.rows) - M;
        L = length(data_real.rows);
        idx_test = allidx(L - M_test + 1: L);
        
        data_ts.rows = (data_real.rows(idx_test));
        data_ts.cols = (data_real.cols(idx_test));
        data_ts.entries = (data_real.entries(idx_test));
        data_ts.nentries = length(data_ts.entries);
        
        %% Initialization
        
        if ~random_initialization,
            sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
            [U, B, V] = svds(sparse_structure, r);
            Xinit.L = U*(B.^(0.5));
            Xinit.R = V*(B.^(0.5));
            fprintf('**** Initialization by taking %i dominant SVD\n', r);
        else
            Xinit.L = randn(d1, r);
            Xinit.R = randn(d2, r);
            fprintf('**** Random initialization\n');
        end
        
        
        %% Scaled-SGD
        options.maxiterations = maxiterations;
        options.batchsize = batchsize;
        options.msetol = msetol;
        options.relresidualtol = relresidualtol;
        options.mu = mu;
        options.stepsize = stepsize;
        options.lambda = lambda;
        options.flag = flag;
        options.stepSelect = stepSelect;
        options.beta = round(3*M/4);
        options.weightsprop = 'One';
        
        %% Random
        options.shuffle = 'random';
        fprintf('---------------------  Random shuffling ---------------------\n')
        [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
        
        %% Smart
        options.shuffle = 'shuffling';
        fprintf('---------------------  Smart shuffling ---------------------\n')
        [X_shuffling, infos_shuffling] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
        
        %% Weighted
        options.shuffle = 'weighted';
        options.weightCost = 'cheap';
        options.weightsprop = 'One';
        fprintf('---------------------  Weighted shuffling ---------------------\n')
        [X_weighted, infos_weighted] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
        
        %% Averages
        
        
        
        test_nmae_random(1:size(infos_random.nmae_movielens),e)       = infos_random.nmae_movielens;
        test_nmae_shuffling(1:size(infos_shuffling.nmae_movielens),e) = infos_shuffling.nmae_movielens;
        test_nmae_weighted(1:size(infos_weighted.nmae_movielens),e)   = infos_weighted.nmae_movielens;
        
        
        
    end
    
    %% Mean
    
    nmea_mean_random = mean(test_nmae_random, 2);
    nmea_mean_shuffling = mean(test_nmae_shuffling, 2);
    nmea_mean_weighted = mean(test_nmae_weighted, 2);
    
    
    %% Print NMEA
    nmea_mean_random_final(count,1) = nmea_mean_random(end);
    nmea_mean_shuffling_final(count,1) = nmea_mean_shuffling(end);
    nmea_mean_weighted_final(count,1) = nmea_mean_weighted(end);
    
    
    
    count = count + 1;
    
end

%% Construct the table

fid = fopen('netflix.txt','w');
%fprintf(fid,'%% This file was generated with %s on %s using m=%e and n=%e \n',mfilename('fullpath'),host_name,m,n);
fprintf(fid,'%s \n', '\begin{table}');
fprintf(fid,'%s \n', '\centering');
fprintf(fid,'%s \n', '\begin{tabular}{|l|c|c|c|}');
fprintf(fid,'%s \n', '\hline');
fprintf(fid,'%s \n', '                       & Random shuffling                        & Smart shuffling                         & Weighted shuffling  \\ \hline');
fprintf(fid,'$r = 3$  & %s & %s & %s %s \n', num2str(nmea_mean_random_final(1,1))      , num2str(nmea_mean_shuffling_final(1,1)) , num2str(nmea_mean_weighted_final(1,1))   , '   \\');
fprintf(fid,'$r = 5$  & %s & %s & %s %s \n', num2str(nmea_mean_random_final(2,1))      , num2str(nmea_mean_shuffling_final(2,1)) , num2str(nmea_mean_weighted_final(2,1))   ,'   \\');
fprintf(fid,'$r = 7$  & %s & %s & %s %s \n', num2str(nmea_mean_random_final(3,1))      , num2str(nmea_mean_shuffling_final(3,1)) , num2str(nmea_mean_weighted_final(3,1))   ,'   \\');
fprintf(fid,'%s \n', '\hline');
fprintf(fid,'%s \n', '\end{tabular}');
fprintf(fid,'\\caption{%s} \n', 'Table 5.1');
fprintf(fid,'%s \n', '\label{tab:MovieLens}');
fprintf(fid,'%s \n', '\end{table}');
fclose(fid);





