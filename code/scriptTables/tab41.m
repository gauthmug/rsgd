clear; clc; close all;

%% Intro
% Running this code generates Table 4.1 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
% in the form of a TXT file called 'weights.txt'
% containing the table in LaTeX format
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from P.-A. Absil, I. V. Oseledets
% in the context of the paper
% "Low-rank retractions: a survey and new results",
% Technical report, 2013
% https://sites.uclouvain.be/absil/2013.04
%
% This implementation is due to
% P.-A. Absil, I. V. Oseledets
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM


%% Compute elements of the table

weightsprop = 'One';
beta = round(N/2);
tab41;
timeOne2 = time_mean_weighted(end);

weightsprop = 'One';
beta = round(3*N/4);
tab41;
timeOne3 = time_mean_weighted(end);

weightsprop = 'One';
beta = round(N);
tab41;
timeOne4 = time_mean_weighted(end);

weightsprop = 'Delta';
beta = round(N/2);
tab41;
timeDelta2 = time_mean_weighted(end);

weightsprop = 'Delta';
beta = round(3*N/4);
tab41;
timeDelta3 = time_mean_weighted(end);

weightsprop = 'Delta';
beta = round(N);
tab41;
timeDelta4 = time_mean_weighted(end);



%% Construct the table


fid = fopen('weights.txt','w');
%fprintf(fid,'%% This file was generated with %s on %s using m=%e and n=%e \n',mfilename('fullpath'),host_name,m,n);
fprintf(fid,'%s \n', '\begin{table}');
fprintf(fid,'%s \n', '\centering');
fprintf(fid,'%s \n', '\begin{tabular}{l|l|l}');
fprintf(fid,'%s \n', '\rowcolor{lightgray}');
fprintf(fid,'%s \n', '$\beta$  & $w_{ij} = 1$ & $w_{ij} \sim \delta_{ij}$  \\ \hline');
fprintf(fid,'$N/4$  & %s & %s %s \n', '$\infty$'             ,       '$\infty$'    , '   \\');
fprintf(fid,'$N/2$  & %s & %s %s \n', num2str(timeOne2)      , num2str(timeDelta2) ,'   \\');
fprintf(fid,'$3N/4$ & %s & %s %s \n', num2str(timeOne3)      , num2str(timeDelta3) ,'   \\');
fprintf(fid,'$N$    & %s & %s %s \n', num2str(timeOne4)      , num2str(timeDelta4) ,'   \\');
fprintf(fid,'%s \n', '\end{tabular}');
fprintf(fid,'\\caption{%s} \n', 'Table 4.1');
fprintf(fid,'%s \n', '\label{tab:timingWeights}');
fprintf(fid,'%s \n', '\end{table}');
fclose(fid);

