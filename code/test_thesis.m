clear; close all;


%% Problem specifications and options
d1 = 100;
d2 = 100;
true_rank = 5;
over_sampling = 4;

r = true_rank;
noiseFac = 1e-6;

mu = 0.5; % For scaled_sgd
maxiterations = 100;
batchsize = r;
msetol = 1e-10;
relresidualtol = 1e-4; % norm of error divided by the norm of entries.
random_initialization = false;
shuffling = 'random';
stepsize = 1e-1;
stepSelect = 'bold';

condition_number = 0; % 0 for well-conditioned data; > 0 for ill-conditioned data.

fprintf('Rank %i matrix of size %i times %i and over-sampling = %i\n', true_rank, d1, d2, over_sampling);






%% Generate well-conditioned or ill-conditioned data

N = over_sampling*true_rank*(d2 + d1 -true_rank); % total entries

% The left and right factors which make up our true data matrix Y.
YL = randn(d1, true_rank);
YR = randn(d2, true_rank);

% Condition number
if condition_number > 0
    YLQ = orth(YL);
    YRQ = orth(YR);
        
    s1 = 1000;
    %     step = 1000; S0 = diag([s1:step:s1+(true_rank-1)*step]*1); % Linear decay
    S0 = s1*diag(logspace(-log10(condition_number),0,true_rank)); % Exponential decay
    
    YL = YLQ*S0;
    YR = YRQ;
    
    fprintf('Creating a matrix with singular values...\n')
    for kk = 1: length(diag(S0));
        fprintf('%s \n', num2str(S0(kk, kk), '%10.5e') );
    end
    singular_vals = svd(YL'*YL);
    condition_number = sqrt(max(singular_vals)/min(singular_vals));
    fprintf('Condition number is %f \n', condition_number);

end



% Select a random set of M entries of Y = YL YR'.
idx = unique(ceil(d2*d1*rand(1,(10*N))));
idx = idx(randperm(length(idx)));

[I, J] = ind2sub([d1, d2],idx(1:N));
[J, inxs] = sort(J); I=I(inxs)';

% Values of Y at the locations indexed by I and J.
S = sum(YL(I,:).*YR(J,:), 2);


S_noiseFree = S;

% Add noise.
noise = noiseFac*max(S)*randn(size(S));
S = S + noise;


values = sparse(I,J,S, d1,d2);
indicator = sparse(I,J,1,d1 ,d2);

[rows, cols] = find(indicator == 1);
values_vec = values(:);
entries = values_vec(sub2ind(size(values), rows, cols));


%% Interface with our algorithms

% Learning data
data_ls.rows = rows;
data_ls.cols = cols;
data_ls.entries = entries;
data_ls.nentries = length(data_ls.entries);


% Testing data
data_ts.Gs = YL;
data_ts.Hs = YR;
data_ts.nentries = 1*data_ls.nentries;
data_ts.rows = randi(d1, data_ts.nentries, 1);
data_ts.cols = randi(d2, data_ts.nentries, 1);
data_ts.entries = sum(YL(data_ts.rows,:).*YR(data_ts.cols,:), 2);




%% Initialization

if ~random_initialization,
    sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
    [U, B, V] = svds(sparse_structure, r);
    Xinit.L = U*(B.^(0.5));
    Xinit.R = V*(B.^(0.5));
    fprintf('**** Initialization by taking %i dominant SVD\n', r);
else
    Xinit.L = randn(d1, r);
    Xinit.R = randn(d2, r);
    fprintf('**** Random initialization\n');
end





%% Scaled-SGD

% Options are not mandatory
options.maxiterations = maxiterations;
options.batchsize = batchsize;
options.msetol = msetol;
options.relresidualtol = relresidualtol;
options.mu = mu;
options.stepsize = stepsize;
options.lambda = 0;
options.shuffling = shuffling;
options.stepSelect = stepSelect;

fprintf('---------------------  Scaled-SGD ---------------------\n')
[X_scaled_sgd, infos_scaled_sgd] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);





%% Plots

% Cost versus iterations
fs = 20;
figure;
semilogy(infos_scaled_sgd.cost,'-','Color','b','LineWidth',2);
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Iterations','FontSize',fs);
ylabel(ax1,'Mean square error','FontSize',fs);
axis([get(gca,'XLim') msetol 1e3])
legend('Scaled-SGD');
legend 'boxoff';
title([num2str(d1),' by ',num2str(d2),', rank ',num2str(true_rank),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
box off;




% Test error versus iterations
if ~isempty(data_ts),
    fs = 20;
    figure;
    semilogy(infos_scaled_sgd.cost_test,'-','Color','b','LineWidth',2);
    ax1 = gca;
    set(ax1,'FontSize',fs);
    xlabel(ax1,'Iterations','FontSize',fs);
    ylabel(ax1,'Means square error on test set','FontSize',fs);
    axis([get(gca,'XLim') msetol 1e3])
    legend('Scaled-SGD');
    legend 'boxoff';
    title([num2str(d1),' by ',num2str(d2),', rank ',num2str(true_rank),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
    box off;
end





