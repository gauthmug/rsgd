% Add folders to path.


addpath(pwd);


cd main/;
addpath(genpath(pwd));
cd ..;

cd data/;
addpath(genpath(pwd));
cd ..;

cd scriptFigures/;
addpath(genpath(pwd));
cd ..;

cd scriptTables/;
addpath(genpath(pwd));
cd ..;

