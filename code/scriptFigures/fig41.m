clear; clc; close all;

%% Intro
% Running this code generates Figure 4.1 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 100;
rng(SD);

%% Load full Jester dataset of size 24983-by-100.
load('data/jester_mat.mat');

%% Randomly select nu rows and creat the data structure
nu = 2000; % Number of users selected
%r = 7;

noiseFac = 0;
mu = 0.5;
maxiterations = 100;
msetol = 1e-8; 
relresidualtol = 1e-4; 
random_initialization = true;
stepSelect = 'bold';
stepsize = 1e-2;
lambda = 0;
flag = 'in';



numberExp = 5;
experim = 1:numberExp;

numberRanks = 10;
ranks = 1:numberRanks;

train_err_exp = zeros(numberExp, numberRanks);
test_err_exp = zeros(numberExp, numberRanks);

train_err = zeros(numberRanks, 1);
test_err = zeros(numberRanks, 1);

for e = experim
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    
    p = randperm(size(A,1), nu);
    A = A(p, :); % Matrix of size nu-by-100
    Avec = A(:);
    
    Avecindices = 1:length(Avec);
    Avecindices = Avecindices';
    i = ones(length(Avec),1);
    i(Avec == 99) = 0;
    Avecindices_final = Avecindices(logical(i));
    [I, J] = ind2sub([size(A,1)  100],Avecindices_final);
    
    Avecsfinall = Avec(logical(i));
    
    
    [Isort, indI] = sort(I,'ascend');
    
    
    data_real.rows = Isort;
    data_real.cols = J(indI);
    data_real.entries = Avecsfinall(indI);
    data_real.nentries = length(data_real.entries);
    
    % Test data: two ratings per user
    [~,IA,~] = unique(Isort,'stable');
    data_ts_ind = [];
    for ii = 1 : length(IA);
        if ii < length(IA)
            inneridx = randperm(IA(ii+1) - IA(ii), 2);
        else
            inneridx = randperm(length(data_real.entries) +1 - IA(ii), 2);
        end
        data_ts_ind = [data_ts_ind; IA(ii) + inneridx' - 1];
    end
    
    
    
    
    data_ts.rows = data_real.rows(data_ts_ind);
    data_ts.cols = data_real.cols(data_ts_ind);
    data_ts.entries = data_real.entries(data_ts_ind);
    data_ts.nentries = length(data_ts.rows);
    
    
    % Train data
    data_ls = data_real;
    data_ls.rows(data_ts_ind) = [];
    data_ls.cols(data_ts_ind) = [];
    data_ls.entries(data_ts_ind) = [];
    data_ls.nentries = length(data_ls.rows);
    
    
    
    
    
    % Permute train data
    random_order = randperm(length(data_ls.rows));
    data_ls.rows = data_ls.rows(random_order);
    data_ls.cols = data_ls.cols(random_order);
    data_ls.entries = data_ls.entries(random_order);
    
    
    %% Dimensions
    d1 = size(A, 1);
    d2 = size(A, 2);
    
    for r = ranks
        
        fprintf('Rank %i out of %i \n', r, numberRanks);
        
        
        %% Initialization
        
        if ~random_initialization,
            sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
            [U, B, V] = svds(sparse_structure, r);
            Xinit.L = U*(B.^(0.5));
            Xinit.R = V*(B.^(0.5));
            fprintf('**** Initialization by taking %i dominant SVD\n', r);
        else
            Xinit.L = randn(d1, r);
            Xinit.R = randn(d2, r);
            fprintf('**** Random initialization\n');
        end
        
        
        
        
        
        %% Scaled-SGD
        options.maxiterations = maxiterations;
        options.msetol = msetol;
        options.relresidualtol = relresidualtol;
        options.mu = mu;
        options.stepSelect = stepSelect;
        options.stepsize = stepsize;
        options.lambda = lambda; 
        options.flag = flag;
        
        
        options.shuffle = 'random';
        fprintf('---------------------  Random Scaled-SGD ---------------------\n')
        [X_scaled_sgd_random, infos_scaled_sgd_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
        
        train_err(r,1) = infos_scaled_sgd_random.cost(end);
        test_err(r,1) = infos_scaled_sgd_random.cost_test(end);
        
        train_err_exp(e,r) = train_err(r,1);
        test_err_exp(e,r) = test_err(r,1);
        
    end
    
    
    
end

mean_train = mean(train_err_exp, 1);
mean_test = mean(test_err_exp, 1);






%% Plots


fs = 20;
figure;
plot(mean_train,'-o','LineWidth',1);
hold on;
plot(mean_test,'-o','LineWidth',1);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Ranks','FontSize',fs);
ylabel(ax1,'MSE','FontSize',fs);
%axis([get(gca,'XLim') 0 maxY])
legend('Train','Test');
legend 'boxoff';
%title([num2str(n),' by ',num2str(m),', rank ',num2str(r)])
%title('Evolution of the training error and testing error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('rankJokes');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');










