clear; clc; close all;

%% Intro
% Running this code generates Figure E.3 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 0501918;
rng(SD);



filename = '/data/ml-20m/ratings.csv';
totalRows = 20000263;
totalRows2 = (totalRows - 1)/2;
numberRatings = 50000; % <--- change this value

maxiterations = 100;
batchsize = 1;
msetol = 1e-8;
relresidualtol = 1e-8;
mu = 0.5;
stepsize = 1e-1;
lambda = 0;
flag = 'in';
stepSelect = 'bold';


numberExp = 2;
experim = 1:numberExp;

numberRanks = 15;
ranks = 1:numberRanks;

train_err = zeros(numberExp, numberRanks);
test_err = zeros(numberExp, numberRanks);








for e = experim
    
    
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    
    nu = randi(totalRows2);
    nuNext = nu + numberRatings;
    
    %% Load MovieLens 20M data that has users as rows and movies as columns.
    data = csvread(filename,nu, 0,[nu 0 nuNext 2]);
    myDataMat = spconvert(data);
    
    data_real.cols = data(:,1); % users
    data_real.rows = data(:,2); % movies
    data_real.entries = data(:,3); % entries
    
    % Movies as rows. Replace movie ids by integers.
    [rows_sorted, IA, IC] = unique(data_real.rows);
    movie_ids = 1:length(rows_sorted);
    tmp = movie_ids(IC);
    data_real.rows = tmp;
    
    
    %% Remove the ones with entries zero
    rm_ind = find(data_real.entries == 0);
    data_real.entries(rm_ind) =[];
    data_real.rows(rm_ind) =[];
    data_real.cols(rm_ind) =[];
    
    
    
    
    %% permute them
    random_order = randperm(length(data_real.rows));
    data_real.rows = data_real.rows(random_order);
    data_real.cols = data_real.cols(random_order);
    data_real.entries = data_real.entries(random_order);
    data_real.nentries = length(data_real.entries);
    
    
    %% Train / test split
    prop_known = 0.6; % or 0.9 % Training fraction
    
    % Data training
    allidx = 1 : length(data_real.rows); % all indices
    
    M = round(prop_known*length(data_real.rows)); % Number of entries known
    idx = 1 : M; % indices for training
    Matcom.row = (data_real.rows(idx))';
    Matcom.col = (data_real.cols(idx))';
    Matcom.values_Omega = (data_real.entries(idx))';
    
    
    d1 = max(data_real.rows);
    d2 = max(data_real.cols);
    
    
    S = sparse(Matcom.row', Matcom.col', Matcom.values_Omega', d1, d2);
    [I_0, J_0, data_0] = find(S);
    Matcom.row = I_0';
    Matcom.col = J_0';
    Matcom.values_Omega = data_0';
    M = length(Matcom.row);
    
    
    data_ls.rows = Matcom.row';
    data_ls.cols = Matcom.col';
    data_ls.entries = Matcom.values_Omega';
    data_ls.nentries = length(data_ls.rows);
    
    
    % Test data
    M_test = length(data_real.rows) - M;
    L = length(data_real.rows);
    idx_test = allidx(L - M_test + 1: L);
    
    data_ts.rows = (data_real.rows(idx_test));
    data_ts.cols = (data_real.cols(idx_test));
    data_ts.entries = (data_real.entries(idx_test));
    data_ts.nentries = length(data_ts.entries);
    %
    PA = sparse(data_ls.rows, data_ls.cols, data_ls.entries, d1 ,d2);
    P = sparse(data_ls.rows, data_ls.cols, 1, d1, d2);
    
    PA_test = sparse(data_ts.rows, data_ts.cols, data_ts.entries, d1, d2);
    P_test = sparse(data_ts.rows, data_ts.cols, 1, d1, d2);
    
    for r = ranks
        
        fprintf('Rank %i out of %i \n', r, numberRanks);
        
        
        %% Initialization
        
        random_initialization = true;
        
        if ~random_initialization,
            sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,n,m);
            [U, B, V] = svds(sparse_structure, r);
            Xinit.L = U*(B.^(0.5));
            Xinit.R = V*(B.^(0.5));
            fprintf('**** Initialization by taking %i dominant SVD\n', r);
        else
            Xinit.L = randn(d1, r);
            Xinit.R = randn(d2, r);
            fprintf('**** Random initialization\n');
        end
        
        
        %% Scaled-SGD
        options.maxiterations = maxiterations;
        options.batchsize = batchsize;
        options.msetol = msetol;
        options.relresidualtol = relresidualtol;
        options.mu = mu;
        options.stepsize = stepsize;
        options.lambda = lambda;
        options.flag = flag;
        options.stepSelect = stepSelect;
        
        %% Random
        options.shuffle = 'random';
        fprintf('---------------------  Random shuffling ---------------------\n')
        [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
        
        
        
        train_err(e,r) = sqrt(infos_random.cost(end));
        test_err(e,r) = sqrt(infos_random.cost_test(end));
        
    
    end
    
    
end

%% Mean

mean_train = mean(train_err, 1);
mean_test = mean(test_err, 1);


%% Plots

fs = 20;
figure;
plot(mean_train,'-o','LineWidth',1);
hold on;
plot(mean_test,'-o','LineWidth',1);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Ranks','FontSize',fs);
ylabel(ax1,'RMSE','FontSize',fs);
%axis([get(gca,'XLim') 0 maxY])
legend('Train','Test');
legend 'boxoff';
%title([num2str(n),' by ',num2str(m),', rank ',num2str(r)])
%title('Evolution of the training error and testing error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('moviesRank');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');




fprintf('We are done :) \n')
%load handel
%sound(y,Fs)




