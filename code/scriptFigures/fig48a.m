clc; clear; close all;

%% Intro
% Running this code generates Figure 4.8a of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 2650;
%SD = 'shuffle';
rng(SD);


%% Problem specifications and options
d1 = 1000;
d2 = 1000;
r = 10;
over_sampling = 6;
noiseFac = 1e-6;

N = over_sampling*r*(d2 + d1 - r);
% total number of known entries
% this corresponds to the size of the set Omega

if mod(N,2) == 1
    N = N-1;
end

%flagSize = 'large';
flagSize = 'small';

switch flagSize
    case 'large'
        batchsize1 = floor(N/10);
        batchsize2 = floor(N/5);
        batchsize3 = floor(N);
    case 'small'
        batchsize1 = floor(N/100);
        batchsize2 = floor(N/50);
        batchsize3 = floor(N/10);
        
end


mu = 0.5;
maxiterations = N/2;
msetol = 1e-8;
relresidualtol = 1e-40; % norm of error divided by the norm of entries.
random_initialization = true;
condition_number = 0; % 0 for well-conditioned data; > 0 for ill-conditioned data.
flag = 'in';
stepsize = 1e-1;
stepSelect = 'bold';



fprintf('Rank %i matrix of size %i times %i and over-sampling = %i\n', r, d1, d2, over_sampling);

numberExp = 3;
experim = 1:numberExp;

time_random_final = zeros(numberExp, 1);
time_constant1_final = zeros(numberExp, 1);
time_constant2_final = zeros(numberExp, 1);
time_constant3_final = zeros(numberExp, 1);

time_random = zeros(maxiterations, numberExp);
time_constant1 = zeros(maxiterations, numberExp);
time_constant2 = zeros(maxiterations, numberExp);
time_constant3 = zeros(maxiterations, numberExp);

err_random = zeros(maxiterations, numberExp);
err_constant1 = zeros(maxiterations, numberExp);
err_constant2 = zeros(maxiterations, numberExp);
err_constant3 = zeros(maxiterations, numberExp);

for e = experim
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    
    %% Generate well-conditioned or ill-conditioned data
    
    
    
    % The left and right factors which make up our true data matrix Y.
    YL = randn(d1, r);
    YR = randn(d2, r);
    
    if condition_number > 0
        YLQ = orth(YL);
        YRQ = orth(YR);
        
        s1 = 1000;
        S0 = s1*diag(logspace(-log10(condition_number),0,r)); % Exponential decay
        
        YL = YLQ*S0;
        YR = YRQ;
        
        fprintf('Creating a matrix with singular values...\n')
        for kk = 1: length(diag(S0));
            fprintf('%s \n', num2str(S0(kk, kk), '%10.5e') );
        end
        singular_vals = svd(YL'*YL);
        condition_number = sqrt(max(singular_vals)/min(singular_vals));
        fprintf('Condition number is %f \n', condition_number);
        
    end
    
    % Select a random set of M entries of Y = YL YR'.
    idx = unique(ceil(d2*d1*rand(1,(10*N))));
    idx = idx(randperm(length(idx)));
    
    [I, J] = ind2sub([d1, d2],idx(1:N));
    [J, inxs] = sort(J);
    I = I(inxs)';
    
    % Values of Y at the locations indexed by I and J.
    cleanS = sum(YL(I,:).*YR(J,:), 2);
    
    % Add noise.
    noise = noiseFac*max(cleanS)*randn(size(cleanS));
    S = cleanS + noise;
    
    
    values = sparse(I,J,S,d1,d2);
    indicator = sparse(I,J,1,d1,d2);
    
    [rows, cols] = find(indicator == 1);
    values_vec = values(:);
    entries = values_vec(sub2ind(size(values), rows, cols));
    
    
    %% Interface with our algorithms
    
    % Learning data
    data_ls.rows = rows;
    data_ls.cols = cols;
    data_ls.entries = entries;
    data_ls.nentries = length(data_ls.entries);
    
    
    % Testing data
    data_ts.nentries = 1*data_ls.nentries;
    data_ts.rows = randi(d1, data_ts.nentries, 1);
    data_ts.cols = randi(d2, data_ts.nentries, 1);
    data_ts.entries = sum(YL(data_ts.rows,:).*YR(data_ts.cols,:), 2);
    
    
    
    
    %% Options
    options.maxiterations = maxiterations;
    options.msetol = msetol;
    options.relresidualtol = relresidualtol;
    options.mu = mu;
    options.stepsize = stepsize;
    options.lambda = 0;
    options.flag = flag;
    options.stepSelect = stepSelect;
   
    
    %% Initialization
    
    if random_initialization,
        Xinit.L = randn(d1, r);
        Xinit.R = randn(d2, r);
        fprintf('**** Random initialization\n');
    else
        sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
        [U, B, V] = svds(sparse_structure, r);
        Xinit.L = U*(B.^(0.5));
        Xinit.R = V*(B.^(0.5));
        fprintf('**** Initialization by taking %i dominant SVD\n', r);
    end
    
    
    
    
    %% Standard SGD
    
    options.shuffle = 'random';
    
    fprintf('---------------------  Standard SGD ---------------------\n')
    [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_random_final(e, 1) = infos_random.time(end);
    time_random(1:size(infos_random.time),e) = infos_random.time;
    err_random(1:size(infos_random.rmse),e) = infos_random.rmse;
    
    %% Hybrid SGD - Constant1
    
    options.batchflag = 'constant';
    options.batchsize = batchsize1;
    fprintf('---------------------  Constant 1 ---------------------\n')
    [X_constant1, infos_constant1] = batchSGD(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_constant1_final(e, 1) = infos_constant1.time(end);
    time_constant1(1:size(infos_constant1.time),e) = infos_constant1.time;
    err_constant1(1:size(infos_constant1.rmse),e) = infos_constant1.rmse;
    
    %% Hybrid SGD - Constant2
    
    options.batchflag = 'constant';
    options.batchsize = batchsize2;
    fprintf('---------------------  Constant 2 ---------------------\n')
    [X_constant2, infos_constant2] = batchSGD(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_constant2_final(e, 1) = infos_constant2.time(end);
    time_constant2(1:size(infos_constant2.time),e) = infos_constant2.time;
    err_constant2(1:size(infos_constant2.rmse),e) = infos_constant2.rmse;
    
    %% Hybrid SGD - Constant3
    
    options.batchflag = 'constant';
    options.batchsize = batchsize3;
    fprintf('---------------------  Constant 3 ---------------------\n')
    [X_constant3, infos_constant3] = batchSGD(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_constant3_final(e, 1) = infos_constant3.time(end);
    time_constant3(1:size(infos_constant3.time),e) = infos_constant3.time;
    err_constant3(1:size(infos_constant3.rmse),e) = infos_constant3.rmse;

end

%% Averages

mean_err_random = mean(err_random, 2);
mean_err_constant1 = mean(err_constant1, 2);
mean_err_constant2 = mean(err_constant2, 2);
mean_err_constant3 = mean(err_constant3, 2);

mean_random_time = mean(time_random, 2);
mean_constant_time1 = mean(time_constant1, 2);
mean_constant_time2 = mean(time_constant2, 2);
mean_constant_time3 = mean(time_constant3, 2);

mean_time_random_final = mean(time_random_final);
mean_time_constant1_final = mean(time_constant1_final);
mean_time_constant2_final = mean(time_constant2_final);
mean_time_constant3_final = mean(time_constant3_final);


%% Plots

% Cost versus iterations
fs = 20;
figure;
semilogy(mean_random_time, mean_err_random,'-','LineWidth',2);
hold on;
semilogy(mean_constant_time1, mean_err_constant1,'-','LineWidth',2);
hold on;
semilogy(mean_constant_time2, mean_err_constant2,'-','LineWidth',2);
hold on;
semilogy(mean_constant_time3, mean_err_constant3,'-','LineWidth',2);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Time (s)','FontSize',fs);
ylabel(ax1,'RMSE','FontSize',fs);
%axis([get(gca,'XLim') msetol 1e3])
%legend('Entire epoch', ['Batchsize = ', num2str(batchsize1)], ['Batchsize = ', num2str(batchsize2)], ['Batchsize = ', num2str(batchsize3)]);
legend('Entire epoch', ['Batchsize = ', 'M/100'], ['Batchsize = ', 'M/50'], ['Batchsize = ', 'M/10']);
legend 'boxoff';
title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling)])
box off;


switch batchflag
    case 'large'
        saveas(gcf,sprintf('largeConstant'),'epsc')
    case 'small'
        saveas(gcf,sprintf('smallConstant'),'epsc') 
end







fprintf('We are done :) \n')





