clear; clc; close all;

%% Intro
% Running this code generates Figure 4.5 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 3105;
%SD = 'shuffle';
rng(SD);

%% Load full Jester dataset of size 24983-by-100.
load('data/jester_mat.mat');

%% Randomly select nu rows and creat the data structure
nu = 2000; % Number of users selected
%r = 7;

noiseFac = 0;
mu = 0.5;
maxiterations = 40;
batchsize = 1;
msetol = 1e-8; % Mean square error tolerance.
relresidualtol = 1e-4; % Norm of error divided by the norm of entries.
random_initialization = true;
shuffle = true; % Randomly shuffling of the entries for SGD updates.
r = 5;
stepsize = 1e-1;
lambda = 0;
flag = 'in';
stepSelect = 'bold';


numberExp = 3;
experim = 1:numberExp;

train_rmse_no = zeros(maxiterations, numberExp);
train_rmse_random = zeros(maxiterations, numberExp);
train_rmse_random_replacement = zeros(maxiterations, numberExp);
train_rmse_shuffling = zeros(maxiterations, numberExp);

test_rmse_no = zeros(maxiterations, numberExp);
test_rmse_random = zeros(maxiterations, numberExp);
test_rmse_random_replacement = zeros(maxiterations, numberExp);
test_rmse_shuffling = zeros(maxiterations, numberExp);

for e = experim
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    
    p = randperm(size(A,1), nu);
    A = A(p, :); % Matrix of size nu-by-100
    Avec = A(:);
    
    Avecindices = 1:length(Avec);
    Avecindices = Avecindices';
    i = ones(length(Avec),1);
    i(Avec == 99) = 0;
    Avecindices_final = Avecindices(logical(i));
    [I, J] = ind2sub([size(A,1)  100],Avecindices_final);
    
    Avecsfinall = Avec(logical(i));
    
    
    [Isort, indI] = sort(I,'ascend');
    
    
    data_real.rows = Isort;
    data_real.cols = J(indI);
    data_real.entries = Avecsfinall(indI);
    data_real.nentries = length(data_real.entries);
    
    % Test data: two ratings per user
    [~,IA,~] = unique(Isort,'stable');
    data_ts_ind = [];
    %data_ts_ind = zeros(length(IA),1);
    for ii = 1 : length(IA);
        if ii < length(IA)
            inneridx = randperm(IA(ii+1) - IA(ii), 2);
        else
            inneridx = randperm(length(data_real.entries) +1 - IA(ii), 2);
        end
        data_ts_ind = [data_ts_ind; IA(ii) + inneridx' - 1];
        %data_ts_ind(ii,1) = IA(ii) + inneridx' - 1;
    end
    
    
    
    
    data_ts.rows = data_real.rows(data_ts_ind);
    data_ts.cols = data_real.cols(data_ts_ind);
    data_ts.entries = data_real.entries(data_ts_ind);
    data_ts.nentries = length(data_ts.rows);
    
    
    % Train data
    data_ls = data_real;
    data_ls.rows(data_ts_ind) = [];
    data_ls.cols(data_ts_ind) = [];
    data_ls.entries(data_ts_ind) = [];
    data_ls.nentries = length(data_ls.rows);
    
    
    
    
    
    % Permute train data
    random_order = randperm(length(data_ls.rows));
    data_ls.rows = data_ls.rows(random_order);
    data_ls.cols = data_ls.cols(random_order);
    data_ls.entries = data_ls.entries(random_order);
    
    
    %% Dimensions
    d1 = size(A, 1);
    d2 = size(A, 2);
    
    
    %% Initialization
    
    if ~random_initialization,
        sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
        [U, B, V] = svds(sparse_structure, r);
        Xinit.L = U*(B.^(0.5));
        Xinit.R = V*(B.^(0.5));
        fprintf('**** Initialization by taking %i dominant SVD\n', r);
    else
        Xinit.L = randn(d1, r);
        Xinit.R = randn(d2, r);
        fprintf('**** Random initialization\n');
    end
    
    
    
    
    
    %% Scaled-SGD
    options.maxiterations = maxiterations;
    options.batchsize = batchsize;
    options.msetol = msetol;
    options.relresidualtol = relresidualtol;
    options.mu = mu;
    options.stepsize = stepsize;
    options.lambda = lambda; 
    options.flag = flag;
    options.stepSelect = stepSelect;
    
    %% No shuffling
    
    % Options are not mandatory
    options.shuffle = 'no';
    
    fprintf('---------------------  No shuffling ---------------------\n')
    [X_no, infos_no] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Random SGD
    
    % Options are not mandatory
    options.shuffle = 'random';
    
    fprintf('---------------------  Random shuffling ---------------------\n')
    [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Random-replacement SGD
    
    % Options are not mandatory
    options.shuffle = 'random_replacement';
    
    fprintf('---------------------  Random shuffling with replacement ---------------------\n')
    [X_random_replacement, infos_random_replacement] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Shuffled SGD
    
    % Options are not mandatory
    options.shuffle = 'shuffling';
    
    fprintf('---------------------  Smart shuffling ---------------------\n')
    [X_shuffling, infos_shuffling] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Averages
    
    train_rmse_no(1:size(infos_no.rmse),e)                                  =     infos_no.rmse;
    train_rmse_random(1:size(infos_random.rmse),e)                          =     infos_random.rmse;
    train_rmse_random_replacement(1:size(infos_random_replacement.rmse),e)  =     infos_random_replacement.rmse;
    train_rmse_shuffling(1:size(infos_shuffling.rmse),e)                    =     infos_shuffling.rmse;
    
    test_rmse_no(1:size(infos_no.rmse_test),e)                                  =     infos_no.rmse_test;
    test_rmse_random(1:size(infos_random.rmse_test),e)                          =     infos_random.rmse_test;
    test_rmse_random_replacement(1:size(infos_random_replacement.rmse_test),e)  =     infos_random_replacement.rmse_test;
    test_rmse_shuffling(1:size(infos_shuffling.rmse_test),e)                    =     infos_shuffling.rmse_test;
    
    
    
end

%% Mean and standard deviation

train_mean_no = mean(train_rmse_no, 2);
train_mean_random = mean(train_rmse_random, 2);
train_mean_random_replacement = mean(train_rmse_random_replacement, 2);
train_mean_shuffling = mean(train_rmse_shuffling, 2);

test_mean_no = mean(test_rmse_no, 2);
test_mean_random = mean(test_rmse_random, 2);
test_mean_random_replacement = mean(test_rmse_random_replacement, 2);
test_mean_shuffling = mean(test_rmse_shuffling, 2);




%% Plots

% Train
fs = 20;
figure;
NNN = length(test_mean_no);
XAXIS = linspace(1,NNN,NNN);
indis = 5;
semilogy(XAXIS(indis:end),train_mean_no(indis:end),'-o','LineWidth',1);
hold on;
semilogy(XAXIS(indis:end),train_mean_random(indis:end),'-o','LineWidth',1);
hold on;
semilogy(XAXIS(indis:end),train_mean_random_replacement(indis:end),'-o','LineWidth',1);
hold on;
semilogy(XAXIS(indis:end),train_mean_shuffling(indis:end),'-o','LineWidth',1);
hold on;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Iterations','FontSize',fs);
ylabel(ax1,'Train RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('No shuffling', 'Random without repl.', 'Random with repl.', 'Smart shuffling');
legend 'boxoff';
%title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
%title('Jester: training error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('jokesFourTrain');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');



% Test
figure;
semilogy(XAXIS(indis:end),test_mean_no(indis:end),'-o','LineWidth',1);
hold on;
semilogy(XAXIS(indis:end),test_mean_random(indis:end),'-o','LineWidth',1);
hold on;
semilogy(XAXIS(indis:end),test_mean_random_replacement(indis:end),'-o','LineWidth',1);
hold on;
semilogy(XAXIS(indis:end),test_mean_shuffling(indis:end),'-o','LineWidth',1);
hold on;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Iterations','FontSize',fs);
ylabel(ax1,'Test RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('No shuffling', 'Random without repl.', 'Random with repl.', 'Smart shuffling');
legend 'boxoff';
%title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
%title('Jester: testing error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('jokesFourTest');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');

fprintf('We are done :) \n')
%load handel
%sound(y,Fs)










