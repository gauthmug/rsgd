clear; clc; close all;

%% Intro
% Running this code generates Figure 4.7a of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 031019;
rng(SD);

%% Load full Jester dataset of size 24983-by-100.
load('data/jester_mat.mat');

%% Randomly select nu rows and create the data structure
nu = 2000; % Number of users selected
r = 5;

noiseFac = 0;
mu = 0.5;
maxiterations = 50;
batchsize = 1;
msetol = 1e-8; % Mean square error tolerance.
relresidualtol = 1e-4; % Norm of error divided by the norm of entries.
random_initialization = true;
shuffle = true; % Randomly shuffling of the entries for SGD updates.
stepsize = 1e-2;
lambda = 0;
flag = 'in';
stepSelect = 'bold';
weightCost = 'cheap';

weightsprop = 'One';
%weightsprop = 'Delta';


numberExp = 5;
experim = 1:numberExp;


train_rmse_random = zeros(maxiterations, numberExp);
train_rmse_weighted = zeros(maxiterations, numberExp);

test_rmse_random = zeros(maxiterations, numberExp);
test_rmse_weighted = zeros(maxiterations, numberExp);

time_random = zeros(maxiterations, numberExp);
time_weighted = zeros(maxiterations, numberExp);


for e = experim
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    
    p = randperm(size(A,1), nu);
    A = A(p, :); % Matrix of size nu-by-100
    Avec = A(:);
    
    Avecindices = 1:length(Avec);
    Avecindices = Avecindices';
    i = ones(length(Avec),1);
    i(Avec == 99) = 0;
    Avecindices_final = Avecindices(logical(i));
    [I, J] = ind2sub([size(A,1)  100],Avecindices_final);
    
    Avecsfinall = Avec(logical(i));
    
    
    [Isort, indI] = sort(I,'ascend');
    
    
    data_real.rows = Isort;
    data_real.cols = J(indI);
    data_real.entries = Avecsfinall(indI);
    data_real.nentries = length(data_real.entries);
    
    % Test data: two ratings per user
    [~,IA,~] = unique(Isort,'stable');
    data_ts_ind = [];
    for ii = 1 : length(IA);
        if ii < length(IA)
            inneridx = randperm(IA(ii+1) - IA(ii), 2);
        else
            inneridx = randperm(length(data_real.entries) +1 - IA(ii), 2);
        end
        data_ts_ind = [data_ts_ind; IA(ii) + inneridx' - 1];
    end
    
    
    
    
    data_ts.rows = data_real.rows(data_ts_ind);
    data_ts.cols = data_real.cols(data_ts_ind);
    data_ts.entries = data_real.entries(data_ts_ind);
    data_ts.nentries = length(data_ts.rows);
    
    
    % Train data
    data_ls = data_real;
    data_ls.rows(data_ts_ind) = [];
    data_ls.cols(data_ts_ind) = [];
    data_ls.entries(data_ts_ind) = [];
    data_ls.nentries = length(data_ls.rows);
    
    
    
    % Permute train data
    random_order = randperm(length(data_ls.rows));
    data_ls.rows = data_ls.rows(random_order);
    data_ls.cols = data_ls.cols(random_order);
    data_ls.entries = data_ls.entries(random_order);
    
    
    %% Dimensions
    d1 = size(A, 1);
    d2 = size(A, 2);
    
    
    
    
    
    %% Initialization
    
    if ~random_initialization,
        sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
        [U, B, V] = svds(sparse_structure, r);
        Xinit.L = U*(B.^(0.5));
        Xinit.R = V*(B.^(0.5));
        fprintf('**** Initialization by taking %i dominant SVD\n', r);
    else
        Xinit.L = randn(d1, r);
        Xinit.R = randn(d2, r);
        fprintf('**** Random initialization\n');
    end
    
    
    
    
    %% Beta
    N = data_ls.nentries; % Number of known entries.

    beta = round(3*N/4); % <----- change this value
    
    
    %% Scaled-SGD
    options.maxiterations = maxiterations;
    options.batchsize = batchsize;
    options.msetol = msetol;
    options.relresidualtol = relresidualtol;
    options.mu = mu;
    options.stepsize = stepsize;
    options.lambda = lambda;
    options.flag = flag;
    options.weightsprop = weightsprop;
    options.beta = beta;
    options.stepSelect = stepSelect;
    options.weightCost = weightCost;
    
    %% Random
    options.shuffle = 'random';
    fprintf('---------------------  Random shuffling ---------------------\n')
    [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    
    %% Weights
    options.shuffle = 'weighted';
    fprintf('--------------------- Weighted shuffling ---------------------\n')
    [X_weighted, infos_weighted] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
   
    
    %% Ratio
    %nnz(w)/data_ls.nentries
    
    %% Averages
    train_rmse_random(1:size(infos_random.rmse),e)              = infos_random.rmse;
    train_rmse_weighted(1:size(infos_weighted.rmse),e)          = infos_weighted.rmse;
    
    test_rmse_random(1:size(infos_random.rmse_test),e)          = infos_random.rmse_test;
    test_rmse_weighted(1:size(infos_weighted.rmse_test),e)      = infos_weighted.rmse_test;
    
    time_random(1:size(infos_random.time), e)                   = infos_random.time;
    time_weighted(1:size(infos_weighted.time), e)               = infos_weighted.time;
    
    
    
end

%% Averages
train_mean_random = mean(train_rmse_random, 2);
train_mean_weighted = mean(train_rmse_weighted, 2);

test_mean_random = mean(test_rmse_random, 2);
test_mean_weighted = mean(test_rmse_weighted, 2);

time_mean_random = mean(time_random, 2);
time_mean_weighted = mean(time_weighted, 2);



%% Plot
% Train and Test
fs = 20;
figure;
blue = [0    0.4470    0.7410];
red = [0.8500    0.3250    0.0980];
semilogy(time_mean_random(1:end),train_mean_random(1:end),'-o','color',blue,'LineWidth',1);
hold on;
semilogy(time_mean_random(1:end),test_mean_random(1:end),'-*','color',blue, 'LineWidth',1);
hold on;
semilogy(time_mean_weighted(1:end),train_mean_weighted(1:end),'-o','color',red, 'LineWidth',1);
hold on;
semilogy(time_mean_weighted(1:end),test_mean_weighted(1:end),'-*','color',red, 'LineWidth',1);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Time (s)','FontSize',fs);
ylabel(ax1,'RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('Random shuffling (Train)', 'Random shuffling (Test)', 'Weighted shuffling (Train)', 'Weighted shuffling (Test)');
legend 'boxoff';
%title([num2str(n),' by ',num2str(m),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
%title('')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = strcat('jokesWeights',weightsprop);
saveas(gcf, fullfile(fileDir,fileName), 'epsc');


fprintf('We are done :) \n')
%load handel
%sound(y,Fs)


