clc; clear; close all;

%% Intro
% Running this code generates Figure 4.9 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 2619;
%SD = 'shuffle';
rng(SD);


%% Problem specifications and options
d1 = 1000;
d2 = 1000;
r = 10;
over_sampling = 6;
noiseFac = 1e-6;


N = over_sampling*r*(d2 + d1 - r);
% total number of known entries
% this corresponds to the size of the set Omega

if mod(N,2) == 1
    N = N-1;
end


mu = 0.5;
maxiterations = N/100;
batchsize = N/10;
batchflag = 'increase';
msetol = 1e-8;
relresidualtol = 1e-40;
random_initialization = true;
condition_number = 0; 
flag = 'in';
stepsize = 1e-1;
stepSelect = 'bold';


fprintf('Rank %i matrix of size %i times %i and over-sampling = %i\n', r, d1, d2, over_sampling);

numberExp = 3;
experim = 1:numberExp;



time_random_final = zeros(numberExp, 1);
time_increase_final = zeros(numberExp, 1);
time_shuffle_final = zeros(numberExp, 1);
time_constant_final = zeros(numberExp, 1);

time_random = zeros(maxiterations, numberExp);
time_increase = zeros(maxiterations, numberExp);
time_shuffle = zeros(maxiterations, numberExp);
time_constant = zeros(maxiterations, numberExp);

err_random = zeros(maxiterations, numberExp);
err_increase = zeros(maxiterations, numberExp);
err_shuffle = zeros(maxiterations, numberExp);
err_constant = zeros(maxiterations, numberExp);

for e = experim
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    
    %% Generate well-conditioned or ill-conditioned data
    
    
    
    % The left and right factors which make up our true data matrix Y.
    YL = randn(d1, r);
    YR = randn(d2, r);
    
    if condition_number > 0
        YLQ = orth(YL);
        YRQ = orth(YR);
        
        s1 = 1000;
        %     step = 1000; S0 = diag([s1:step:s1+(true_rank-1)*step]*1); % Linear decay
        S0 = s1*diag(logspace(-log10(condition_number),0,r)); % Exponential decay
        
        YL = YLQ*S0;
        YR = YRQ;
        
        fprintf('Creating a matrix with singular values...\n')
        for kk = 1: length(diag(S0));
            fprintf('%s \n', num2str(S0(kk, kk), '%10.5e') );
        end
        singular_vals = svd(YL'*YL);
        condition_number = sqrt(max(singular_vals)/min(singular_vals));
        fprintf('Condition number is %f \n', condition_number);
        
    end
    
    % Select a random set of M entries of Y = YL YR'.
    idx = unique(ceil(d2*d1*rand(1,(10*N))));
    idx = idx(randperm(length(idx)));
    
    [I, J] = ind2sub([d1, d2],idx(1:N));
    [J, inxs] = sort(J);
    I = I(inxs)';
    
    % Values of Y at the locations indexed by I and J.
    cleanS = sum(YL(I,:).*YR(J,:), 2);
    
    % Add noise.
    noise = noiseFac*max(cleanS)*randn(size(cleanS));
    S = cleanS + noise;
    
    
    values = sparse(I,J,S,d1,d2);
    indicator = sparse(I,J,1,d1,d2);
    
    [rows, cols] = find(indicator == 1);
    values_vec = values(:);
    entries = values_vec(sub2ind(size(values), rows, cols));
    
    
    %% Interface with our algorithms
    
    % Learning data
    data_ls.rows = rows;
    data_ls.cols = cols;
    data_ls.entries = entries;
    data_ls.nentries = length(data_ls.entries);
    
    
    % Testing data
    data_ts.nentries = 1*data_ls.nentries;
    data_ts.rows = randi(d1, data_ts.nentries, 1);
    data_ts.cols = randi(d2, data_ts.nentries, 1);
    data_ts.entries = sum(YL(data_ts.rows,:).*YR(data_ts.cols,:), 2);
    
    
    
    
    %% Options
    options.maxiterations = maxiterations;
    options.batchsize = batchsize;
    options.msetol = msetol;
    options.relresidualtol = relresidualtol;
    options.mu = mu;
    options.stepsize = stepsize;
    options.lambda = 0;
    options.flag = flag;
    options.stepSelect = stepSelect;
   
    
    %% Initialization
    
    if random_initialization,
        Xinit.L = randn(d1, r);
        Xinit.R = randn(d2, r);
        fprintf('**** Random initialization\n');
    else
        sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
        [U, B, V] = svds(sparse_structure, r);
        Xinit.L = U*(B.^(0.5));
        Xinit.R = V*(B.^(0.5));
        fprintf('**** Initialization by taking %i dominant SVD\n', r);
    end
    
    
    
    
    %% Standard SGD
    

    options.shuffle = 'random';
    fprintf('---------------------  Standard SGD ---------------------\n')
    [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_random_final(e, 1) = infos_random.time(end);
    time_random(1:size(infos_random.time),e) = infos_random.time;
    err_random(1:size(infos_random.rmse),e) = infos_random.rmse;

    %% Hybrid SGD - Increase
    
    options.batchflag = 'increase';
    fprintf('---------------------  Increase Batch ---------------------\n')
    [X_increase, infos_increase] = batchSGD(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_increase_final(e, 1) = infos_increase.time(end);
    time_increase(1:size(infos_increase.time),e) = infos_increase.time;
    err_increase(1:size(infos_increase.rmse),e) = infos_increase.rmse;
    
    %% Hybrid SGD - Shuffle
    
    options.batchflag = 'shuffle';
    fprintf('---------------------  Shuffle Batch ---------------------\n')
    [X_shuffle, infos_shuffle] = batchSGD(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_shuffle_final(e, 1) = infos_shuffle.time(end);
    time_shuffle(1:size(infos_shuffle.time),e) = infos_shuffle.time;
    err_shuffle(1:size(infos_shuffle.rmse),e) = infos_shuffle.rmse;
    
    %% Hybrid SGD - Constant
    
    options.batchflag = 'constant';
    fprintf('---------------------  Constant Batch ---------------------\n')
    [X_constant, infos_constant] = batchSGD(Xinit, d1, d2, r, data_ls, data_ts, options);
    time_constant_final(e, 1) = infos_constant.time(end);
    time_constant(1:size(infos_constant.time),e) = infos_constant.time;
    err_constant(1:size(infos_constant.rmse),e) = infos_constant.rmse;

end

%% Averages

mean_err_random = mean(err_random, 2);
mean_err_increase = mean(err_increase, 2);
mean_err_shuffle = mean(err_shuffle, 2);
mean_err_constant = mean(err_constant, 2);

mean_random_time = mean(time_random, 2);
mean_increase_time = mean(time_increase, 2);
mean_shuffle_time = mean(time_shuffle, 2);
mean_constant_time = mean(time_constant, 2);

mean_time_random_final = mean(time_random_final);
mean_time_constant_final = mean(time_constant_final);
mean_time_shuffle_final = mean(time_shuffle_final);
mean_time_increase_final = mean(time_increase_final);

%% Plots

% Cost versus iterations
fs = 15;
figure;
semilogy(mean_random_time, mean_err_random,'-','LineWidth',2);
hold on;
semilogy(mean_increase_time, mean_err_increase,'-','LineWidth',2);
hold on;
semilogy(mean_shuffle_time, mean_err_shuffle,'-','LineWidth',2);
hold on;
semilogy(mean_constant_time, mean_err_constant,'-','LineWidth',2);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Time (s)','FontSize',fs);
ylabel(ax1,'RMSE','FontSize',fs);
%axis([get(gca,'XLim') msetol 1e3])
legend('Entire epoch (Random)', 'Increasing batch-size', 'Random batch-size', 'Constant batch-size');
legend 'boxoff';
title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling)])
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('hybrid');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');




fprintf('We are done :) \n')





