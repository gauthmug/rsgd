clear; clc; close all;

%% Intro
% Running this code generates Figure 5.1 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 06062018;
%SD = 'shuffle';
rng(SD);

%% Load full traffic dataset of size 963-by-10560
load('traffic.mat');
Ymat = traffic.Ymat;
Ycoo = traffic.Ycoo;



%% Problem specifications and options
mu = 0.5;
maxiterations = 50;
batchsize = 1;
msetol = 1e-8;
relresidualtol = 1e-6; 
random_initialization = false;
stepsize = 1e-1;
lambda = 1e-2;
flag = 'in';
stepSelect = 'counter';


%nu = 1000;
nu = 10560; % total number

r = 7; % <--- change this value: we tried 7 because there are 7 days in the week
over_sampling = 4;

numberExp = 2;
experim = 1:numberExp;

train_rmse_random = zeros(maxiterations, numberExp);
train_rmse_smart = zeros(maxiterations, numberExp);
train_rmse_weighted = zeros(maxiterations, numberExp);

test_rmse_random = zeros(maxiterations, numberExp);
test_rmse_smart = zeros(maxiterations, numberExp);
test_rmse_weighted = zeros(maxiterations, numberExp);

time_random = zeros(maxiterations, numberExp);
time_smart = zeros(maxiterations, numberExp);
time_weighted = zeros(maxiterations, numberExp);


for e = experim
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    
    p = randperm(size(Ymat,2), nu);
    A = Ymat(:, p); % Matrix of size 963-by-nu
    d1 = size(A,1);
    d2 = size(A,2);
    N = over_sampling*r*(d2 + d1 - r); % total number of known entries
    
    
    
    % Select a random set of M entries
    idx = unique(ceil(d2*d1*rand(1,(10*N))));
    idx = idx(randperm(length(idx)));
    
    [I, J] = ind2sub([d1, d2],idx(1:N));
    [J, inxs] = sort(J);
    I = I(inxs)';
    
    % Values of Y at the locations indexed by I and J.
    %S = sum(YL(I,:).*YR(J,:), 2);
    S = zeros(N,1);
    for k = 1:N
        S(k,1) = Ymat(I(k), J(k));
    end
    
    
    values = sparse(I,J,S,d1,d2);
    indicator = sparse(I,J,1,d1,d2);
    
    [rows, cols] = find(indicator == 1);
    values_vec = values(:);
    entries = values_vec(sub2ind(size(values), rows, cols));
    
    
    %% Interface with our algorithms
    
    % Learning data
    data_ls.rows = rows;
    data_ls.cols = cols;
    data_ls.entries = entries;
    data_ls.nentries = length(data_ls.entries);
    
    
    % Testing data
    data_ts.nentries = 1*data_ls.nentries;
    data_ts.rows = randi(d1, data_ts.nentries, 1);
    data_ts.cols = randi(d2, data_ts.nentries, 1);
    
    for k = 1:data_ts.nentries
        data_ts.entries(k,1) = Ymat(data_ts.rows(k), data_ts.cols(k));
    end
    
    
    
    % Permute train data
    random_order = randperm(length(data_ls.rows));
    data_ls.rows = data_ls.rows(random_order);
    data_ls.cols = data_ls.cols(random_order);
    data_ls.entries = data_ls.entries(random_order);
    
    %% Initialization
    
    Xinit.L = randn(d1, r);
    Xinit.R = randn(d2, r);
    
    
    
    %% Scaled-SGD
    options.maxiterations = maxiterations;
    options.batchsize = batchsize;
    options.msetol = msetol;
    options.relresidualtol = relresidualtol;
    options.mu = mu;
    options.stepsize = stepsize;
    options.lambda = lambda;
    options.flag = flag;
    options.stepSelect = stepSelect;
    

    %% Random SGD
    options.shuffle = 'random';
    fprintf('---------------------  Random shuffling ---------------------\n')
    [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    
    %% Smart SGD
    options.shuffle = 'shuffling';  
    fprintf('---------------------  Smart shuffling ---------------------\n')
    [X_smart, infos_smart] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    
    %% Weighted
    options.shuffle = 'weighted';
    options.beta = round(3*N/4);
    options.weightsprop = 'One';
    options.weightCost = 'cheap';
    fprintf('---------------------  Weighted shuffling ---------------------\n')
    [X_weighted, infos_weighted] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    

    %% Averages
    train_rmse_random(1:size(infos_random.rmse),e)      = infos_random.rmse;
    train_rmse_smart(1:size(infos_smart.rmse),e)        = infos_smart.rmse;
    train_rmse_weighted(1:size(infos_weighted.rmse),e)  = infos_weighted.rmse;
    
    test_rmse_random(1:size(infos_random.rmse_test),e)     = infos_random.rmse_test;
    test_rmse_smart(1:size(infos_smart.rmse_test),e)       = infos_smart.rmse_test;
    test_rmse_weighted(1:size(infos_weighted.rmse_test),e) = infos_weighted.rmse_test;
    
    time_random(1:size(infos_random.time), e)      = infos_random.time;
    time_smart(1:size(infos_smart.time), e)        = infos_smart.time;
    time_weighted(1:size(infos_weighted.time), e)  = infos_weighted.time;
    
    
    
end


%% Averages
train_mean_random = mean(train_rmse_random, 2);
train_mean_smart = mean(train_rmse_smart, 2);
train_mean_weighted = mean(train_rmse_weighted, 2);

test_mean_random = mean(test_rmse_random, 2);
test_mean_smart = mean(test_rmse_smart, 2);
test_mean_weighted = mean(test_rmse_weighted, 2);

time_mean_random = mean(time_random, 2);
time_mean_smart = mean(time_random, 2);
time_mean_weighted = mean(time_weighted, 2);


%% Plots

% Train
fs = 20;
figure;
semilogy(time_mean_random(5:end), train_mean_random(5:end),'-o','LineWidth',1);
hold on;
semilogy(time_mean_smart(5:end), train_mean_smart(5:end),'-o','LineWidth',1);
hold on;
semilogy(time_mean_weighted(5:end), train_mean_weighted(5:end),'-o','LineWidth',1);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Time (s)','FontSize',fs);
ylabel(ax1,'Train RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('Random shuffling', 'Smart shuffling', 'Weighted shuffling');
legend 'boxoff';
%title([num2str(n),' by ',num2str(m),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('trafficTrain');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');



% Test
figure;
semilogy(time_mean_random(5:end), test_mean_random(5:end),'-o','LineWidth',1);
hold on;
semilogy(time_mean_smart(5:end), test_mean_smart(5:end),'-o','LineWidth',1);
hold on;
semilogy(time_mean_weighted(5:end), test_mean_weighted(5:end),'-o','LineWidth',1);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Time (s)','FontSize',fs);
ylabel(ax1,'Test RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('Random shuffling', 'Smart shuffling', 'Weighted shuffling');
legend 'boxoff';
%title([num2str(n),' by ',num2str(m),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('trafficTest');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');

