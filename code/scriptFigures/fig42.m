clc; clear; close all;

%% Intro
% Running this code generates Figure 4.2 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
%
% This code is inspired from Bamdev Mishra and Rodolphe Sepulchre
% in the context of the paper
% "Scaled stochastic gradient descent for low-rank matrix completion",
% Technical report, arXiv:1603.04989, 2016.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 17 March 2016
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM

%% Controling the random number generator
SD = 70062018;
rng(SD);


%% Problem specifications and options
d1 = 1000;
d2 = 1000;
r = 10;
over_sampling = 4;
noiseFac = 1e-6;
mu = 0.5;
maxiterations = 100;
batchsize = 1;
msetol = 1e-8;
relresidualtol = 1e-5; 
random_initialization = false;
condition_number = 0; 
stepsize = 0.05;
lambda = 0;
flag = 'in';
stepSelect = 'bold';

numberExp = 3;
experim = 1:numberExp;

train_rmse_no = sqrt(msetol)*ones(maxiterations, numberExp);
train_rmse_random = sqrt(msetol)*ones(maxiterations, numberExp);
train_rmse_random_replacement = sqrt(msetol)*ones(maxiterations, numberExp);
train_rmse_shuffling = sqrt(msetol)*ones(maxiterations, numberExp);

test_rmse_no = sqrt(msetol)*ones(maxiterations, numberExp);
test_rmse_random = sqrt(msetol)*ones(maxiterations, numberExp);
test_rmse_random_replacement = sqrt(msetol)*ones(maxiterations, numberExp);
test_rmse_shuffling = sqrt(msetol)*ones(maxiterations, numberExp);




%% Generate well-conditioned or ill-conditioned data

N = over_sampling*r*(d2 + d1 - r); % total number of known entries
% this corresponds to the size of the set Omega

if mod(N,2) == 1
    N = N-1;
end

for e = experim
    
	fprintf('Experiment %i out of %i \n', e, numberExp);

    % The left and right factors which make up our true data matrix Y.
    YL = randn(d1, r);
    YR = randn(d2, r);
    
    if condition_number > 0
        YLQ = orth(YL);
        YRQ = orth(YR);
        
        s1 = 1000;
        %     step = 1000; S0 = diag([s1:step:s1+(true_rank-1)*step]*1); % Linear decay
        S0 = s1*diag(logspace(-log10(condition_number),0,r)); % Exponential decay
        
        YL = YLQ*S0;
        YR = YRQ;
        
        fprintf('Creating a matrix with singular values...\n')
        for kk = 1: length(diag(S0));
            fprintf('%s \n', num2str(S0(kk, kk), '%10.5e') );
        end
        singular_vals = svd(YL'*YL);
        condition_number = sqrt(max(singular_vals)/min(singular_vals));
        fprintf('Condition number is %f \n', condition_number);
        
    end
    
    % Select a random set of M entries of Y = YL YR'.
    idx = unique(ceil(d2*d1*rand(1,(10*N))));
    idx = idx(randperm(length(idx)));
    
    [I, J] = ind2sub([d1, d2],idx(1:N));
    [J, inxs] = sort(J);
    I = I(inxs)';
    
    % Values of Y at the locations indexed by I and J.
    cleanS = sum(YL(I,:).*YR(J,:), 2);
    
    % Add noise.
    noise = noiseFac*max(cleanS)*randn(size(cleanS));
    S = cleanS + noise;
    
    
    values = sparse(I,J,S,d1,d2);
    indicator = sparse(I,J,1,d1,d2);
    
    [rows, cols] = find(indicator == 1);
    values_vec = values(:);
    entries = values_vec(sub2ind(size(values), rows, cols));
    
    
    %% Interface with our algorithms
    
    % Learning data
    data_ls.rows = rows;
    data_ls.cols = cols;
    data_ls.entries = entries;
    data_ls.nentries = length(data_ls.entries);
    
    
    % Testing data
    data_ts.nentries = 1*data_ls.nentries;
    data_ts.rows = randi(d1, data_ts.nentries, 1);
    data_ts.cols = randi(d2, data_ts.nentries, 1);
    data_ts.entries = sum(YL(data_ts.rows,:).*YR(data_ts.cols,:), 2);
    
    
    
    
    %% Initialization
    
    if random_initialization,
        Xinit.L = randn(d1, r);
        Xinit.R = randn(d2, r);
        fprintf('**** Random initialization\n');
    else
        sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
        [U, B, V] = svds(sparse_structure, r);
        Xinit.L = U*(B.^(0.5));
        Xinit.R = V*(B.^(0.5));
        fprintf('**** Initialization by taking %i dominant SVD\n', r);
    end
    
    %% Options
    options.maxiterations = maxiterations;
    options.batchsize = batchsize;
    options.msetol = msetol;
    options.relresidualtol = relresidualtol;
    options.mu = mu;
    options.stepsize = stepsize;
    options.lambda = lambda;
    options.flag = flag;
    options.stepSelect = stepSelect;
    
    
    
    %% No shuffling
    
    % Options are not mandatory
    options.shuffle = 'no';
    
    fprintf('---------------------  No shuffling ---------------------\n')
    [X_no, infos_no] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Random SGD
    
    % Options are not mandatory
    options.shuffle = 'random';
    
    fprintf('---------------------  Random shuffling ---------------------\n')
    [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Random-replacement SGD
    
    % Options are not mandatory
    options.shuffle = 'random_replacement';
    
    fprintf('---------------------  Random shuffling with replacement ---------------------\n')
    [X_random_replacement, infos_random_replacement] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Shuffled SGD
    
    % Options are not mandatory
    options.shuffle = 'shuffling';
    
    fprintf('---------------------  Smart shuffling ---------------------\n')
    [X_shuffling, infos_shuffling] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Averages
    
    train_rmse_no(1:size(infos_no.rmse),e)                                  =     infos_no.rmse;
    train_rmse_random(1:size(infos_random.rmse),e)                          =     infos_random.rmse;
    train_rmse_random_replacement(1:size(infos_random_replacement.rmse),e)  =     infos_random_replacement.rmse;
    train_rmse_shuffling(1:size(infos_shuffling.rmse),e)                    =     infos_shuffling.rmse;
    
    test_rmse_no(1:size(infos_no.rmse_test),e)                                  =     infos_no.rmse_test;
    test_rmse_random(1:size(infos_random.rmse_test),e)                          =     infos_random.rmse_test;
    test_rmse_random_replacement(1:size(infos_random_replacement.rmse_test),e)  =     infos_random_replacement.rmse_test;
    test_rmse_shuffling(1:size(infos_shuffling.rmse_test),e)                    =     infos_shuffling.rmse_test;
    
    
end



%% Mean and standard deviation

train_mean_no = mean(train_rmse_no, 2);
train_mean_random = mean(train_rmse_random, 2);
train_mean_random_replacement = mean(train_rmse_random_replacement, 2);
train_mean_shuffling = mean(train_rmse_shuffling, 2);

test_mean_no = mean(test_rmse_no, 2);
test_mean_random = mean(test_rmse_random, 2);
test_mean_random_replacement = mean(test_rmse_random_replacement, 2);
test_mean_shuffling = mean(test_rmse_shuffling, 2);


%std_no = std(avg_rmse_no, 2);
%std_random = std(avg_rmse_random, 2);
%std_random_replacement = std(avg_rmse_random_replacement, 2);
%std_shuffling = std(avg_rmse_shuffling, 2);

%% Plots

% Train
fs = 20;
figure;
semilogy(train_mean_no(1:17),'-o','LineWidth',1);
hold on;
semilogy(train_mean_random(1:18),'-o','LineWidth',1);
hold on;
semilogy(train_mean_random_replacement(1:19),'-o','LineWidth',1);
hold on;
semilogy(train_mean_shuffling(1:20),'-o','LineWidth',1);
hold on;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Iterations','FontSize',fs);
ylabel(ax1,'Train RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('No shuffling', 'Random without repl.', 'Random with repl.', 'Smart shuffling');
legend 'boxoff';
%title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
%title('Jester: training error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('syntheticFourTrain');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');



% Test
figure;
semilogy(test_mean_no(1:20),'-o','LineWidth',1);
hold on;
semilogy(test_mean_random(1:20),'-o','LineWidth',1);
hold on;
semilogy(test_mean_random_replacement(1:20),'-o','LineWidth',1);
hold on;
semilogy(test_mean_shuffling(1:20),'-o','LineWidth',1);
hold on;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Iterations','FontSize',fs);
ylabel(ax1,'Test RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('No shuffling', 'Random without repl.', 'Random with repl.', 'Smart shuffling');
legend 'boxoff';
%title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
%title('Jester: testing error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('syntheticFourTest');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');

fprintf('We are done :) \n')
%load handel
%sound(y,Fs)


