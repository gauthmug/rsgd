clc;
clear;
close all;

%% Intro
% Running this code DOES NOT generate Figure E.1 of the thesis 
% 'Stochastic Gradient Methods for Matrix Completion'
% available here: http://bit.ly/SGD2018
%
% Authors: Gauthier Muguerza, P.-A. Absil, Estelle Massart
% 
% Unfortunately, because we are using data from the
% Alzheimer's Disease Neuroimaging Initiative (ADNI, http://adni-info.org)
% we cannot publicly share this data and as a result cannot explicitly
% recreate this Figure.
% Individuals must register with ADNI to download the data.
%
%
% This code is inspired from B. Mishra, H. Kasai, P. Jawanpuria, and A. Saroop
% in the context of the paper
% "A Riemannian gossip approach to decentralized subspace learning on Grassmann manifold",
% Technical report, arXiv:1705.00467, 2017.
%
% This implementation is due to
% Bamdev Mishra <bamdevm@gmail.com>, 2017
% with modifications from
% Gauthier Muguerza <gauthier.muguerza@gmail.com>, 2018
%
% Approximated run-time: 2 hours
% Specifications:
%   - version R2015b of MATLAB
%   - 2.0 GHz Intel Core i7
%   - 8 GB of RAM
%
%
% This script cannot work on your computer, since it requires the data set
% available from ADNI:
% *Data used in preparation of this article were obtained from the Alzheimer's Disease
% Neuroimaging Initiative (ADNI) database (adni.loni.usc.edu). As such, the investigators
% within the ADNI contributed to the design and implementation of ADNI and/or provided data
% but did not participate in analysis or writing of this report. A complete listing of ADNI
% investigators can be found at:
% http://adni.loni.usc.edu/wp-content/uploads/how_to_apply/ADNI_Acknowledgement_List.pdf
%
%

%% Controling the random number generator
SD = 04012018;
%SD = 'shuffle';
rng(SD);


importADNI

%% Computing some numbers

sum(sum(dataADNI == 0));


sum(isnan(dataADNI));
d1 = size(dataADNI, 1);
d2 = size(dataADNI, 2);

numberOfUnknownEntries = sum(sum(dataADNI == -4)) ...
                            + sum(sum(dataADNI == -1)) ...
                            + sum(sum(isnan(dataADNI)));
totalNumberOfEntries = d1*d2;
ratioOfMissingEntries = numberOfUnknownEntries/totalNumberOfEntries;





A = dataADNI(:,9:end);

%% Options
r = 5; % <--- try different values here
maxiterations = 100;
batchsize = 1;
msetol = 1e-8;
relresidualtol = 1e-8;
mu = 0.5;
stepsize = 1e-1;
lambda = 0;
flag = 'in';
stepSelect = 'bold';
random_initialization = false;

numberExp = 3;
experim = 1:numberExp;

train_rmse_random = zeros(maxiterations, numberExp);
train_rmse_weighted = zeros(maxiterations, numberExp);
train_rmse_shuffling = zeros(maxiterations, numberExp);

test_rmse_random = zeros(maxiterations, numberExp);
test_rmse_weighted = zeros(maxiterations, numberExp);
test_rmse_shuffling = zeros(maxiterations, numberExp);


for e = experim
    
    fprintf('Experiment %i out of %i \n', e, numberExp);
    
    Avec = A(:);
    
    Avecindices = 1:length(Avec);
    Avecindices = Avecindices';
    i = ones(length(Avec),1);
    i(isnan(Avec)) = 0;
    i(Avec == -4) = 0;
    i(Avec == -1) = 0;
    Avecindices_final = Avecindices(logical(i));
    [I, J] = ind2sub([size(A,1)  100],Avecindices_final);
    
    Avecsfinall = Avec(logical(i));
    
    
    [Isort, indI] = sort(I,'ascend');
    
    
    data_real.rows = Isort;
    data_real.cols = J(indI);
    data_real.entries = Avecsfinall(indI);
    data_real.nentries = length(data_real.entries);
    

    
    
    %% permute them
    random_order = randperm(length(data_real.rows));
    data_real.rows = data_real.rows(random_order);
    data_real.cols = data_real.cols(random_order);
    data_real.entries = data_real.entries(random_order);

    
    %% Train / test split
    prop_known = 0.8; % or 0.9 % Training fraction <-- change value here
    
    % Data training
    allidx = 1 : length(data_real.rows); % all indices
    
    M = round(prop_known*length(data_real.rows)); % Number of entries known
    idx = 1 : M; % indices for training
    Matcom.row = (data_real.rows(idx));
    Matcom.col = (data_real.cols(idx));
    Matcom.values_Omega = (data_real.entries(idx));
    
    d1 = max(data_real.rows);
    d2 = max(data_real.cols);
    
    
    S = sparse(Matcom.row, Matcom.col, Matcom.values_Omega, d1, d2);
    [I_0, J_0, data_0] = find(S);
    Matcom.row = I_0;
    Matcom.col = J_0;
    Matcom.values_Omega = data_0;
    M = length(Matcom.row);
    
    data_ls.rows = Matcom.row;
    data_ls.cols = Matcom.col;
    data_ls.entries = Matcom.values_Omega;
    data_ls.nentries = length(data_ls.rows);
    
    
    % Test data
    M_test = length(data_real.rows) - M;
    L = length(data_real.rows);
    idx_test = allidx(L - M_test + 1: L);
    
    data_ts.rows = (data_real.rows(idx_test));
    data_ts.cols = (data_real.cols(idx_test));
    data_ts.entries = (data_real.entries(idx_test));
    data_ts.nentries = length(data_ts.entries);
       
    %% Initialization
    
    
    
    if ~random_initialization,
        sparse_structure = sparse(data_ls.rows,data_ls.cols,data_ls.entries,d1,d2);
        [U, B, V] = svds(sparse_structure, r);
        Xinit.L = U*(B.^(0.5));
        Xinit.R = V*(B.^(0.5));
        fprintf('**** Initialization by taking %i dominant SVD\n', r);
    else
        Xinit.L = randn(d1, r);
        Xinit.R = randn(d2, r);
        fprintf('**** Random initialization\n');
    end
    
    
    %% Scaled-SGD
    options.maxiterations = maxiterations;
    options.batchsize = batchsize;
    options.msetol = msetol;
    options.relresidualtol = relresidualtol;
    options.mu = mu;
    options.stepsize = stepsize;
    options.lambda = lambda;
    options.flag = flag;
    options.stepSelect = stepSelect;
    options.beta = round(3.5*M/4);
    options.weightsprop = 'One';
    
    
    %% Random
    options.shuffle = 'random';
    fprintf('---------------------  Random shuffling ---------------------\n')
    [X_random, infos_random] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    
    
    %% Samrt
    options.shuffle = 'shuffling';
    fprintf('---------------------  smart shuffling ---------------------\n')
    [X_shuffling, infos_shuffling] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    %% Weighted
    options.shuffle = 'weighted';
    options.weightCost = 'cheap';
    options.weightsprop = 'One';
    fprintf('---------------------  weighted shuffling ---------------------\n')
    [X_weighted, infos_weighted] = scaledSGDOne(Xinit, d1, d2, r, data_ls, data_ts, options);
    
    
    
    train_rmse_random(1:size(infos_random.rmse),e)       = infos_random.rmse;
    train_rmse_shuffling(1:size(infos_shuffling.rmse),e) = infos_shuffling.rmse;
    train_rmse_weighted(1:size(infos_weighted.rmse),e)   = infos_weighted.rmse;

    test_rmse_random(1:size(infos_random.nmae_movielens),e)       = infos_random.rmse_test;
    test_rmse_shuffling(1:size(infos_shuffling.nmae_movielens),e) = infos_shuffling.rmse_test;
    test_rmse_weighted(1:size(infos_weighted.nmae_movielens),e)   = infos_weighted.rmse_test;

end

%% Mean

train_mean_random = mean(train_rmse_random, 2);
train_mean_shuffling = mean(train_rmse_shuffling, 2);
train_mean_weighted = mean(train_rmse_weighted, 2);

test_mean_random = mean(test_rmse_random, 2);
test_mean_shuffling = mean(test_rmse_shuffling, 2);
test_mean_weighted = mean(test_rmse_weighted, 2);


%% Plots

% Train
fs = 20;
figure;
semilogy(train_mean_random(1:end),'-o','LineWidth',1);
hold on;
semilogy(train_mean_shuffling(1:end),'-o','LineWidth',1);
hold on;
semilogy(train_mean_weighted(1:end),'-o','LineWidth',1);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Iterations','FontSize',fs);
ylabel(ax1,'Train RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('Random shuffling', 'Smart shuffling', 'Weighted shuffling');
legend 'boxoff';
%title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
%title('Jester: training error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('ADNITrain');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');


% Test
figure;
semilogy(test_mean_random(1:end),'-o','LineWidth',1);
hold on;
semilogy(test_mean_shuffling(1:end),'-o','LineWidth',1);
hold on;
semilogy(test_mean_weighted(1:end),'-o','LineWidth',1);
hold off;
ax1 = gca;
set(ax1,'FontSize',fs);
xlabel(ax1,'Iterations','FontSize',fs);
ylabel(ax1,'Test RMSE','FontSize',fs);
% %axis([get(gca,'XLim') msetol 1e3])
legend('Random shuffling', 'Smart shuffling', 'Weighted shuffling');
legend 'boxoff';
%title([num2str(d1),' by ',num2str(d2),', rank ',num2str(r),', over-sampling ',num2str(over_sampling),', Condition No. ', num2str(condition_number)])
%title('Jester: training error')
box off;
fileDir = '/Users/gauthiermuguerza/Desktop/rsgd/report/images';
fileName = sprintf('ADNITest');
saveas(gcf, fullfile(fileDir,fileName), 'epsc');



fprintf('We are done :) \n')
%load handel
%sound(y,Fs)

    
